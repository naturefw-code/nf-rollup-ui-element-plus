declare const _default: import("vue").DefineComponent<{
    moduleId: NumberConstructor;
    active: ObjectConstructor;
}, {
    findMeta: {
        moduleId: number;
        quickFind: number[];
        allFind: number[];
        customer: {};
        findKind: {};
        "label-width": number;
        "finditem-width": number;
        customerDefault: number;
        itemMeta: {
            100: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: number;
                placeholder: string;
                title: string;
                colCount: number;
            };
            101: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            102: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            105: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            110: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            111: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            112: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            113: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            114: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            115: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            116: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            120: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            121: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            150: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: boolean;
                placeholder: string;
                title: string;
                optionList: {
                    value: boolean;
                    label: string;
                }[];
                colCount: number;
            };
            151: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: boolean;
                placeholder: string;
                title: string;
                optionList: {
                    value: boolean;
                    label: string;
                }[];
                colCount: number;
            };
            152: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                placeholder: string;
                title: string;
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            153: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            160: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: number;
                placeholder: string;
                title: string;
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            161: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                placeholder: string;
                title: string;
                colCount: number;
            };
            162: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                placeholder: string;
                title: string;
                colCount: number;
                optionList: ({
                    value: string;
                    label: string;
                    children: {
                        value: string;
                        label: string;
                        children: {
                            value: string;
                            label: string;
                        }[];
                    }[];
                } | {
                    value: string;
                    label: string;
                    children: {
                        value: string;
                        label: string;
                    }[];
                })[];
            };
        };
    };
    state: import("../controller/type").PageList & import("@naturefw/nf-state/dist/type").IState;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    moduleId: NumberConstructor;
    active: ObjectConstructor;
}>>, {}>;
export default _default;
