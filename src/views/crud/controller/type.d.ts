/**
 * 列表用的状态
 */
export interface PageList {
    moduleId: string | number;
    meta: any;
    dataList: Array<any>;
    findValue: any;
    findArray: Array<any>;
    pagerInfo: {
        pagerSize: number;
        count: number;
        pagerIndex: number;
    };
    selection?: {
        dataId: string | number;
        row: any;
        dataIds: Array<any>;
        rows: Array<any>;
    };
    query?: any;
    loadData?(isReset: boolean): void;
}
