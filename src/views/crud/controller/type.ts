  
/**
 * 列表用的状态
 */
export interface PageList {
  moduleId: string | number, // 模块ID
  meta: any, // 列表用的各种meta：grid、pager、button、find、form
  dataList: Array<any>, // 数据列表
  findValue: any, // 查询条件的精简形式
  findArray: Array<any>, // 查询条件的对象形式
  pagerInfo: { // 分页信息
    pagerSize: number,
    count: number, // 总数
    pagerIndex: number // 当前页号
  },
  selection?: { // 列表里选择的记录
    dataId: string | number, // 单选ID number 、string
    row: any, // 单选的数据对象 {}
    dataIds: Array<any>, // 多选ID []
    rows: Array<any> // 多选的数据对象 []
  },
  query?: any, // 查询条件
  loadData? (isReset: boolean): void,

}