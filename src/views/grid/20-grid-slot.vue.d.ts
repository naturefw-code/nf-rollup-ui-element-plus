declare const _default: import("vue").DefineComponent<{
    moduleID: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
}, {
    handleEdit: (index: number, row: any) => void;
    handleDelete: (index: number, row: any) => void;
    dataList: any;
    selection: {
        dataId: string | number;
        row: any;
        dataIds: (string | number)[];
        rows: any[];
    };
    gridMeta: {
        gridMeta: {
            moduleId: number;
            idName: string;
            colOrder: number[];
        };
        gridTable: {
            height: number;
            stripe: boolean;
            border: boolean;
            fit: boolean;
            "highlight-current-row": boolean;
        };
        itemMeta: {
            90: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            100: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            101: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            102: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            105: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            110: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            111: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            112: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            113: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
                "show-overflow-tooltip": boolean;
            };
            114: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            115: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            116: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            120: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            121: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            150: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            151: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            152: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            153: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            160: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            161: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            162: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            163: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
            164: {
                id: number;
                colName: string;
                label: string;
                width: number;
                title: string;
                align: string;
                "header-align": string;
            };
        };
    };
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    moduleID: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
}>>, {
    moduleID: string | number;
}>;
export default _default;
