declare const _default: import("vue").DefineComponent<Readonly<import("vue").ComponentPropsOptions<{
    [x: string]: unknown;
}>>, {
    buttonMeta: {
        moduleId: number;
        btnOrder: number[];
        itemMeta: {
            10: {
                btnTitle: string;
                btnKind: string;
                dialogTitle: string;
                dialogWidth: string;
                controlKey: string;
                openModuleId: number;
                formMetaId: number;
                actionId: number;
                hotkey: string;
            };
            20: {
                btnTitle: string;
                btnKind: string;
                dialogTitle: string;
                dialogWidth: string;
                controlKey: string;
                openModuleId: number;
                formMetaId: number;
                actionId: number;
                hotkey: string;
            };
            80: {
                btnTitle: string;
                btnKind: string;
                dialogTitle: string;
                dialogWidth: string;
                controlKey: string;
                openModuleId: number;
                formMetaId: number;
                actionId: number;
                hotkey: string;
            };
            30: {
                btnTitle: string;
                btnKind: string;
                dialogTitle: string;
                dialogWidth: string;
                controlKey: string;
                openModuleId: number;
                formMetaId: number;
                actionId: number;
                hotkey: string;
            };
            31: {
                btnTitle: string;
                btnKind: string;
                dialogTitle: string;
                dialogWidth: string;
                controlKey: string;
                openModuleId: number;
                formMetaId: number;
                actionId: number;
                hotkey: string;
            };
        };
    };
    componentKind: {
        form: import("vue").DefineComponent<Readonly<import("vue").ComponentPropsOptions<{
            [x: string]: unknown;
        }>>, {
            formMeta: {
                formMeta: {
                    moduleId: number;
                    formId: number;
                    columnsNumber: number;
                    colOrder: number[];
                    linkageMeta: {
                        90: {
                            1: number[];
                            2: number[];
                            3: number[];
                            4: number[];
                            5: number[];
                            6: number[];
                        };
                    };
                    ruleMeta: {
                        101: ({
                            trigger: string;
                            message: string;
                            required: boolean;
                            min?: undefined;
                            max?: undefined;
                        } | {
                            trigger: string;
                            message: string;
                            min: number;
                            max: number;
                            required?: undefined;
                        })[];
                    };
                };
                itemMeta: {
                    90: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: number;
                            title: string;
                            optionList: {
                                value: number;
                                label: string;
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    100: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: number;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    101: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    102: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    103: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    104: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    105: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    106: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    107: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            optionList: {
                                value: string;
                                label: string;
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    108: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    110: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    111: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    112: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    120: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    121: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    122: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    123: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    124: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    125: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    126: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    127: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    128: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    130: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    131: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    132: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    133: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    140: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    150: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: boolean;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    151: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: boolean;
                            title: string;
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    152: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            optionList: {
                                value: number;
                                label: string;
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    153: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            optionList: {
                                value: number;
                                label: string;
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    160: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: number;
                            title: string;
                            optionList: {
                                value: number;
                                label: string;
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    161: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            optionList: {
                                value: number;
                                label: string;
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    162: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            optionList: {
                                label: string;
                                options: {
                                    value: number;
                                    label: string;
                                    disabled: boolean;
                                }[];
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    163: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            optionList: {
                                label: string;
                                options: {
                                    value: number;
                                    label: string;
                                    disabled: boolean;
                                }[];
                            }[];
                            colCount: number;
                        };
                        placeholder: string;
                    };
                    164: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                            optionList: ({
                                value: string;
                                label: string;
                                children: {
                                    value: string;
                                    label: string;
                                    children: {
                                        value: string;
                                        label: string;
                                    }[];
                                }[];
                            } | {
                                value: string;
                                label: string;
                                children: {
                                    value: string;
                                    label: string;
                                }[];
                            })[];
                        };
                        placeholder: string;
                    };
                    165: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: string;
                            title: string;
                            colCount: number;
                            optionList: ({
                                parentId: string;
                                level: number;
                                value: string;
                                label: string;
                                disabled: boolean;
                            } | {
                                parentId: number;
                                level: number;
                                value: string;
                                label: string;
                                disabled: boolean;
                            })[];
                        };
                        placeholder: string;
                    };
                    166: {
                        formItemMeta: {
                            columnId: number;
                            colName: string;
                            label: string;
                            controlType: number;
                            isClear: boolean;
                            defValue: never[];
                            title: string;
                            colCount: number;
                            optionList: ({
                                parentId: string;
                                level: number;
                                value: string;
                                label: string;
                                disabled: boolean;
                            } | {
                                parentId: number;
                                level: number;
                                value: string;
                                label: string;
                                disabled: boolean;
                            })[];
                        };
                        placeholder: string;
                    };
                };
            };
            model2: {};
            model: {
                [x: string]: any;
                [x: number]: any;
            };
        }, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, readonly string[] | Readonly<import("vue").ExtractPropTypes<Readonly<import("vue").ComponentObjectPropsOptions<{
            [x: string]: unknown;
        }>>>>, {
            [x: number]: string;
        } | {}>;
        list: import("vue").DefineComponent<{
            moduleID: {
                type: (StringConstructor | NumberConstructor)[];
                default: number;
            };
        }, {
            dataList: any;
            selection: {
                dataId: string;
                row: {};
                dataIds: never[];
                rows: never[];
            };
            gridMeta: {
                gridMeta: {
                    moduleId: number;
                    idName: string;
                    colOrder: number[];
                };
                height: number;
                stripe: boolean;
                border: boolean;
                fit: boolean;
                "highlight-current-row": boolean;
                itemMeta: {
                    90: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    100: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    101: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    102: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    105: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    110: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    111: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    112: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    113: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                        "show-overflow-tooltip": boolean;
                    };
                    114: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    115: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    116: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    120: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    121: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    150: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    151: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    152: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    153: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    160: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    161: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    162: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    163: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                    164: {
                        id: number;
                        colName: string;
                        label: string;
                        width: number;
                        title: string;
                        align: string;
                        "header-align": string;
                    };
                };
            };
        }, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
            moduleID: {
                type: (StringConstructor | NumberConstructor)[];
                default: number;
            };
        }>>, {
            moduleID: string | number;
        }>;
    };
    moduleId: number;
    click1: () => void;
    click2: () => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, readonly string[] | Readonly<import("vue").ExtractPropTypes<Readonly<import("vue").ComponentObjectPropsOptions<{
    [x: string]: unknown;
}>>>>, {
    [x: number]: string;
} | {}>;
export default _default;
