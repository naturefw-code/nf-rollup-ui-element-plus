declare const _default: import("vue").DefineComponent<Readonly<import("vue").ComponentPropsOptions<{
    [x: string]: unknown;
}>>, {
    formMeta: {
        formMeta: {
            moduleId: number;
            formId: number;
            formColCount: number;
            colOrder: number[];
            formColShow: {
                90: {
                    1: number[];
                    2: number[];
                    3: number[];
                    4: number[];
                    5: number[];
                    6: number[];
                };
            };
            ruleMeta: {
                101: ({
                    trigger: string;
                    message: string;
                    required: boolean;
                    min?: undefined;
                    max?: undefined;
                } | {
                    trigger: string;
                    message: string;
                    min: number;
                    max: number;
                    required?: undefined;
                })[];
            };
        };
        itemMeta: {
            90: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: number;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            100: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: number;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            101: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            102: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            103: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            104: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            105: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            106: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            107: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: string;
                    label: string;
                }[];
                colCount: number;
            };
            108: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            110: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            111: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            112: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            120: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            121: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            122: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            123: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            124: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            125: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            126: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            127: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            128: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            130: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            131: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            132: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            133: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            140: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            150: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            151: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            152: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            153: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            160: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: number;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            161: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            162: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    label: string;
                    options: {
                        value: number;
                        label: string;
                        disabled: boolean;
                    }[];
                }[];
                colCount: number;
            };
            163: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    label: string;
                    options: {
                        value: number;
                        label: string;
                        disabled: boolean;
                    }[];
                }[];
                colCount: number;
            };
            164: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
                optionList: ({
                    value: string;
                    label: string;
                    children: {
                        value: string;
                        label: string;
                        children: {
                            value: string;
                            label: string;
                        }[];
                    }[];
                } | {
                    value: string;
                    label: string;
                    children: {
                        value: string;
                        label: string;
                    }[];
                })[];
            };
            165: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
                optionList: ({
                    parentId: string;
                    level: number;
                    value: string;
                    label: string;
                    disabled: boolean;
                } | {
                    parentId: number;
                    level: number;
                    value: string;
                    label: string;
                    disabled: boolean;
                })[];
            };
            166: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
                optionList: ({
                    parentId: string;
                    level: number;
                    value: string;
                    label: string;
                    disabled: boolean;
                } | {
                    parentId: number;
                    level: number;
                    value: string;
                    label: string;
                    disabled: boolean;
                })[];
            };
        };
    };
    model2: {};
    model3: {};
    model: {
        [x: string]: any;
        [x: number]: any;
    };
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, readonly string[] | Readonly<import("vue").ExtractPropTypes<Readonly<import("vue").ComponentObjectPropsOptions<{
    [x: string]: unknown;
}>>>>, {
    [x: number]: string;
} | {}>;
export default _default;
