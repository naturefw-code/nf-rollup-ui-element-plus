/**
 * 用于查询演示的 indexedDB 数据库
 */

// 创建help
import { dbCreateHelp } from '@naturefw/storage'

// 访问后端
// import axios from 'axios'
// 访问状态
// import { store } from '@naturefw/nf-state'

// 设置数据库名称和版本
const db = {
  dbName: 'nf-find-db-test',
  ver: 1
}

/**
 * 用户的个性化方案
 */
export default async function createDBHelp (callback) {
  // 设置基础路径
  // config.baseUrl = url

  const help = dbCreateHelp({
    dbFlag: 'nf-find-db-test',
    dbConfig: db,
    stores: { 
      /**
       * * caseId：'方案编号',
       * * moduleId: '模块ID',
       * * label: '方案名称',
       * * meta: { // meta内容
       * * * header-align: '标题对齐方式'
       * * * align: '内容对齐方式'
       * * * width: '宽度'
       * * }
       */
      find_test: { // 列表的个性化方案
        id: 'findId',
        index: {
          modId: false
        },
        isClear: false
      }
    },
    // 设置初始数据
    async init (help) {
      console.log('find-db', help)
    }
  })
  return help
}