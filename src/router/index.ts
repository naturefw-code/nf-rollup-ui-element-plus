import { Document, FolderOpened } from '@element-plus/icons-vue'
// import { createRouter } from '@naturefw/ui-elp'
import { createRouter } from '../../lib/main'

// import { createRouter } from '../../lib/router/router'

// import type { IMenus } from '../../lib/types/90-router'

import home from '../views/home.vue'

// import list from '../views/data-list.vue'

// import list from '../views/plat/p02-table.vue'

// const baseUrl = (document.location.host.includes('.gitee.io')) ?
//  '/nf-rollup-state' :  ''

const url = import.meta.env.BASE_URL
// console.log('url', url)
const baseUrl = url === '/' ? '': '' + url

export default createRouter({
  /**
   * 基础路径
   */
  baseUrl: baseUrl,
  /**
   * 首页
   */
  home: home,

  menus: [
    {
      menuId: '1',
      title: '表单控件',
      naviId: '0',
      path: 'form',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '1010',
          naviId: '0',
          title: '基础表单',
          path: 'form',
          icon: Document,
          childrens:[],
          component: () => import('../views/form/10-form.vue')
        },
        {
          menuId: '1012',
          naviId: '0',
          title: 'slot 扩展表单',
          path: 'slot',
          icon: Document,
          childrens:[],
          component: () => import('../views/form/20-form-slot.vue')
        },
        {
          menuId: '1014',
          naviId: '0',
          title: 'card 形式表单',
          path: 'card',
          icon: Document,
          childrens:[],
          component: () => import('../views/form/30-form-card.vue')
        },
        {
          menuId: '1016',
          title: 'step 形式表单',
          naviId: '0',
          path: 'step',
          icon: Document,
          childrens:[],
          component: () => import('../views/form/40-form-step.vue')
        },
        {
          menuId: '1018',
          naviId: '0',
          title: 'tab 形式表单',
          path: 'tab',
          icon: Document,
          childrens:[],
          component: () => import('../views/form/50-form-tab.vue')
        }
      ]
    },
    {
      menuId: '2000',
      title: '查询控件',
      naviId: '0',
      path: 'find',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '2014',
          naviId: '0',
          title: '基础查询',
          path: 'base',
          icon: Document,
          childrens:[],
          component: () => import('../views/find/10-find.vue')
        },
        {
          menuId: '2016',
          naviId: '0',
          title: 'indexedDB 查询',
          path: 'indexedDB',
          icon: Document,
          childrens:[],
          component: () => import('../views/find/20-indexedDB.vue')
        }
      ]
    },
    {
      menuId: '3000',
      title: '列表控件',
      naviId: '0',
      path: 'gird',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '3060',
          naviId: '0',
          title: '列表控件',
          path: 'grid',
          icon: Document,
          childrens:[],
          component: () => import('../views/grid/10-grid.vue')
        },
        {
          menuId: '3070',
          naviId: '0',
          title: '列表控件的扩展',
          path: 'grid-slot',
          icon: Document,
          childrens:[],
          component: () => import('../views/grid/20-grid-slot.vue')
        },
        {
          menuId: '3080',
          naviId: '0',
          title: '列表控件的列',
          path: 'grid-col',
          icon: Document,
          childrens:[],
          component: () => import('../views/grid/30-grid-slot-col.vue')
        }
      ]
    },
    {
      menuId: '4000',
      title: '其他控件',
      naviId: '0',
      path: 'other',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '4080',
          naviId: '0',
          title: '按钮控件',
          path: 'button',
          icon: Document,
          childrens:[],
          component: () => import('../views/button/10-button.vue')
        },
        {
          menuId: '4090',
          naviId: '0',
          title: '路由和菜单',
          path: 'menu',
          icon: Document,
          childrens:[],
          component: () => import('../views/router/10-router.vue')
        },
        {
          menuId: '4110',
          naviId: '0',
          title: '拖拽dianlog',
          path: 'dianlog',
          icon: Document,
          childrens:[],
          component: () => import('../views/dialog/10-dialog.vue')
        }
      ]
    },
    {
      menuId: '5000',
      title: '增删改查',
      naviId: '0',
      path: 'crud',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '5010',
          naviId: '0',
          title: '基础表单',
          path: 'base',
          icon: Document,
          childrens:[],
          component: () => import('../views/crud/10-crud.vue')
        }
      ]
    }
  ]
})
