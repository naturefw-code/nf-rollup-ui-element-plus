import { createApp } from 'vue'
import App from './App.vue'

import test from './views/extend/10-form-item-test.vue'

// UI库
import ElementPlus1 from 'element-plus'
// import 'element-plus/lib/theme-chalk/index.css'
// import 'dayjs/locale/zh-cn'
// import locale from 'element-plus/lib/locale/lang/zh-cn'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

// 建立 indexedDB 数据库
import dbFind from './store/find-db'

// 实现拖拽的自定义指令 
import {
  // config,
  // AllFormItem,
  // nfElementPlus
} from '../lib/main'

import {
  formItemKey
} from '../lib/main-item'

formItemKey[200] = test // 增加
// formItemKey[100] = test // 覆盖
 

// 设置icon
import installIcon from './icon/index'

// 简易路由
import router from './router/index'

const app = createApp(App)
app.config.globalProperties.$ELEMENT = {
  // locale: zhCn,
  size: 'small'
}

app.use(router) // 路由
  .use(dbFind)
  .use(ElementPlus1, { locale: zhCn, size: 'small' }) // UI库
  .use(installIcon) // 注册全局图标
  // .use(nfElementPlus) // 全局注册 , {formItem}
  .mount('#app')
