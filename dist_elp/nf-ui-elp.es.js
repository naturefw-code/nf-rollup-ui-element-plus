import { watch, defineComponent, reactive, ref, resolveComponent, resolveDirective, openBlock, createElementBlock, createCommentVNode, createVNode, withCtx, createTextVNode, toDisplayString, withDirectives, createBlock, resolveDynamicComponent, unref, Fragment, renderList, mergeProps, onMounted, createElementVNode, nextTick, withModifiers, computed, vShow, normalizeStyle, markRaw, createSlots, renderSlot, normalizeProps, guardReactiveProps, Transition } from 'vue';
import { ElButton, ElDialog, ElScrollbar, ElMessage, ElMessageBox, ElSpace, ElSubMenu, ElMenuItem, ElMenu, ElTabs, ElTabPane, ElLoading, ElTable, ElTableColumn, ElFormItem, ElRow, ElCol, ElForm, ElCard, ElSteps, ElStep } from 'element-plus';
import { mykeydown, itemProps, itemController, loadController, getChildrenOne, shallowToDeep, findKindDict, findProps, useRouter, gridProps, formProps, formController, formDrag, gridDrag } from '@naturefw/ui-core';
export { createDataList, createModel, createRouter, formController, itemController, itemProps, lifecycle, loadController, service, serviceConfig, useRouter } from '@naturefw/ui-core';
import { CircleCloseFilled, Check, ArrowDownBold, ArrowRightBold } from '@element-plus/icons-vue';
import dayjs from 'dayjs';
import { useStoreLocal, defineStore } from '@naturefw/nf-state';

function dialogDrag() {
  const setDialog = (container, dialog, dialogTitle, width) => {
    const oldCursor = dialogTitle.style.cursor;
    const clientWidth = document.documentElement.clientWidth;
    const clientHeight = document.documentElement.clientHeight;
    const tmpWidth = clientWidth * (100 - width) / 200;
    const domset = {
      x: tmpWidth,
      y: clientHeight * 15 / 100
    };
    if (dialog.style.marginLeft === "") {
      dialog.style.marginLeft = domset.x + "px";
    } else {
      domset.x = dialog.style.marginLeft.replace("px", "") * 1;
    }
    if (dialog.style.marginTop === "") {
      dialog.style.marginTop = domset.y + "px";
    } else {
      domset.y = dialog.style.marginTop.replace("px", "") * 1;
    }
    const start = { x: 0, y: 0 };
    const move = { x: 0, y: 0 };
    dialogTitle.onmouseover = () => {
      dialogTitle.style.cursor = "move";
    };
    dialogTitle.onmousedown = (e) => {
      start.x = e.clientX;
      start.y = e.clientY;
      dialogTitle.style.cursor = "move";
    };
    container.onmousemove = (e) => {
      if (start.x === 0) {
        return;
      }
      move.x = e.clientX - start.x;
      move.y = e.clientY - start.y;
      dialog.style.marginLeft = domset.x + move.x + "px";
      dialog.style.marginTop = domset.y + move.y + "px";
    };
    container.onmouseup = (e) => {
      if (start.x === 0) {
        return;
      }
      move.x = e.clientX - start.x;
      move.y = e.clientY - start.y;
      domset.x += move.x;
      domset.y += move.y;
      dialogTitle.style.cursor = oldCursor;
      dialog.style.marginLeft = domset.x + "px";
      dialog.style.marginTop = domset.y + "px";
      start.x = 0;
    };
  };
  const unload = (container, dialogTitle) => {
    dialogTitle.onmouseover = null;
    dialogTitle.onmousedown = null;
    container.onmousemove = null;
    container.onmouseup = null;
  };
  return {
    setDialog,
    unload
  };
}

const _dialogDrag = {
  mounted(el, binding) {
    watch(binding.value, () => {
      if (!binding.value.visible)
        return;
      const container = el.firstElementChild.firstElementChild;
      if (container.onmousemove)
        return;
      setTimeout(() => {
        const _dialogTitle = el.getElementsByClassName("el-dialog__header");
        if (_dialogTitle.length === 0) {
          console.warn("\u6CA1\u6709\u627E\u5230\u8981\u62D6\u62FD\u7684 el-dialog", el);
        } else {
          const { setDialog } = dialogDrag();
          const dialogTitle = _dialogTitle[0];
          const dialog = el.firstElementChild.firstElementChild.firstElementChild;
          const arr = dialog.style.cssText.split(";");
          const width = arr[0].replace("%", "").replace("--el-dialog-width:", "");
          setDialog(container, dialog, dialogTitle, width);
        }
      }, 300);
    });
  },
  unmounted(el, binding) {
    setTimeout(() => {
      const _dialogTitle = el.getElementsByClassName("el-dialog__header");
      if (_dialogTitle.length === 0) {
        console.warn("\u6CA1\u6709\u627E\u5230\u8981\u62D6\u62FD\u7684dialog", el);
      } else {
        const container = el.firstElementChild.firstElementChild;
        const dialogTitle = _dialogTitle[0];
        const { unload } = dialogDrag();
        unload(container, dialogTitle);
      }
    }, 500);
  }
};
const install$3 = (app, options) => {
  app.directive("dialogDrag", _dialogDrag);
};

var _export_sfc = (sfc, props) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props) {
    target[key] = val;
  }
  return target;
};

const _sfc_main$N = defineComponent({
  name: "nf-el-button-dialog",
  directives: {
    dialogDrag: _dialogDrag
  },
  components: {
    ElButton,
    ElDialog,
    ElScrollbar
  },
  props: {
    moduleId: [Number, String],
    dataId: [Number, String],
    active: Object,
    buttonMeta: {
      type: Object,
      default: () => {
        return {};
      }
    },
    componentKind: {
      type: Object,
      default: () => {
        return {};
      }
    }
  },
  setup(props, context) {
    const dialogInfo = reactive({
      visible: false
    });
    const btnDisabled = ref(true);
    const buttonClick = () => {
      if (props.buttonMeta.btnKind === "add") {
        dialogInfo.visible = true;
      } else if (!btnDisabled.value) {
        dialogInfo.visible = true;
      } else {
        ElMessage({
          type: "info",
          message: "\u8BF7\u5148\u9009\u62E9\u4E00\u6761\u8BB0\u5F55\u3002"
        });
      }
    };
    mykeydown(props.buttonMeta.hotkey, (e, isAlt) => {
      if (isAlt) {
        buttonClick();
      }
    });
    watch(() => props.dataId, (id) => {
      btnDisabled.value = props.buttonMeta.btnKind === "add" ? false : id === "";
    }, { immediate: true });
    return {
      dialogInfo,
      btnDisabled,
      buttonClick
    };
  }
});
function _sfc_render$J(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_button = resolveComponent("el-button");
  const _component_el_scrollbar = resolveComponent("el-scrollbar");
  const _component_el_dialog = resolveComponent("el-dialog");
  const _directive_dialog_drag = resolveDirective("dialog-drag");
  return openBlock(), createElementBlock("div", null, [
    createCommentVNode("\u5F39\u7A97\u7684\u6309\u94AE"),
    createVNode(_component_el_button, {
      type: "primary",
      disabled: _ctx.btnDisabled,
      onClick: _cache[0] || (_cache[0] = ($event) => _ctx.buttonClick())
    }, {
      default: withCtx(() => [
        createTextVNode(toDisplayString(_ctx.buttonMeta.btnTitle) + "(" + toDisplayString(_ctx.buttonMeta.hotkey) + ") ", 1)
      ]),
      _: 1
    }, 8, ["disabled"]),
    createCommentVNode('\u5F39\u7A97\uFF0C\u8868\u5355   v-dialogdrag="buttonMeta.dialogWidth"'),
    withDirectives((openBlock(), createElementBlock("div", null, [
      createVNode(_component_el_dialog, {
        title: _ctx.buttonMeta.dialogTitle,
        modelValue: _ctx.dialogInfo.visible,
        "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => _ctx.dialogInfo.visible = $event),
        modal: true,
        width: _ctx.buttonMeta.dialogWidth,
        "close-on-click-modal": true,
        "destroy-on-close": false
      }, {
        default: withCtx(() => [
          createVNode(_component_el_scrollbar, { style: { "height": "600px", "margin": "2px" } }, {
            default: withCtx(() => [
              (openBlock(), createBlock(resolveDynamicComponent(_ctx.componentKind[_ctx.buttonMeta.controlKey]), {
                moduleId: _ctx.buttonMeta.openModuleId,
                dataId: _ctx.dataId,
                formMetaId: _ctx.buttonMeta.formMetaId,
                actionId: _ctx.buttonMeta.actionId,
                type: _ctx.buttonMeta.btnKind,
                dialogInfo: _ctx.dialogInfo
              }, null, 8, ["moduleId", "dataId", "formMetaId", "actionId", "type", "dialogInfo"]))
            ]),
            _: 1
          })
        ]),
        _: 1
      }, 8, ["title", "modelValue", "width"])
    ])), [
      [_directive_dialog_drag, _ctx.dialogInfo]
    ])
  ]);
}
var nfButtonDialog = /* @__PURE__ */ _export_sfc(_sfc_main$N, [["render", _sfc_render$J], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/button/button-dialog.vue"]]);

const msgBox$2 = {
    confirmButtonText: '删除', // + dataListState.choice.dataId,
    cancelButtonText: '后悔了',
    type: 'warning'
  };
  const msgSucess = {
    type: 'success',
    message: '删除成功！'
  };
  const msgError$2 = {
    type: 'info',
    message: '已经取消了。'
  };

  const msgError1 = {
    type: 'info',
    message: '请先选择一条记录。'
  };



const _sfc_main$M = {
  __name: 'button-service',
  props: {
  moduleId: [Number, String],
  dataId: [Number, String],
  active: Object, // 按钮的meta集合
  buttonMeta: Object,
  reloadPager: Function,
  events: {
    type: Object,
    default: {
      deleteClick: () => {} // 删除按钮的单击事件，已经确实过的
    }
  }
},
  setup(__props) {

const props = __props;


// 属性：模块ID


const deleteClick = props.events.deleteClick;

// 设置按钮是否可用
const btnDisabled = ref(true);

// 单击按钮，执行删除操作
const buttonClick = () => {
  // model.delete(moduleId, actionId, dataId).then(() = > {})
  if (!btnDisabled.value) {
    ElMessageBox.confirm(`此操作将删除该记录(${props.dataId}), 是否继续？`, '温馨提示', msgBox$2)
      .then(async () => {
        // 调用回调函数
        const re = await deleteClick(props.dataId, props.buttonMeta, props.moduleId);
        if (re) {
          ElMessage(msgSucess);
        }
      })
      .catch(() => {
        // 取消删除操作
        ElMessage(msgError$2);
      });
  } else {
    // 没有选择记录的提示
    ElMessage(msgError1);
  }
};

// 按钮快捷键
mykeydown(props.buttonMeta.hotkey, (e, isAlt) => {
  if (isAlt && props.active.moduleId === props.moduleId) {
    buttonClick();
  }
});

// 监听选择的变化，记入状态
watch(() => props.dataId, (id) => {
  btnDisabled.value = (props.buttonMeta.btnKind === 'add') ? 
    false : id === '';
},
{ immediate: true });




return (_ctx, _cache) => {
  return (openBlock(), createElementBlock("div", null, [
    createCommentVNode("按钮"),
    createVNode(unref(ElButton), {
      type: "primary",
      disabled: btnDisabled.value,
      onClick: _cache[0] || (_cache[0] = $event => (buttonClick()))
    }, {
      default: withCtx(() => [
        createTextVNode(toDisplayString(__props.buttonMeta.btnTitle) + "(" + toDisplayString(__props.buttonMeta.hotkey) + ") ", 1 /* TEXT */)
      ]),
      _: 1 /* STABLE */
    }, 8 /* PROPS */, ["disabled"])
  ]))
}
}

};
var nfButtonService = /*#__PURE__*/_export_sfc(_sfc_main$M, [['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/button/button-service.vue"]]);

const _sfc_main$L = {
  __name: 'button',
  props: {
  moduleId: [Number, String], // 模块ID
  active: Object, // 按钮的meta集合
  dataId: {
    type: [String, Number],
    default: ''
  },
  dataIds: {
    type: [Array],
    default: []
  },
  btnOrder: Array, // 按钮的排序
  itemMeta: Object, // 按钮的meta集合
  events: {
    type: Object,
    default: {
      deleteClick: () => {} // 删除按钮的单击事件，已经确实过的
    }
  },
  componentKind: { // 弹窗打开的组件的字典
    type: Object,
    default: () => {}
  }
},
  setup(__props) {


// 属性：模块ID




return (_ctx, _cache) => {
  return (openBlock(), createElementBlock("div", null, [
    createCommentVNode("多个按钮"),
    createVNode(unref(ElSpace), { wrap: "" }, {
      default: withCtx(() => [
        (openBlock(true), createElementBlock(Fragment, null, renderList(__props.btnOrder, (item, key) => {
          return (openBlock(), createElementBlock("span", {
            key: 'btn_'+ key
          }, [
            (__props.itemMeta[item].btnKind === 'delete')
              ? (openBlock(), createBlock(nfButtonService, {
                  key: 0,
                  moduleId: __props.moduleId,
                  dataId: __props.dataId,
                  dataIds: __props.dataIds,
                  active: __props.active,
                  buttonMeta: __props.itemMeta[item],
                  events: __props.events
                }, null, 8 /* PROPS */, ["moduleId", "dataId", "dataIds", "active", "buttonMeta", "events"]))
              : (openBlock(), createBlock(nfButtonDialog, {
                  key: 1,
                  moduleId: __props.moduleId,
                  dataId: __props.dataId,
                  active: __props.active,
                  buttonMeta: __props.itemMeta[item],
                  componentKind: __props.componentKind
                }, null, 8 /* PROPS */, ["moduleId", "dataId", "active", "buttonMeta", "componentKind"]))
          ]))
        }), 128 /* KEYED_FRAGMENT */))
      ]),
      _: 1 /* STABLE */
    })
  ]))
}
}

};
var nfButton = /*#__PURE__*/_export_sfc(_sfc_main$L, [['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/button/button.vue"]]);

const _sfc_main$K = defineComponent({
  name: "nf-el-from-item-area",
  inheritAttrs: false,
  components: {},
  props: {
    modelValue: [String, Number],
    ...itemProps
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    return {
      ...itemController(props, context.emit)
    };
  }
});
function _sfc_render$I(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_input = resolveComponent("el-input");
  return openBlock(), createBlock(_component_el_input, mergeProps({
    type: "textarea",
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    title: _ctx.title,
    clearable: _ctx.clearable,
    onBlur: _ctx.run,
    onChange: _ctx.run,
    onClear: _ctx.run,
    onKeydown: _ctx.clear
  }), null, 16, ["modelValue", "id", "name", "title", "clearable", "onBlur", "onChange", "onClear", "onKeydown"]);
}
var nfArea = /* @__PURE__ */ _export_sfc(_sfc_main$K, [["render", _sfc_render$I], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/t-area.vue"]]);

const _sfc_main$J = defineComponent({
  name: "nf-el-form-item-text",
  inheritAttrs: false,
  components: {},
  props: {
    modelValue: [String, Number],
    colName: String,
    ...itemProps
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      value,
      run,
      clear
    } = itemController(props, context.emit);
    return {
      value,
      run,
      clear
    };
  }
});
function _sfc_render$H(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_input = resolveComponent("el-input");
  return openBlock(), createBlock(_component_el_input, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    title: _ctx.title,
    clearable: _ctx.clearable,
    onBlur: _ctx.run,
    onChange: _ctx.run,
    onClear: _ctx.run,
    onKeydown: _ctx.clear
  }), null, 16, ["modelValue", "id", "name", "title", "clearable", "onBlur", "onChange", "onClear", "onKeydown"]);
}
var nfText = /* @__PURE__ */ _export_sfc(_sfc_main$J, [["render", _sfc_render$H], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/t-text.vue"]]);

const _sfc_main$I = defineComponent({
  name: "nf-el-form-item-password",
  inheritAttrs: false,
  components: {},
  props: {
    modelValue: [String],
    "show-password": {
      type: Boolean,
      default: true
    },
    ...itemProps
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    return {
      ...itemController(props, context.emit)
    };
  }
});
function _sfc_render$G(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_input = resolveComponent("el-input");
  return openBlock(), createBlock(_component_el_input, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    title: _ctx.title,
    clearable: _ctx.clearable,
    "show-password": ""
  }), null, 16, ["modelValue", "id", "name", "title", "clearable"]);
}
var nfPassword = /* @__PURE__ */ _export_sfc(_sfc_main$I, [["render", _sfc_render$G], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/t-password.vue"]]);

const urlManage = (value) => {
  const comList = [
    { value: ".com" },
    { value: ".cn" },
    { value: ".net" },
    { value: ".gov" },
    { value: ".com.cn" },
    { value: ".net.cn" },
    { value: ".org.cn" },
    { value: ".gov.cn" },
    { value: ".org" },
    { value: ".top" },
    { value: ".vip" },
    { value: ".\u4E2D\u56FD" },
    { value: ".\u4F01\u4E1A" },
    { value: ".\u516C\u53F8" },
    { value: ".\u7F51\u7EDC" }
  ];
  const querySearch = (str, cb) => {
    const results = str ? comList.filter((item) => item.value.indexOf(str.toLowerCase()) === 0) : comList;
    cb(results);
  };
  const url = reactive({
    http: "Https://",
    com: ".com",
    value: ""
  });
  watch(url, (v1) => {
    value.value = url.http + url.value + url.com;
  });
  watch(() => value.value, (v1) => {
    if (typeof v1 !== "string")
      return;
    if (v1 === null)
      return;
    if (!v1)
      return;
    const arrUrlAll = v1.toLowerCase().split("://");
    if (arrUrlAll.length === 1) {
      url.value = arrUrlAll[0];
    } else if (arrUrlAll.length === 2) {
      url.http = arrUrlAll[0] + "://";
      const arrUrl = arrUrlAll[1].split(".");
      const len = arrUrl.length;
      let endPosition = 0;
      switch (len) {
        case 1:
          url.value = arrUrl[0];
          break;
        case 2:
          url.value = arrUrl[0];
          url.com = "." + arrUrl[1];
          break;
        default:
          if (arrUrl[len - 1] === "cn" && (arrUrl[len - 2] === "com" || arrUrl[len - 2] === "net" || arrUrl[len - 2] === "org" || arrUrl[len - 2] === "gov")) {
            endPosition = len - 2;
            url.com = "." + arrUrl[endPosition] + ".cn";
          } else {
            endPosition = len - 1;
            url.com = "." + arrUrl[endPosition];
          }
          url.value = arrUrl[0];
          for (let i = 1; i < endPosition; i++) {
            url.value += "." + arrUrl[i];
          }
          break;
      }
    }
  }, { immediate: true });
  return {
    url,
    querySearch
  };
};
const _sfc_main$H = defineComponent({
  name: "nf-el-form-item-url",
  inheritAttrs: false,
  components: {},
  props: {
    modelValue: [String],
    ...itemProps
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      value,
      run,
      clear
    } = itemController(props, context.emit);
    const domUrl = ref(null);
    onMounted(() => {
      setTimeout(() => {
      }, 300);
    });
    return {
      domUrl,
      run,
      clear,
      ...urlManage(value)
    };
  }
});
function _sfc_render$F(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_option = resolveComponent("el-option");
  const _component_el_select = resolveComponent("el-select");
  const _component_el_autocomplete = resolveComponent("el-autocomplete");
  const _component_el_input = resolveComponent("el-input");
  return openBlock(), createBlock(_component_el_input, mergeProps({
    ref: "domUrl",
    modelValue: _ctx.url.value,
    "onUpdate:modelValue": _cache[2] || (_cache[2] = ($event) => _ctx.url.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    title: _ctx.title,
    clearable: _ctx.clearable,
    onBlur: _ctx.run,
    onChange: _ctx.run,
    onClear: _ctx.run,
    onKeydown: _ctx.clear
  }), {
    prepend: withCtx(() => [
      createCommentVNode("\u524D\u9762\u7684\u9009\u9879"),
      createVNode(_component_el_select, mergeProps({
        style: { "width": "90px" },
        modelValue: _ctx.url.http,
        "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.url.http = $event)
      }, _ctx.$attrs), {
        default: withCtx(() => [
          createVNode(_component_el_option, {
            label: "Http://",
            value: "Http://"
          }),
          createVNode(_component_el_option, {
            label: "Https://",
            value: "Https://"
          })
        ]),
        _: 1
      }, 16, ["modelValue"])
    ]),
    append: withCtx(() => [
      createCommentVNode("\u540E\u9762\u7684\u57DF\u540D\u540E\u7F00"),
      createVNode(_component_el_autocomplete, {
        style: { "width": "100px" },
        class: "inline-input",
        clearable: "",
        modelValue: _ctx.url.com,
        "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => _ctx.url.com = $event),
        "fetch-suggestions": _ctx.querySearch
      }, null, 8, ["modelValue", "fetch-suggestions"])
    ]),
    _: 1
  }, 16, ["modelValue", "id", "name", "title", "clearable", "onBlur", "onChange", "onClear", "onKeydown"]);
}
var nfUrl = /* @__PURE__ */ _export_sfc(_sfc_main$H, [["render", _sfc_render$F], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/t-url.vue"]]);

const _sfc_main$G = defineComponent({
  name: "nf-el-form-item-autocomplete",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    colName: String,
    modelValue: [String, Array, Number]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      value
    } = itemController(props, context.emit);
    const querySearch = (str, cb) => {
      cb(props.optionList);
    };
    const { loadDict } = loadController(props);
    loadDict();
    return {
      value,
      querySearch
    };
  }
});
const _hoisted_1$c = { class: "name" };
const _hoisted_2$8 = {
  class: "addr",
  style: { "float": "right" }
};
function _sfc_render$E(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_autocomplete = resolveComponent("el-autocomplete");
  return openBlock(), createBlock(_component_el_autocomplete, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    title: _ctx.title,
    clearable: _ctx.clearable,
    "fetch-suggestions": _ctx.querySearch
  }), {
    default: withCtx(({ item }) => [
      createElementVNode("div", _hoisted_1$c, [
        createTextVNode(toDisplayString(item.value) + " ", 1),
        createElementVNode("span", _hoisted_2$8, toDisplayString(item.label), 1)
      ])
    ]),
    _: 1
  }, 16, ["modelValue", "id", "name", "title", "clearable", "fetch-suggestions"]);
}
var nfAutocomplete = /* @__PURE__ */ _export_sfc(_sfc_main$G, [["render", _sfc_render$E], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/t-autocomplete.vue"]]);

const _sfc_main$F = defineComponent({
  name: "nf-el-form-item-color",
  inheritAttrs: false,
  components: {},
  props: {
    modelValue: [String],
    ...itemProps
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    return {
      ...itemController(props, context.emit)
    };
  }
});
function _sfc_render$D(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_color_picker = resolveComponent("el-color-picker");
  return openBlock(), createBlock(_component_el_color_picker, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs), null, 16, ["modelValue"]);
}
var nfColor = /* @__PURE__ */ _export_sfc(_sfc_main$F, [["render", _sfc_render$D], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/t-color.vue"]]);

const _sfc_main$E = defineComponent({
  name: "nf-el-from-item-number",
  inheritAttrs: false,
  components: {
    CircleCloseFilled
  },
  props: {
    ...itemProps,
    controlsPosition: {
      type: String,
      default: "right"
    },
    colName: String,
    modelValue: [String, Number]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      value
    } = itemController(props, context.emit);
    const myclear = () => {
      value.value = false;
      nextTick(() => {
        value.value = null;
      });
    };
    return {
      myclear,
      value
    };
  }
});
function _sfc_render$C(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_input_number = resolveComponent("el-input-number");
  const _component_circle_close_filled = resolveComponent("circle-close-filled");
  return openBlock(), createElementBlock(Fragment, null, [
    createVNode(_component_el_input_number, mergeProps({
      modelValue: _ctx.value,
      "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
    }, _ctx.$attrs, {
      id: "c" + _ctx.formItemMeta.columnId,
      name: "c" + _ctx.formItemMeta.columnId,
      clearable: _ctx.clearable,
      "controls-position": _ctx.controlsPosition
    }), null, 16, ["modelValue", "id", "name", "clearable", "controls-position"]),
    _ctx.clearable ? (openBlock(), createBlock(_component_circle_close_filled, {
      key: 0,
      style: { "width": "1.2em", "height": "1.2em", "margin-top": "0px", "margin-left": "5px", "cursor": "pointer" },
      onClick: withModifiers(_ctx.myclear, ["stop"])
    }, null, 8, ["onClick"])) : createCommentVNode("v-if", true)
  ], 64);
}
var nfNumber = /* @__PURE__ */ _export_sfc(_sfc_main$E, [["render", _sfc_render$C], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/n-number.vue"]]);

const _sfc_main$D = defineComponent({
  name: "nf-el-from-item-range",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    modelValue: [String, Number]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    return {
      ...itemController(props, context.emit)
    };
  }
});
function _sfc_render$B(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_slider = resolveComponent("el-slider");
  return openBlock(), createBlock(_component_el_slider, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    clearable: _ctx.clearable
  }), null, 16, ["modelValue", "id", "name", "clearable"]);
}
var nfRange = /* @__PURE__ */ _export_sfc(_sfc_main$D, [["render", _sfc_render$B], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/n-range.vue"]]);

const _sfc_main$C = defineComponent({
  name: "nf-el-form-item-rate",
  inheritAttrs: false,
  components: {
    CircleCloseFilled
  },
  props: {
    ...itemProps,
    modelValue: [String, Number]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      value
    } = itemController(props, context.emit);
    const myclear = () => {
      value.value = null;
    };
    return {
      value,
      myclear
    };
  }
});
function _sfc_render$A(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_rate = resolveComponent("el-rate");
  const _component_circle_close_filled = resolveComponent("circle-close-filled");
  return openBlock(), createElementBlock(Fragment, null, [
    createVNode(_component_el_rate, mergeProps({
      modelValue: _ctx.value,
      "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
    }, _ctx.$attrs, {
      id: "c" + _ctx.formItemMeta.columnId,
      name: "c" + _ctx.formItemMeta.columnId,
      clearable: _ctx.clearable
    }), null, 16, ["modelValue", "id", "name", "clearable"]),
    _ctx.clearable ? (openBlock(), createBlock(_component_circle_close_filled, {
      key: 0,
      style: { "width": "1.2em", "height": "1.2em", "margin-top": "-2px", "margin-left": "5px", "cursor": "pointer" },
      onClick: withModifiers(_ctx.myclear, ["stop"])
    }, null, 8, ["onClick"])) : createCommentVNode("v-if", true)
  ], 64);
}
var nfRate = /* @__PURE__ */ _export_sfc(_sfc_main$C, [["render", _sfc_render$A], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/n-rate.vue"]]);

const _sfc_main$B = defineComponent({
  name: "nf-el-from-item-date2",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    format: {
      type: String,
      default: "YYYY-MM-DD"
    },
    valueFormat: {
      type: String,
      default: "YYYY-MM-DD"
    },
    colName: String,
    modelValue: [String, Date, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    let dateType = "date";
    if (props.formItemMeta.controlType == "125") {
      dateType = "daterange";
      if (!Array.isArray(value.value)) {
        value.value = [];
      }
    } else {
      if (Array.isArray(value.value)) {
        value.value = "";
      }
    }
    return {
      dateType,
      value
    };
  }
});
function _sfc_render$z(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_date_picker = resolveComponent("el-date-picker");
  return openBlock(), createBlock(_component_el_date_picker, mergeProps({
    ref: "domDate",
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    type: _ctx.dateType,
    name: "c" + _ctx.formItemMeta.columnId,
    format: _ctx.format,
    "value-format": _ctx.valueFormat,
    title: _ctx.title,
    clearable: _ctx.clearable
  }), null, 16, ["modelValue", "type", "name", "format", "value-format", "title", "clearable"]);
}
var nfDate = /* @__PURE__ */ _export_sfc(_sfc_main$B, [["render", _sfc_render$z], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-date.vue"]]);

const _sfc_main$A = defineComponent({
  name: "nf-el-from-item-date-time",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    format: {
      type: String,
      default: "YYYY-MM-DD HH:mm:ss"
    },
    valueFormat: {
      type: String,
      default: "YYYY-MM-DD HH:mm:ss"
    },
    colName: String,
    modelValue: [String, Date, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    let dateType = "datetime";
    if (props.formItemMeta.controlType == "126") {
      dateType = "datetimerange";
      if (!Array.isArray(value.value)) {
        value.value = [];
      }
    } else {
      if (Array.isArray(value.value)) {
        value.value = "";
      }
    }
    return {
      dateType,
      value
    };
  }
});
function _sfc_render$y(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_date_picker = resolveComponent("el-date-picker");
  return openBlock(), createBlock(_component_el_date_picker, mergeProps({
    ref: "domDate",
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    type: _ctx.dateType,
    name: "c" + _ctx.formItemMeta.columnId,
    format: _ctx.format,
    "value-format": _ctx.valueFormat,
    clearable: _ctx.clearable
  }), null, 16, ["modelValue", "type", "name", "format", "value-format", "clearable"]);
}
var nfDatetime = /* @__PURE__ */ _export_sfc(_sfc_main$A, [["render", _sfc_render$y], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-datetime.vue"]]);

const _sfc_main$z = defineComponent({
  name: "nf-el-from-item-month",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    format: {
      type: String,
      default: "YYYY-MM"
    },
    valueFormat: {
      type: String,
      default: "YYYY-MM"
    },
    colName: String,
    modelValue: [String, Date, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    let dateType = "month";
    if (props.formItemMeta.controlType == "127") {
      dateType = "monthrange";
      if (!Array.isArray(value.value)) {
        value.value = [];
      }
    } else {
      if (Array.isArray(value.value)) {
        value.value = "";
      }
    }
    return {
      dateType,
      value
    };
  }
});
function _sfc_render$x(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_date_picker = resolveComponent("el-date-picker");
  return openBlock(), createBlock(_component_el_date_picker, mergeProps({
    ref: "domDate",
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    type: _ctx.dateType,
    name: "c" + _ctx.formItemMeta.columnId,
    format: _ctx.format,
    "value-format": _ctx.valueFormat,
    clearable: _ctx.clearable
  }), null, 16, ["modelValue", "type", "name", "format", "value-format", "clearable"]);
}
var nfMonth = /* @__PURE__ */ _export_sfc(_sfc_main$z, [["render", _sfc_render$x], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-month.vue"]]);

const _sfc_main$y = defineComponent({
  name: "nf-el-from-item-year",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    format: {
      type: String,
      default: "YYYY"
    },
    "value-format": {
      type: String,
      default: "YYYY"
    },
    colName: String,
    modelValue: [String, Date, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    const dateType = Array.isArray(value.value) ? "yearrange" : "year";
    return {
      dateType,
      value
    };
  }
});
function _sfc_render$w(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_date_picker = resolveComponent("el-date-picker");
  return openBlock(), createBlock(_component_el_date_picker, mergeProps({
    ref: "domDate",
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    type: _ctx.dateType,
    name: "c" + _ctx.formItemMeta.columnId,
    format: _ctx.format,
    "value-format": _ctx.valueFormat,
    clearable: _ctx.clearable
  }), null, 16, ["modelValue", "type", "name", "format", "value-format", "clearable"]);
}
var nfYear = /* @__PURE__ */ _export_sfc(_sfc_main$y, [["render", _sfc_render$w], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-year.vue"]]);

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

var isoWeeksInYear$1 = {exports: {}};

(function (module, exports) {
!function(e,n){module.exports=n();}(commonjsGlobal,(function(){return function(e,n){n.prototype.isoWeeksInYear=function(){var e=this.isLeapYear(),n=this.endOf("y").day();return 4===n||e&&5===n?53:52};}}));
}(isoWeeksInYear$1));

var isoWeeksInYear = isoWeeksInYear$1.exports;

var isLeapYear$1 = {exports: {}};

(function (module, exports) {
!function(e,t){module.exports=t();}(commonjsGlobal,(function(){return function(e,t){t.prototype.isLeapYear=function(){return this.$y%4==0&&this.$y%100!=0||this.$y%400==0};}}));
}(isLeapYear$1));

var isLeapYear = isLeapYear$1.exports;

var isoWeek$1 = {exports: {}};

(function (module, exports) {
!function(e,t){module.exports=t();}(commonjsGlobal,(function(){var e="day";return function(t,i,s){var a=function(t){return t.add(4-t.isoWeekday(),e)},d=i.prototype;d.isoWeekYear=function(){return a(this).year()},d.isoWeek=function(t){if(!this.$utils().u(t))return this.add(7*(t-this.isoWeek()),e);var i,d,n,o,r=a(this),u=(i=this.isoWeekYear(),d=this.$u,n=(d?s.utc:s)().year(i).startOf("year"),o=4-n.isoWeekday(),n.isoWeekday()>4&&(o+=7),n.add(o,e));return r.diff(u,"week")+1},d.isoWeekday=function(e){return this.$utils().u(e)?this.day()||7:this.day(this.day()%7?e:e-7)};var n=d.startOf;d.startOf=function(e,t){var i=this.$utils(),s=!!i.u(t)||t;return "isoweek"===i.p(e)?s?this.date(this.date()-(this.isoWeekday()-1)).startOf("day"):this.date(this.date()-1-(this.isoWeekday()-1)+7).endOf("day"):n.bind(this)(e,t)};}}));
}(isoWeek$1));

var isoWeek = isoWeek$1.exports;

dayjs.extend(isoWeeksInYear);
dayjs.extend(isLeapYear);
dayjs.extend(isoWeek);
const dateManage = (value, props) => {
  const mydate = ref("");
  value.value = "";
  const dic = {
    1: (year) => new Date(year + "-01-01"),
    2: (year) => new Date(year - 1 + "-12-31"),
    3: (year) => new Date(year - 1 + "-12-30"),
    4: (year) => new Date(year - 1 + "-12-29"),
    5: (year) => new Date(year + "-01-04"),
    6: (year) => new Date(year + "-01-03"),
    0: (year) => new Date(year + "-01-02")
  };
  const weekToDate = (week) => {
    if (week === "" || week === 0) {
      return new Date();
    }
    const year = week.substr(0, 4);
    const w = week.substr(week.length - 2, 2);
    const dayOfWeek = new Date(year + "-01-01").getDay();
    const firstDay = dic[dayOfWeek](parseInt(year));
    return dayjs(firstDay).add(7 * (w - 1), "day").toDate();
  };
  watch(() => value.value, (v1, v2) => {
    if (v1 === "") {
      mydate.value = "";
    } else {
      mydate.value = weekToDate(v1);
    }
  }, { immediate: true });
  const myChange = (val) => {
    if (val === null) {
      value.value = "";
    } else {
      const wk = dayjs(val).add(1, "day").isoWeek().toString();
      const year = val.getFullYear();
      const re = year.toString() + " " + wk.padStart(2, "0");
      value.value = re;
      document.getElementsByName("c" + props.formItemMeta.columnId)[0].value = re;
    }
  };
  return {
    mydate,
    myChange
  };
};
const _sfc_main$x = defineComponent({
  name: "nf-el-from-item-week",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    format: {
      type: String,
      default: "YYYY-ww"
    },
    valueFormat: {
      type: String,
      default: "YYYY-ww"
    },
    colName: String,
    modelValue: [String, Date]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    return {
      ...dateManage(value, props)
    };
  }
});
function _sfc_render$v(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_date_picker = resolveComponent("el-date-picker");
  return openBlock(), createBlock(_component_el_date_picker, mergeProps({
    modelValue: _ctx.mydate,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.mydate = $event)
  }, _ctx.$attrs, {
    type: "week",
    name: "c" + _ctx.formItemMeta.columnId,
    format: _ctx.format,
    clearable: _ctx.clearable,
    onChange: _ctx.myChange
  }), null, 16, ["modelValue", "name", "format", "clearable", "onChange"]);
}
var nfWeek = /* @__PURE__ */ _export_sfc(_sfc_main$x, [["render", _sfc_render$v], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-week.vue"]]);

const _sfc_main$w = defineComponent({
  name: "nf-el-from-item-date-more",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    format: {
      type: String,
      default: "YYYY-MM-DD"
    },
    valueFormat: {
      type: String,
      default: "YYYY-MM-DD"
    },
    colName: String,
    modelValue: [Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    if (!Array.isArray(value.value)) {
      value.value = [];
    }
    return {
      value
    };
  }
});
function _sfc_render$u(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_date_picker = resolveComponent("el-date-picker");
  return openBlock(), createBlock(_component_el_date_picker, mergeProps({
    ref: "domDate",
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    type: "dates",
    name: "c" + _ctx.formItemMeta.columnId,
    format: _ctx.format,
    "value-format": _ctx.valueFormat,
    clearable: _ctx.clearable
  }), null, 16, ["modelValue", "name", "format", "value-format", "clearable"]);
}
var nfDates = /* @__PURE__ */ _export_sfc(_sfc_main$w, [["render", _sfc_render$u], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-date-more.vue"]]);

const timeManage$1 = (value) => {
  const mytime = ref("");
  const getTime = (_val) => {
    const hour = _val.getHours().toString().padStart(2, "0");
    const mm = _val.getMinutes().toString().padStart(2, "0");
    const ss = _val.getSeconds().toString().padStart(2, "0");
    const re = `${hour}:${mm}:${ss}`;
    return re;
  };
  if (typeof value.value === "string") {
    if (value.value !== "") {
      mytime.value = new Date("1900-1-1 " + value.value);
    }
  }
  watch(() => value.value, (v1, v2) => {
    if (typeof value.value === "string") {
      if (value.value !== "") {
        mytime.value = new Date("1900-1-1 " + value.value);
      }
    }
  });
  const myChange = (_val) => {
    if (_val) {
      if (typeof _val === "string") {
        value.value = _val;
      } else if (Array.isArray(_val)) {
        const re1 = getTime(_val[0]);
        const re2 = getTime(_val[0]);
        value.value = [re1, re2];
      } else {
        value.value = getTime(_val);
      }
    }
  };
  return {
    mytime,
    myChange
  };
};
const _sfc_main$v = defineComponent({
  name: "nf-el-from-item-time-picker",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    showFormat: {
      type: String,
      default: "HH:mm:ss"
    },
    returnFormat: {
      type: String,
      default: "HH:mm:ss"
    },
    colName: String,
    modelValue: [String, Date]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    const { mytime, myChange } = timeManage$1(value);
    return {
      mytime,
      myChange
    };
  }
});
function _sfc_render$t(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_time_picker = resolveComponent("el-time-picker");
  return openBlock(), createBlock(_component_el_time_picker, mergeProps({
    modelValue: _ctx.mytime,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.mytime = $event)
  }, _ctx.$attrs, {
    name: "c" + _ctx.formItemMeta.columnId,
    clearable: _ctx.clearable,
    "is-range": _ctx.formItemMeta.controlType === 131,
    format: _ctx.showFormat,
    onChange: _ctx.myChange
  }), null, 16, ["modelValue", "name", "clearable", "is-range", "format", "onChange"]);
}
var nfTimePicker = /* @__PURE__ */ _export_sfc(_sfc_main$v, [["render", _sfc_render$t], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-time-picker.vue"]]);

const timeManage = (value) => {
  const mytime = ref("");
  if (value.value !== null) {
    mytime.value = value.value;
  }
  watch(() => value.value, (v1, v2) => {
    mytime.value = v1;
  });
  const myChange = (_val) => {
    value.value = _val;
  };
  return {
    mytime,
    myChange
  };
};
const _sfc_main$u = defineComponent({
  name: "el-from-item-time-select",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    showFormat: {
      type: String,
      default: "HH:mm"
    },
    returnFormat: {
      type: String,
      default: "HH:mm"
    },
    modelValue: [String]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    const { mytime, myChange } = timeManage(value);
    return {
      mytime,
      myChange
    };
  }
});
function _sfc_render$s(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_time_select = resolveComponent("el-time-select");
  return openBlock(), createBlock(_component_el_time_select, mergeProps({
    modelValue: _ctx.mytime,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.mytime = $event)
  }, _ctx.$attrs, {
    name: "c" + _ctx.formItemMeta.columnId,
    clearable: _ctx.clearable,
    format: _ctx.showFormat,
    onChange: _ctx.myChange
  }), null, 16, ["modelValue", "name", "clearable", "format", "onChange"]);
}
var nfTimeSelect = /* @__PURE__ */ _export_sfc(_sfc_main$u, [["render", _sfc_render$s], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/d-time-select.vue"]]);

const _sfc_main$t = defineComponent({
  name: "nf-el-from-item-switch",
  inheritAttrs: false,
  components: {
    CircleCloseFilled
  },
  props: {
    ...itemProps,
    colName: String,
    modelValue: [String, Number, Boolean]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      value
    } = itemController(props, context.emit);
    const myclear = () => {
      value.value = false;
      nextTick(() => {
        setTimeout(() => {
          value.value = void 0;
        }, 300);
      });
    };
    return {
      value,
      myclear
    };
  }
});
function _sfc_render$r(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_switch = resolveComponent("el-switch");
  const _component_circle_close_filled = resolveComponent("circle-close-filled");
  return openBlock(), createElementBlock(Fragment, null, [
    createVNode(_component_el_switch, mergeProps({
      modelValue: _ctx.value,
      "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
    }, _ctx.$attrs, {
      id: "c" + _ctx.formItemMeta.columnId,
      name: "c" + _ctx.formItemMeta.columnId,
      clearable: _ctx.clearable
    }), null, 16, ["modelValue", "id", "name", "clearable"]),
    _ctx.clearable ? (openBlock(), createBlock(_component_circle_close_filled, {
      key: 0,
      style: { "width": "1.2em", "height": "1.2em", "margin-top": "0px", "margin-left": "5px", "cursor": "pointer" },
      onClick: withModifiers(_ctx.myclear, ["stop"])
    }, null, 8, ["onClick"])) : createCommentVNode("v-if", true)
  ], 64);
}
var nfSwitch = /* @__PURE__ */ _export_sfc(_sfc_main$t, [["render", _sfc_render$r], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s1-switch.vue"]]);

const _sfc_main$s = defineComponent({
  name: "nf-el-from-item-checkbox",
  inheritAttrs: false,
  components: {
    CircleCloseFilled
  },
  props: {
    ...itemProps,
    colName: String,
    modelValue: [Boolean, String]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      value
    } = itemController(props, context.emit);
    const myclear = () => {
      value.value = false;
      nextTick(() => {
        setTimeout(() => {
          value.value = null;
        }, 300);
      });
    };
    return {
      value,
      myclear
    };
  }
});
function _sfc_render$q(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_checkbox = resolveComponent("el-checkbox");
  const _component_circle_close_filled = resolveComponent("circle-close-filled");
  return openBlock(), createElementBlock(Fragment, null, [
    createVNode(_component_el_checkbox, mergeProps({
      modelValue: _ctx.value,
      "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
    }, _ctx.$attrs, {
      id: "c" + _ctx.formItemMeta.columnId,
      name: "c" + _ctx.formItemMeta.columnId,
      clearable: _ctx.clearable
    }), {
      default: withCtx(() => [
        createTextVNode(toDisplayString(_ctx.title), 1)
      ]),
      _: 1
    }, 16, ["modelValue", "id", "name", "clearable"]),
    _ctx.clearable ? (openBlock(), createBlock(_component_circle_close_filled, {
      key: 0,
      style: { "width": "1.2em", "height": "1.2em", "margin-top": "0px", "margin-left": "5px", "cursor": "pointer" },
      onClick: withModifiers(_ctx.myclear, ["stop"])
    }, null, 8, ["onClick"])) : createCommentVNode("v-if", true)
  ], 64);
}
var nfCheckbox = /* @__PURE__ */ _export_sfc(_sfc_main$s, [["render", _sfc_render$q], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s1-checkbox.vue"]]);

const _sfc_main$r = defineComponent({
  name: "nf-el-from-item-checkboxs",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    colName: String,
    modelValue: [Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { value } = itemController(props, context.emit);
    const checkValue = computed({
      set: (v) => {
        value.value = v;
      },
      get: () => {
        if (typeof value.value !== "object") {
          return [];
        }
        return value.value;
      }
    });
    const { loadDict } = loadController(props);
    loadDict();
    return {
      checkValue
    };
  }
});
function _sfc_render$p(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_checkbox = resolveComponent("el-checkbox");
  const _component_el_checkbox_group = resolveComponent("el-checkbox-group");
  return openBlock(), createBlock(_component_el_checkbox_group, mergeProps({
    modelValue: _ctx.checkValue,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.checkValue = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    clearable: _ctx.clearable
  }), {
    default: withCtx(() => [
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.optionList, (item) => {
        return openBlock(), createBlock(_component_el_checkbox, mergeProps(_ctx.$attrs, {
          key: "check" + item.value,
          label: item.value,
          disabled: item.disabled
        }), {
          default: withCtx(() => [
            createTextVNode(toDisplayString(item.label), 1)
          ]),
          _: 2
        }, 1040, ["label", "disabled"]);
      }), 128))
    ]),
    _: 1
  }, 16, ["modelValue", "id", "name", "clearable"]);
}
var nfCheckboxs = /* @__PURE__ */ _export_sfc(_sfc_main$r, [["render", _sfc_render$p], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s2-checkboxs.vue"]]);

const _sfc_main$q = defineComponent({
  name: "nf-el-from-item-radios",
  inheritAttrs: false,
  components: {
    CircleCloseFilled
  },
  props: {
    ...itemProps,
    colName: String,
    modelValue: [Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const { loadDict } = loadController(props);
    loadDict();
    const {
      value
    } = itemController(props, context.emit);
    const myclear = () => {
      value.value = null;
    };
    return {
      value,
      myclear
    };
  }
});
function _sfc_render$o(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_radio = resolveComponent("el-radio");
  const _component_el_radio_group = resolveComponent("el-radio-group");
  const _component_circle_close_filled = resolveComponent("circle-close-filled");
  return openBlock(), createElementBlock(Fragment, null, [
    createVNode(_component_el_radio_group, mergeProps({
      modelValue: _ctx.value,
      "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
    }, _ctx.$attrs, {
      id: "c" + _ctx.formItemMeta.columnId,
      name: "c" + _ctx.formItemMeta.columnId,
      title: _ctx.title,
      clearable: _ctx.clearable
    }), {
      default: withCtx(() => [
        (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.optionList, (item) => {
          return openBlock(), createBlock(_component_el_radio, mergeProps(_ctx.$attrs, {
            key: "radio" + item.value,
            label: item.value,
            disabled: item.disabled
          }), {
            default: withCtx(() => [
              createTextVNode(toDisplayString(item.label), 1)
            ]),
            _: 2
          }, 1040, ["label", "disabled"]);
        }), 128))
      ]),
      _: 1
    }, 16, ["modelValue", "id", "name", "title", "clearable"]),
    _ctx.clearable ? (openBlock(), createBlock(_component_circle_close_filled, {
      key: 0,
      style: { "width": "1.2em", "height": "1.2em", "margin-top": "0px", "margin-left": "5px", "cursor": "pointer" },
      onClick: withModifiers(_ctx.myclear, ["stop"])
    }, null, 8, ["onClick"])) : createCommentVNode("v-if", true)
  ], 64);
}
var nfRadios = /* @__PURE__ */ _export_sfc(_sfc_main$q, [["render", _sfc_render$o], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s2-radios.vue"]]);

const _sfc_main$p = defineComponent({
  name: "nf-el-from-select",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    "collapse-tags": {
      type: Boolean,
      default: true
    },
    "collapse-tags-tooltip": {
      type: Boolean,
      default: true
    },
    colName: String,
    modelValue: [String, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const multiple = computed(() => props.formItemMeta.controlType === 161);
    const { loadDict } = loadController(props);
    loadDict();
    return {
      multiple,
      ...itemController(props, context.emit)
    };
  }
});
function _sfc_render$n(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_option = resolveComponent("el-option");
  const _component_el_select = resolveComponent("el-select");
  return openBlock(), createBlock(_component_el_select, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    clearable: _ctx.clearable,
    multiple: _ctx.multiple,
    "collapse-tags": _ctx.collapseTags,
    "collapse-tags-tooltip": _ctx.collapseTagsTooltip
  }), {
    default: withCtx(() => [
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.optionList, (item) => {
        return openBlock(), createBlock(_component_el_option, {
          key: "select" + item.value,
          label: item.label,
          value: item.value,
          disabled: item.disabled
        }, null, 8, ["label", "value", "disabled"]);
      }), 128))
    ]),
    _: 1
  }, 16, ["modelValue", "id", "name", "clearable", "multiple", "collapse-tags", "collapse-tags-tooltip"]);
}
var nfSelect = /* @__PURE__ */ _export_sfc(_sfc_main$p, [["render", _sfc_render$n], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s-select.vue"]]);

const _sfc_main$o = defineComponent({
  name: "nf-el-from-select-group",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    "collapse-tags": {
      type: Boolean,
      default: true
    },
    "collapse-tags-tooltip": {
      type: Boolean,
      default: true
    },
    colName: String,
    modelValue: [String, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const multiple = computed(() => props.formItemMeta.controlType === 163);
    const { loadDict } = loadController(props);
    loadDict();
    return {
      multiple,
      ...itemController(props, context.emit)
    };
  }
});
function _sfc_render$m(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_option = resolveComponent("el-option");
  const _component_el_option_group = resolveComponent("el-option-group");
  const _component_el_select = resolveComponent("el-select");
  return openBlock(), createBlock(_component_el_select, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    clearable: _ctx.clearable,
    multiple: _ctx.multiple,
    "collapse-tags": _ctx.collapseTags,
    "collapse-tags-tooltip": _ctx.collapseTagsTooltip
  }), {
    default: withCtx(() => [
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.optionList, (group) => {
        return openBlock(), createBlock(_component_el_option_group, {
          key: group.label,
          label: group.label
        }, {
          default: withCtx(() => [
            (openBlock(true), createElementBlock(Fragment, null, renderList(group.options, (item) => {
              return openBlock(), createBlock(_component_el_option, {
                key: item.value,
                label: "\u3000" + item.label,
                value: item.value,
                disabled: item.disabled
              }, null, 8, ["label", "value", "disabled"]);
            }), 128))
          ]),
          _: 2
        }, 1032, ["label"]);
      }), 128))
    ]),
    _: 1
  }, 16, ["modelValue", "id", "name", "clearable", "multiple", "collapse-tags", "collapse-tags-tooltip"]);
}
var nfSelectGroup = /* @__PURE__ */ _export_sfc(_sfc_main$o, [["render", _sfc_render$m], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s-select-group.vue"]]);

const cascaderManage = (props) => {
  const levelName = props.formItemMeta.colName.split("_");
  const noLazy = {
    lazy: false
  };
  reactive({
    lazy: true,
    lazyLoad(node, resolve) {
      console.log(node);
      const { level } = node;
      if (levelName.length >= level) {
        levelName[level];
        const newNode = [{
          value: "222",
          label: "\u9009\u987911",
          leaf: true
        }];
        resolve(newNode);
      } else {
        resolve([{
          value: "22",
          label: "\u9009\u987911",
          leaf: true
        }]);
      }
    }
  });
  return { propsCascader: noLazy };
};
const _sfc_main$n = defineComponent({
  name: "el-from-item-select-cascader",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    optionKind: {
      type: String,
      default: "deep"
    },
    cascaderProps: {
      type: Object,
      default: () => {
        return {};
      }
    },
    colName: String,
    modelValue: [String, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const {
      propsCascader
    } = cascaderManage(props);
    const { value } = itemController(props, context.emit);
    const filterMethod = (node, value2) => {
    };
    const optionList = props.optionList;
    const myoptionList = computed(() => {
      if (Array.isArray(optionList)) {
        if (optionList.length === 0)
          return optionList;
        if (optionList[0].children)
          return optionList;
        if (typeof optionList[0].parentId !== "undefined")
          return getChildrenOne(optionList, "", 0);
        if (typeof optionList[0] === "object") {
          return shallowToDeep(optionList[0]);
        }
      }
    });
    const { loadDict } = loadController(props);
    loadDict();
    return {
      propsCascader,
      myoptionList,
      filterMethod,
      value
    };
  }
});
function _sfc_render$l(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_cascader = resolveComponent("el-cascader");
  return openBlock(), createBlock(_component_el_cascader, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    clearable: _ctx.clearable,
    options: _ctx.myoptionList,
    "filter-method": _ctx.filterMethod,
    filterable: "",
    props: _ctx.propsCascader
  }), null, 16, ["modelValue", "clearable", "options", "filter-method", "props"]);
}
var nfSelectCascader = /* @__PURE__ */ _export_sfc(_sfc_main$n, [["render", _sfc_render$l], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s-select-cascader.vue"]]);

const _sfc_main$m = defineComponent({
  name: "nf-el-from-select-tree",
  inheritAttrs: false,
  components: {},
  props: {
    ...itemProps,
    "collapse-tags": {
      type: Boolean,
      default: true
    },
    "collapse-tags-tooltip": {
      type: Boolean,
      default: true
    },
    colName: String,
    modelValue: [String, Number, Array]
  },
  emits: ["update:modelValue"],
  setup(props, context) {
    const multiple = computed(() => props.formItemMeta.controlType === 166);
    const { loadDict } = loadController(props);
    loadDict();
    return {
      multiple,
      ...itemController(props, context.emit)
    };
  }
});
function _sfc_render$k(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_option = resolveComponent("el-option");
  const _component_el_select = resolveComponent("el-select");
  return openBlock(), createBlock(_component_el_select, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.value = $event)
  }, _ctx.$attrs, {
    id: "c" + _ctx.formItemMeta.columnId,
    name: "c" + _ctx.formItemMeta.columnId,
    clearable: _ctx.clearable,
    multiple: _ctx.multiple,
    "collapse-tags": _ctx.collapseTags,
    "collapse-tags-tooltip": _ctx.collapseTagsTooltip
  }), {
    default: withCtx(() => [
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.optionList, (item) => {
        return openBlock(), createBlock(_component_el_option, {
          key: "select__" + item.value,
          label: "\u3000".repeat(item.level) + item.label,
          value: item.value,
          disabled: item.disabled
        }, null, 8, ["label", "value", "disabled"]);
      }), 128))
    ]),
    _: 1
  }, 16, ["modelValue", "id", "name", "clearable", "multiple", "collapse-tags", "collapse-tags-tooltip"]);
}
var nfSelectTree = /* @__PURE__ */ _export_sfc(_sfc_main$m, [["render", _sfc_render$k], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/s-select-tree.vue"]]);

const _sfc_main$l = defineComponent({
    name: 'nf-el-form-item-upload-file',
    components: {
      // ElUpload, ElButton
    },
    inheritAttrs: false,
    props: {
      ...itemProps, // 基础属性
      accept: {
        type: [String, Array],
        default: ''
      },
      action: {
        type: [String],
        default: 'https://aaa.com/posts/'
      },
      modelValue: [String]
    },
    emits: ['update:modelValue'],
    setup (props, context) {
      // console.log('props-text', props)
      const {
        value,
        run,
        clear,
        myinput
      } = itemController(props, context.emit);

      // 转换允许上传的类型
      const myAccept = computed(() => {
        if (Array.isArray(props.accept)) {
          return props.accept.join(',')
        } else {
          return props.accept
        }
      });

      const fileList = reactive([
        {
          name: 'food.jpeg',
          url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
        },
        {
          name: 'food2.jpeg',
          url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
        },
      ]);

      // 重新选择文件
      const myChange = (file, fileList) => {
        console.log(file, fileList);
      };

      // 删除文件
      const handleRemove = (file, fileList) => {
        console.log(file, fileList);
      };
      const beforeRemove = (file, fileList) => {
        return ElMessageBox.confirm(`真的要删除 ${file.name} 文件吗？`)
      };
      
      const handlePreview = (file) => {
        console.log(file);
      };
      const handleExceed = (files, fileList) => {
        ElMessage.warning(
          `一次只能同时上传 3 个文件, 您选择的文件数量 ${files.length} 超出限制，
          ${files.length + fileList.length} `
        );
      };
      
      return {
        fileList,
        handleRemove,
        handlePreview,
        handleExceed,
        myChange,
        beforeRemove,
        value,
        myAccept, // 允许上传的文件类型
        run,
        clear,
        myinput
      }
    }
  });

const _hoisted_1$b = /*#__PURE__*/createTextVNode("点击选择文件");
const _hoisted_2$7 = /*#__PURE__*/createElementVNode("div", { class: "el-upload__tip" }, " 文件大小不能超过 500KB ", -1 /* HOISTED */);

function _sfc_render$j(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_button = resolveComponent("el-button");
  const _component_el_upload = resolveComponent("el-upload");

  return (openBlock(), createBlock(_component_el_upload, mergeProps({
    class: "upload-demo",
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => ((_ctx.value) = $event))
  }, _ctx.$attrs, {
    id: 'c' + _ctx.formItemMeta.columnId,
    name: 'c' + _ctx.formItemMeta.columnId,
    accept: _ctx.myAccept,
    action: _ctx.action,
    "on-preview": _ctx.handlePreview,
    "on-remove": _ctx.handleRemove,
    "on-change": _ctx.myChange,
    "before-remove": _ctx.beforeRemove,
    "on-exceed": _ctx.handleExceed,
    "file-list": _ctx.fileList
  }), {
    tip: withCtx(() => [
      _hoisted_2$7
    ]),
    default: withCtx(() => [
      createVNode(_component_el_button, { type: "primary" }, {
        default: withCtx(() => [
          _hoisted_1$b
        ]),
        _: 1 /* STABLE */
      })
    ]),
    _: 1 /* STABLE */
  }, 16 /* FULL_PROPS */, ["modelValue", "id", "name", "accept", "action", "on-preview", "on-remove", "on-change", "before-remove", "on-exceed", "file-list"]))
}
var nfFile = /*#__PURE__*/_export_sfc(_sfc_main$l, [['render',_sfc_render$j],['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/u-file.vue"]]);

const _sfc_main$k = defineComponent({
    name: 'nf-el-form-item-upload-picture',
    inheritAttrs: false,
    components: {
      // ElUpload, ElButton
    },
    props: {
      ...itemProps, // 基础属性
      accept: {
        type: [String, Array],
        default: ''
      },
      action: {
        type: [String],
        default: 'https://aaa.com/posts/'
      },
      modelValue: [String]
    },
    emits: ['update:modelValue'],
    setup (props, context) {
      // console.log('props-text', props)
      // console.log('props-ctx', context)
      const {
        value,
        run,
        clear,
        myinput
      } = itemController(props, context.emit);

      // 转换允许上传的类型
      const myAccept = computed(() => {
        if (Array.isArray(props.accept)) {
          return props.accept.join(',')
        } else {
          return props.accept
        }
      });

      const fileList = reactive([
        {
          name: 'food.jpeg',
          url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
        },
        {
          name: 'food2.jpeg',
          url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
        },
      ]);

      // 重新选择文件
      const myChange = (file, fileList) => {
        console.log(file, fileList);
      };

      // 删除文件
      const handleRemove = (file, fileList) => {
        console.log(file, fileList);
      };
      const beforeRemove = (file, fileList) => {
        return ElMessageBox.confirm(`真的要删除 ${file.name} 文件吗？`)
      };
      
      const handlePreview = (file) => {
        console.log(file);
      };
      
      const handleExceed = (files, fileList) => {
        ElMessage.warning(
          `一次只能同时上传 3 个文件, 您选择的文件数量 ${files.length} 超出限制，
          ${files.length + fileList.length} `
        );
      };

      return {
        fileList,
        handleRemove,
        handlePreview,
        handleExceed,
        myChange,
        beforeRemove,
        value,
        myAccept, // 允许上传的文件类型
        // myvideo, // 视频
        run,
        clear,
        myinput
      }
    }
  });

const _hoisted_1$a = /*#__PURE__*/createTextVNode("点击选择文件");
const _hoisted_2$6 = /*#__PURE__*/createElementVNode("div", { class: "el-upload__tip" }, " 图片大小不能超过 500KB ", -1 /* HOISTED */);

function _sfc_render$i(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_button = resolveComponent("el-button");
  const _component_el_upload = resolveComponent("el-upload");

  return (openBlock(), createBlock(_component_el_upload, mergeProps({
    modelValue: _ctx.value,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => ((_ctx.value) = $event))
  }, _ctx.$attrs, {
    id: 'c' + _ctx.formItemMeta.columnId,
    name: 'c' + _ctx.formItemMeta.columnId,
    accept: _ctx.myAccept,
    action: _ctx.action,
    "on-preview": _ctx.handlePreview,
    "on-remove": _ctx.handleRemove,
    "on-change": _ctx.myChange,
    "before-remove": _ctx.beforeRemove,
    "on-exceed": _ctx.handleExceed,
    "file-list": _ctx.fileList
  }), {
    tip: withCtx(() => [
      _hoisted_2$6
    ]),
    default: withCtx(() => [
      createVNode(_component_el_button, { type: "primary" }, {
        default: withCtx(() => [
          _hoisted_1$a
        ]),
        _: 1 /* STABLE */
      })
    ]),
    _: 1 /* STABLE */
  }, 16 /* FULL_PROPS */, ["modelValue", "id", "name", "accept", "action", "on-preview", "on-remove", "on-change", "before-remove", "on-exceed", "file-list"]))
}
var nfPicture = /*#__PURE__*/_export_sfc(_sfc_main$k, [['render',_sfc_render$i],['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/u-picture.vue"]]);

const _sfc_main$j = defineComponent({
    name: 'nf-el-form-item-upload-video',
    inheritAttrs: false,
    components: {
      // ElUpload, ElButton
    },
    props: {
      ...itemProps, // 基础属性
      accept: {
        type: [String, Array],
        default: ''
      },
      modelValue: [String]
    },
    emits: ['update:modelValue'],
    setup (props, context) {
      // console.log('props-text', props)
      // console.log('props-ctx', context)
      const {
        value,
        run,
        clear,
        myinput
      } = itemController(props, context.emit);

      // 转换允许上传的类型
      const myAccept = computed(() => {
        if (Array.isArray(props.accept)) {
          return props.accept.join(',')
        } else {
          return props.accept
        }
      });

      const fileList = reactive([
        {
          name: 'food.jpeg',
          url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
        },
        {
          name: 'food2.jpeg',
          url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
        },
      ]);

      const myvideo = ref('');

      const myChange = (file, fileList) => {
        console.log(file, fileList);
        const url = window.URL.createObjectURL(file.raw);
        myvideo.value = url;
        document.getElementById('vv').src = url;
      };

      const handleRemove = (file, fileList) => {
        console.log(file, fileList);
      };
      const beforeRemove = (file, fileList) => {
        return ElMessageBox.confirm(`真的要删除 ${file.name} 文件吗？`)
      };
      
      const handlePreview = (file) => {
        console.log(file);
        myvideo.value = file;
      };
      
      const handleExceed = (files, fileList) => {
        ElMessage.warning(
          `The limit is 3, you selected ${files.length} files this time, add up to ${
            files.length + fileList.length
          } totally`
        );
      };
      
      return {
        fileList,
        handleRemove,
        handlePreview,
        handleExceed,
        myChange,
        beforeRemove,
        value,
        myAccept, // 允许上传的文件类型
        myvideo, // 视频
        run,
        clear,
        myinput
      }
    }
  });

const _hoisted_1$9 = /*#__PURE__*/createTextVNode("Click to upload");
const _hoisted_2$5 = /*#__PURE__*/createElementVNode("div", { class: "el-upload__tip" }, " jpg/png files with a size less than 500kb ", -1 /* HOISTED */);
const _hoisted_3$2 = /*#__PURE__*/createElementVNode("video", {
  width: "320",
  height: "240",
  id: "vv",
  controls: ""
}, " 您的浏览器不支持Video标签。 ", -1 /* HOISTED */);

function _sfc_render$h(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_button = resolveComponent("el-button");
  const _component_el_upload = resolveComponent("el-upload");

  return (openBlock(), createElementBlock(Fragment, null, [
    createVNode(_component_el_upload, mergeProps({
      modelValue: _ctx.value,
      "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => ((_ctx.value) = $event))
    }, _ctx.$attrs, {
      id: 'c' + _ctx.formItemMeta.columnId,
      name: 'c' + _ctx.formItemMeta.columnId,
      accept: _ctx.myAccept,
      action: _ctx.action,
      "on-preview": _ctx.handlePreview,
      "on-remove": _ctx.handleRemove,
      "on-change": _ctx.myChange,
      "before-remove": _ctx.beforeRemove,
      "on-exceed": _ctx.handleExceed,
      "file-list": _ctx.fileList
    }), {
      tip: withCtx(() => [
        _hoisted_2$5
      ]),
      default: withCtx(() => [
        createVNode(_component_el_button, { type: "primary" }, {
          default: withCtx(() => [
            _hoisted_1$9
          ]),
          _: 1 /* STABLE */
        }),
        _hoisted_3$2
      ]),
      _: 1 /* STABLE */
    }, 16 /* FULL_PROPS */, ["modelValue", "id", "name", "accept", "action", "on-preview", "on-remove", "on-change", "before-remove", "on-exceed", "file-list"]),
    createTextVNode(toDisplayString(_ctx.myvideo), 1 /* TEXT */)
  ], 64 /* STABLE_FRAGMENT */))
}
var nfVideo = /*#__PURE__*/_export_sfc(_sfc_main$j, [['render',_sfc_render$h],['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form-item/u-video.vue"]]);

const AllFormItem = {
  nfText,
  nfArea,
  nfPassword,
  nfUrl,
  nfAutocomplete,
  nfColor,
  nfNumber,
  nfRange,
  nfRate,
  nfDate,
  nfDatetime,
  nfMonth,
  nfYear,
  nfWeek,
  nfDates,
  nfTimePicker,
  nfTimeSelect,
  nfFile,
  nfPicture,
  nfVideo,
  nfSwitch,
  nfCheckbox,
  nfCheckboxs,
  nfRadios,
  nfSelect,
  nfSelectGroup,
  nfSelectCascader,
  nfSelectTree
};
const formItemKey = {
  100: nfArea,
  101: nfText,
  102: nfPassword,
  103: nfText,
  104: nfText,
  105: nfUrl,
  106: nfText,
  107: nfAutocomplete,
  108: nfColor,
  110: nfNumber,
  111: nfRange,
  112: nfRate,
  120: nfDate,
  121: nfDatetime,
  122: nfMonth,
  123: nfWeek,
  124: nfYear,
  125: nfDate,
  126: nfDatetime,
  127: nfMonth,
  128: nfDates,
  130: nfTimePicker,
  131: nfTimePicker,
  132: nfTimeSelect,
  133: nfTimeSelect,
  140: nfFile,
  141: nfPicture,
  142: nfVideo,
  150: nfCheckbox,
  151: nfSwitch,
  152: nfCheckboxs,
  153: nfRadios,
  160: nfSelect,
  161: nfSelect,
  162: nfSelectGroup,
  163: nfSelectGroup,
  164: nfSelectCascader,
  165: nfSelectTree,
  166: nfSelectTree
};

function createFindModel(state) {
  const findModel = reactive({});
  const { itemMeta } = state;
  for (const key in itemMeta) {
    const m = itemMeta[key].formItemMeta;
    findModel[m.columnId] = {
      colName: m.colName,
      dateKind: 0,
      findKind: [],
      useKind: 21,
      controlKind: [],
      useControlKind: "",
      value: "",
      valueStart: "",
      valueEnd: ""
    };
    switch (m.controlType) {
      case 102:
        findModel[m.columnId].findKind = [];
        findModel[m.columnId].value = "";
        break;
      case 100:
      case 101:
      case 103:
      case 104:
      case 105:
      case 106:
        findModel[m.columnId].findKind = [21, 22, 23, 24, 25, 26, 27, 28];
        findModel[m.columnId].value = "";
        break;
      case 107:
        findModel[m.columnId].findKind = [21, 22];
        findModel[m.columnId].value = "";
        break;
      case 108:
        findModel[m.columnId].findKind = [21, 23];
        findModel[m.columnId].value = "";
        break;
      case 140:
      case 141:
      case 142:
        findModel[m.columnId].findKind = [21, 23, 25, 26];
        findModel[m.columnId].value = "";
        break;
      case 110:
      case 111:
      case 112:
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 22, 18, 19];
        findModel[m.columnId].value = null;
        findModel[m.columnId].valueStart = null;
        findModel[m.columnId].valueEnd = null;
        break;
      case 120:
      case 121:
      case 122:
      case 123:
      case 124:
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 18, 19];
        findModel[m.columnId].value = "";
        findModel[m.columnId].controlKind = ["\u65E5\u671F", "\u5E74\u6708", "\u5E74"];
        break;
      case 125:
      case 126:
      case 127:
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 18, 19];
        findModel[m.columnId].value = [];
        break;
      case 128:
        findModel[m.columnId].findKind = [21, 41];
        findModel[m.columnId].value = [];
        break;
      case 130:
      case 131:
      case 132:
      case 133:
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 18, 19];
        findModel[m.columnId].value = "";
        break;
      case 150:
      case 151:
        findModel[m.columnId].findKind = [21];
        findModel[m.columnId].value = null;
        break;
      case 152:
      case 161:
        findModel[m.columnId].findKind = [21, 41, 42];
        findModel[m.columnId].value = [];
        findModel[m.columnId].controlKind = ["\u591A\u9009", "\u4E0B\u62C9\u591A\u9009"];
        break;
      case 153:
      case 160:
        findModel[m.columnId].findKind = [21, 22, 41, 42];
        findModel[m.columnId].value = null;
        findModel[m.columnId].controlKind = ["\u591A\u9009", "\u5355\u9009", "\u4E0B\u62C9"];
        break;
      case 162:
        findModel[m.columnId].findKind = [21, 22];
        findModel[m.columnId].value = null;
        break;
      case 164:
        findModel[m.columnId].findKind = [21, 41, 42];
        findModel[m.columnId].value = [];
        break;
      case 165:
        findModel[m.columnId].findKind = [21, 22];
        findModel[m.columnId].value = [];
        break;
      case 163:
      case 166:
        findModel[m.columnId].findKind = [21, 41, 42];
        findModel[m.columnId].value = [];
        break;
    }
  }
  return findModel;
}

const typeName = (val) => {
  return Object.prototype.toString.call(val).replace(/^\[object (\S+)\]$/, "$1").toLowerCase();
};

const _isEmpty = (val) => {
  switch (typeName(val)) {
    case null:
    case "null":
    case "undefined":
      return true;
    case "string":
      return val.length === 0;
    case "number":
    case "bigInt":
      return isNaN(val);
    case "array":
      return val.length === 0;
    default:
      return false;
  }
};
const _selectCascader = (colName, model, state) => {
  const colNames = colName.split("_");
  colNames.forEach((col, index) => {
    if (index < model.value.length) {
      state.querys.push({
        colName: col,
        findKind: model.useKind,
        value: model.value[index],
        valueStart: "",
        valueEnd: ""
      });
      state.queryMini[col] = [model.useKind, model.value[index]];
    } else {
      delete state.queryMini[col];
    }
  });
};
const _selectCascader2 = (colName, model, state) => {
  const colNames = colName.split("_");
  state.querys.push({
    colName: colNames[0],
    findKind: model.useKind,
    value: "",
    valueStart: model.value[0],
    valueEnd: model.value[1]
  });
  state.queryMini[colNames[0]] = [model.useKind, [model.value[0], model.value[1]]];
  state.querys.push({
    colName: colNames[1],
    findKind: model.useKind,
    value: "",
    valueStart: model.valueEnd[0],
    valueEnd: model.valueEnd[1]
  });
  state.queryMini[colNames[1]] = [model.useKind, [model.valueEnd[0], model.valueEnd[1]]];
};
function createQuery(state) {
  state.querys.length = 0;
  for (const key in state.queryMini) {
    delete state.queryMini[key];
  }
  const tmpFindCol = [];
  for (const [key, model] of Object.entries(state.findModel)) {
    const {
      colName,
      controlType
    } = state.itemMeta[key]?.formItemMeta;
    if (model.useKind >= 17 && model.useKind <= 19) {
      if (_isEmpty(model.value) || _isEmpty(model.valueEnd)) ; else {
        tmpFindCol.push(key);
        if (colName.split("_").length > 1) {
          _selectCascader2(colName, model, state);
        } else {
          state.querys.push({
            colName,
            findKind: model.useKind,
            value: "",
            valueStart: model.value,
            valueEnd: model.valueEnd
          });
          state.queryMini[colName] = [model.useKind, [model.value, model.valueEnd]];
        }
      }
    } else {
      if (_isEmpty(model.value)) ; else {
        if (model.value) {
          tmpFindCol.push(key);
          if (colName.split("_").length > 1) {
            _selectCascader(colName, model, state);
          } else {
            state.querys.push({
              colName,
              findKind: model.useKind,
              value: model.value,
              valueStart: "",
              valueEnd: ""
            });
            state.queryMini[colName] = [model.useKind, model.value];
          }
        }
      }
    }
    if (state.showMoreFind && tmpFindCol.length > 0) {
      state.tmpQuickFind.length = 0;
      state.tmpQuickFind.push(...tmpFindCol);
    }
  }
}

const flag = Symbol("find-control");
function regFindState(props) {
  const state = defineStore(flag, {
    state: () => {
      return {
        showMoreFind: false,
        findModel: {},
        queryMini: {},
        querys: [],
        itemMeta: props.itemMeta,
        quickFind: props.findMeta.quickFind,
        tmpQuickFind: [],
        allFind: props.findMeta.allFind,
        customer: [],
        customerDefault: props.findMeta.customerDefault
      };
    },
    getters: {},
    actions: {}
  }, { isLocal: true });
  state.findModel = createFindModel(state);
  state.queryMini = props.queryMini;
  state.querys = props.querys;
  state.tmpQuickFind.push(...state.quickFind);
  state.customer = Array.isArray(props.findMeta.customer) ? props.findMeta.customer : [];
  watch(state.findModel, () => {
    createQuery(state);
  });
  return state;
}
const getFindState = () => {
  return useStoreLocal(flag);
};

const findItemDict = {
  100: AllFormItem.nfText,
  101: AllFormItem.nfText,
  102: AllFormItem.nfText,
  103: AllFormItem.nfText,
  104: AllFormItem.nfText,
  105: AllFormItem.nfText,
  106: AllFormItem.nfText,
  107: AllFormItem.nfAutocomplete,
  108: AllFormItem.nfText,
  110: AllFormItem.nfNumber,
  111: AllFormItem.nfNumber,
  112: AllFormItem.nfNumber,
  120: AllFormItem.nfDate,
  121: AllFormItem.nfDatetime,
  122: AllFormItem.nfMonth,
  123: AllFormItem.nfWeek,
  124: AllFormItem.nfYear,
  125: AllFormItem.nfDate,
  126: AllFormItem.nfDatetime,
  127: AllFormItem.nfMonth,
  128: AllFormItem.nfDates,
  130: AllFormItem.nfTimePicker,
  131: AllFormItem.nfTimePicker,
  132: AllFormItem.nfTimeSelect,
  133: AllFormItem.nfTimeSelect,
  140: AllFormItem.nfText,
  141: AllFormItem.nfText,
  142: AllFormItem.nfText,
  150: AllFormItem.nfCheckbox,
  151: AllFormItem.nfSwitch,
  152: AllFormItem.nfCheckboxs,
  153: AllFormItem.nfRadios,
  160: AllFormItem.nfSelect,
  161: AllFormItem.nfSelect,
  162: AllFormItem.nfSelectGroup,
  163: AllFormItem.nfSelectGroup,
  164: AllFormItem.nfSelectCascader,
  165: AllFormItem.nfSelectTree,
  166: AllFormItem.nfSelectTree
};

var sFindkind_vue_vue_type_style_index_0_lang = /* #__PURE__ */ (() => ".findkind-icon{color:#d8cec6;width:1em;height:1em;margin-left:8px;margin-right:8px;margin-top:1px}.example-showcase .el-dropdown+.el-dropdown{margin-left:15px}.example-showcase .el-dropdown-link{cursor:pointer;color:var(--el-color-primary);display:flex;align-items:center}\n")();

const ditKind = reactive({
  "": 101,
  "\u65E5\u671F": 120,
  "\u65E5\u671F\u65F6\u95F4": 121,
  "\u5E74\u6708": 122,
  "\u5E74\u5468": 123,
  "\u5E74": 124,
  "\u591A\u9009": 152,
  "\u5355\u9009": 153,
  "\u4E0B\u62C9": 160,
  "\u4E0B\u62C9\u591A\u9009": 161
});
const _sfc_main$i = defineComponent({
  name: "nf-el-find-kind",
  components: {
    Check,
    ArrowDownBold,
    ArrowRightBold
  },
  props: {
    columnId: {
      type: [Number, String],
      default: 0
    }
  },
  setup(props, context) {
    const state = getFindState();
    const kindClick = (command) => {
      if (command === command * 1) {
        state.findModel[props.columnId].useKind = command;
      } else {
        const type = ditKind[command];
        state.findModel[props.columnId].useControlKind = type;
        controlType.value = type;
        const val = state.findModel[props.columnId].value;
        switch (type) {
          case 152:
          case 161:
            if (!Array.isArray(val) && val) {
              state.findModel[props.columnId].value = [val];
            }
            break;
          default:
            if (Array.isArray(val)) {
              state.findModel[props.columnId].value = val[0];
            }
            break;
        }
      }
    };
    const isUp = ref(false);
    const mychange = (val) => {
      isUp.value = val;
    };
    const defControlType = state.itemMeta[props.columnId]?.formItemMeta?.controlType ?? 101;
    const controlType = ref(defControlType);
    return {
      ditKind,
      findKindDict,
      defControlType,
      controlType,
      isUp,
      mychange,
      kindClick,
      state
    };
  }
});
const _hoisted_1$8 = {
  class: "flex flex-wrap items-center",
  style: { "float": "left", "margin-right": "10px" }
};
const _hoisted_2$4 = { key: 0 };
function _sfc_render$g(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_arrow_down_bold = resolveComponent("arrow-down-bold");
  const _component_arrow_right_bold = resolveComponent("arrow-right-bold");
  const _component_el_button = resolveComponent("el-button");
  const _component_el_dropdown_item = resolveComponent("el-dropdown-item");
  const _component_Check = resolveComponent("Check");
  const _component_el_icon = resolveComponent("el-icon");
  const _component_el_dropdown_menu = resolveComponent("el-dropdown-menu");
  const _component_el_dropdown = resolveComponent("el-dropdown");
  return openBlock(), createElementBlock("div", _hoisted_1$8, [
    createVNode(_component_el_dropdown, {
      onCommand: _ctx.kindClick,
      onVisibleChange: _ctx.mychange
    }, {
      dropdown: withCtx(() => [
        createVNode(_component_el_dropdown_menu, null, {
          default: withCtx(() => [
            (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.state.findModel[_ctx.columnId].findKind, (kindId, index) => {
              return openBlock(), createBlock(_component_el_dropdown_item, {
                key: index,
                command: kindId
              }, {
                default: withCtx(() => [
                  createTextVNode(toDisplayString(_ctx.findKindDict[kindId].name), 1)
                ]),
                _: 2
              }, 1032, ["command"]);
            }), 128)),
            _ctx.state.findModel[_ctx.columnId].controlKind.length > 0 ? (openBlock(), createElementBlock("hr", _hoisted_2$4)) : createCommentVNode("v-if", true),
            createCommentVNode("\u53EF\u4EE5\u5207\u6362\u7684\u63A7\u4EF6"),
            (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.state.findModel[_ctx.columnId].controlKind, (kindId, index) => {
              return openBlock(), createBlock(_component_el_dropdown_item, {
                key: index,
                command: kindId
              }, {
                default: withCtx(() => [
                  createTextVNode(toDisplayString(kindId) + " \xA0 ", 1),
                  _ctx.controlType === _ctx.ditKind[kindId] ? (openBlock(), createBlock(_component_el_icon, {
                    key: 0,
                    color: "red"
                  }, {
                    default: withCtx(() => [
                      createVNode(_component_Check)
                    ]),
                    _: 1
                  })) : createCommentVNode("v-if", true)
                ]),
                _: 2
              }, 1032, ["command"]);
            }), 128))
          ]),
          _: 1
        })
      ]),
      default: withCtx(() => [
        createVNode(_component_el_button, {
          type: "primary",
          plain: "",
          style: { "width": "75px" }
        }, {
          default: withCtx(() => [
            createTextVNode(toDisplayString(_ctx.findKindDict[_ctx.state.findModel[_ctx.columnId].useKind].name) + " ", 1),
            withDirectives(createVNode(_component_arrow_down_bold, { class: "findkind-icon" }, null, 512), [
              [vShow, _ctx.isUp]
            ]),
            withDirectives(createVNode(_component_arrow_right_bold, { class: "findkind-icon" }, null, 512), [
              [vShow, !_ctx.isUp]
            ])
          ]),
          _: 1
        })
      ]),
      _: 1
    }, 8, ["onCommand", "onVisibleChange"])
  ]);
}
var findKind = /* @__PURE__ */ _export_sfc(_sfc_main$i, [["render", _sfc_render$g], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/find2/item/s-findkind.vue"]]);

const _sfc_main$h = defineComponent({
  name: "nf-el-find-item",
  components: {
    findKind
  },
  props: {
    columnId: {
      type: [Number, String],
      required: true
    }
  },
  setup(props, context) {
    const state = getFindState();
    const defControlType = state.itemMeta[props.columnId]?.formItemMeta?.controlType ?? 101;
    const controlType = ref(defControlType);
    watch(() => state.findModel[props.columnId].useControlKind, (kind) => {
      controlType.value = kind;
    });
    return {
      controlType,
      findItemDict,
      state
    };
  }
});
const _hoisted_1$7 = { key: 0 };
const _hoisted_2$3 = /* @__PURE__ */ createElementVNode("span", { style: { "float": "left" } }, "\xA0 - \xA0", -1);
function _sfc_render$f(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_find_kind = resolveComponent("find-kind");
  return openBlock(), createElementBlock(Fragment, null, [
    createCommentVNode("\u67E5\u8BE2\u5B57\u6BB5"),
    createVNode(_component_find_kind, { columnId: _ctx.columnId }, null, 8, ["columnId"]),
    createElementVNode("span", null, [
      createCommentVNode("\u4E00\u4E2A\u67E5\u8BE2\u5B57\u6BB5"),
      (openBlock(), createBlock(resolveDynamicComponent(_ctx.findItemDict[_ctx.controlType]), mergeProps({
        style: { "float": "left", "width": "140px" },
        model: _ctx.state.findModel[_ctx.columnId]
      }, _ctx.state.itemMeta[_ctx.columnId], { colName: "value" }), null, 16, ["model"]))
    ]),
    _ctx.state.findModel[_ctx.columnId].useKind >= 17 && _ctx.state.findModel[_ctx.columnId].useKind <= 19 ? (openBlock(), createElementBlock("span", _hoisted_1$7, [
      createCommentVNode("\u4E24\u4E2A\u67E5\u8BE2\u5B57\u6BB5"),
      _hoisted_2$3,
      (openBlock(), createBlock(resolveDynamicComponent(_ctx.findItemDict[_ctx.controlType]), mergeProps({
        style: { "float": "left", "width": "140px" },
        model: _ctx.state.findModel[_ctx.columnId]
      }, _ctx.state.itemMeta[_ctx.columnId], { colName: "valueEnd" }), null, 16, ["model"]))
    ])) : createCommentVNode("v-if", true)
  ], 64);
}
var findItem = /* @__PURE__ */ _export_sfc(_sfc_main$h, [["render", _sfc_render$f], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/find2/item/find-item.vue"]]);

const _sfc_main$g = defineComponent({
  name: "nf-el-find-case",
  props: {},
  setup(props, context) {
    const state = getFindState();
    const mydefault = () => {
      state.tmpQuickFind.length = 0;
      state.tmpQuickFind.push(...state.quickFind);
    };
    const mycase = (index) => {
      state.tmpQuickFind.length = 0;
      state.tmpQuickFind.push(...state.customer[index].colIds);
    };
    return {
      mydefault,
      mycase,
      state
    };
  }
});
const _hoisted_1$6 = { style: { "float": "left", "width": "90px" } };
const _hoisted_2$2 = /* @__PURE__ */ createTextVNode(" \u67E5\u8BE2\u65B9\u6848 ");
const _hoisted_3$1 = /* @__PURE__ */ createTextVNode("\u9ED8\u8BA4");
const _hoisted_4 = /* @__PURE__ */ createElementVNode("br", null, null, -1);
function _sfc_render$e(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_button = resolveComponent("el-button");
  const _component_el_popover = resolveComponent("el-popover");
  return openBlock(), createElementBlock(Fragment, null, [
    createCommentVNode("\u9009\u62E9\u67E5\u8BE2\u65B9\u6848"),
    createElementVNode("div", _hoisted_1$6, [
      createVNode(_component_el_popover, {
        placement: "bottom",
        width: 200,
        sytle: "height:400px;",
        trigger: "hover"
      }, {
        reference: withCtx(() => [
          createVNode(_component_el_button, {
            type: "primary",
            round: "",
            style: { "margin-right": "16px" }
          }, {
            default: withCtx(() => [
              _hoisted_2$2
            ]),
            _: 1
          })
        ]),
        default: withCtx(() => [
          createVNode(_component_el_button, {
            type: "",
            onClick: _ctx.mydefault
          }, {
            default: withCtx(() => [
              _hoisted_3$1
            ]),
            _: 1
          }, 8, ["onClick"]),
          _hoisted_4,
          (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.state.customer, (item, index) => {
            return openBlock(), createBlock(_component_el_button, {
              type: "",
              key: "case_" + item.id + index,
              onClick: ($event) => _ctx.mycase(index)
            }, {
              default: withCtx(() => [
                createTextVNode(toDisplayString(item.label), 1)
              ]),
              _: 2
            }, 1032, ["onClick"]);
          }), 128))
        ]),
        _: 1
      })
    ])
  ], 2112);
}
var findCase = /* @__PURE__ */ _export_sfc(_sfc_main$g, [["render", _sfc_render$e], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/find2/item/s-findcase.vue"]]);

const _sfc_main$f = defineComponent({
  name: "nf-el-find-quick",
  components: {
    findItem,
    findCase
  },
  props: {},
  setup() {
    const state = getFindState();
    return {
      state
    };
  }
});
const _hoisted_1$5 = { style: { "position": "absolute", "right": "5px", "top": "0px", "z-index": "100" } };
const _hoisted_2$1 = /* @__PURE__ */ createTextVNode("\u66F4\u591A");
const _hoisted_3 = { style: { "float": "left", "width": "90px" } };
function _sfc_render$d(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_button = resolveComponent("el-button");
  const _component_find_case = resolveComponent("find-case");
  const _component_find_item = resolveComponent("find-item");
  const _component_el_scrollbar = resolveComponent("el-scrollbar");
  const _component_el_card = resolveComponent("el-card");
  return openBlock(), createElementBlock(Fragment, null, [
    createCommentVNode("\u5FEB\u6377\u67E5\u8BE2"),
    createVNode(_component_el_card, {
      class: "box-card",
      "body-style": { padding: "7px", height: "63px" }
    }, {
      default: withCtx(() => [
        createVNode(_component_el_scrollbar, { style: { "min-width": "700px", "height": "60px" } }, {
          default: withCtx(() => [
            createElementVNode("div", _hoisted_1$5, [
              createCommentVNode("\u66F4\u591A\u67E5\u8BE2\u7684\u6309\u94AE"),
              createVNode(_component_el_button, {
                type: "primary",
                round: "",
                onClick: _cache[0] || (_cache[0] = ($event) => _ctx.state.showMoreFind = true)
              }, {
                default: withCtx(() => [
                  _hoisted_2$1
                ]),
                _: 1
              })
            ]),
            createElementVNode("div", {
              style: normalizeStyle("width: " + (160 + _ctx.state.quickFind.length * 290) + "px;")
            }, [
              createCommentVNode("\u5FEB\u6377\u67E5\u8BE2\u6309\u94AE"),
              createElementVNode("div", _hoisted_3, [
                createCommentVNode("\u9009\u62E9\u67E5\u8BE2\u65B9\u6848"),
                createVNode(_component_find_case)
              ]),
              createCommentVNode("\u5FEB\u6377\u67E5\u8BE2\u7684\u5B57\u6BB5"),
              createElementVNode("div", {
                style: normalizeStyle([{ "float": "left", "min-width": "550px" }, "width: " + _ctx.state.quickFind.length * 290 + "px;"])
              }, [
                (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.state.tmpQuickFind, (colId, index) => {
                  return openBlock(), createElementBlock("div", {
                    style: normalizeStyle("float:left;width:" + (_ctx.state.findModel[colId].useKind === 17 ? 480 : 260) + "px;padding-top:4px;"),
                    key: "quick_" + colId + "_" + index
                  }, [
                    createCommentVNode("\u67E5\u8BE2\u5B57\u6BB5"),
                    createVNode(_component_find_item, { columnId: colId }, null, 8, ["columnId"])
                  ], 4);
                }), 128))
              ], 4)
            ], 4)
          ]),
          _: 1
        })
      ]),
      _: 1
    })
  ], 2112);
}
var findQuick = /* @__PURE__ */ _export_sfc(_sfc_main$f, [["render", _sfc_render$d], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/find2/item/find-quick.vue"]]);

const _sfc_main$e = defineComponent({
  name: "nf-el-find-all-div",
  components: {
    findItem
  },
  props: {},
  setup(props, context) {
    const state = getFindState();
    return {
      state
    };
  }
});
const _hoisted_1$4 = { style: { "width": "80px", "font-size": "9pt", "text-align": "right", "float": "left", "margin-left": "5px", "margin-right": "5px", "margin-top": "5px" } };
function _sfc_render$c(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_find_item = resolveComponent("find-item");
  const _component_el_scrollbar = resolveComponent("el-scrollbar");
  const _component_el_drawer = resolveComponent("el-drawer");
  return openBlock(), createElementBlock(Fragment, null, [
    createCommentVNode("\u5168\u90E8\u67E5\u8BE2"),
    createCommentVNode("div\u5F62\u5F0F\u7684\u5168\u90E8\u67E5\u8BE2\u6761\u4EF6 border: 1px solid rgb(207, 225, 243); min-height: 33px; width: 340px;"),
    createVNode(_component_el_drawer, {
      title: "\u67E5\u8BE2\u6761\u4EF6",
      "with-header": false,
      modelValue: _ctx.state.showMoreFind,
      "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.state.showMoreFind = $event),
      direction: "ttb",
      "custom-class": "demo-drawer",
      ref: "drawer",
      size: "400px"
    }, {
      default: withCtx(() => [
        createVNode(_component_el_scrollbar, { style: { "height": "350px", "text-align": "left" } }, {
          default: withCtx(() => [
            (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.state.allFind, (ctrId, index) => {
              return openBlock(), createElementBlock("div", {
                style: normalizeStyle([{ "float": "left", "margin": "5px", "border": "1px solid rgb(207, 225, 243)", "min-height": "33px" }, "width:" + (_ctx.state.findModel[ctrId].useKind === 17 ? 680 : 340) + "px;"]),
                key: "find_" + ctrId + "_" + index
              }, [
                createElementVNode("label", _hoisted_1$4, toDisplayString(_ctx.state.itemMeta[ctrId].formItemMeta.label), 1),
                createElementVNode("div", {
                  style: normalizeStyle("width:" + (_ctx.state.findModel[ctrId].useKind === 17 ? 630 : 350) + "px;padding-top:4px;")
                }, [
                  createCommentVNode("\u67E5\u8BE2\u5B57\u6BB5"),
                  createVNode(_component_find_item, { columnId: ctrId }, null, 8, ["columnId"])
                ], 4)
              ], 4);
            }), 128))
          ]),
          _: 1
        })
      ]),
      _: 1
    }, 8, ["modelValue"])
  ], 2112);
}
var findAll = /* @__PURE__ */ _export_sfc(_sfc_main$e, [["render", _sfc_render$c], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/find2/item/find-all-div.vue"]]);

const _sfc_main$d = defineComponent({
  name: "nf-el-find-div",
  components: {
    findQuick,
    findAll
  },
  props: {
    ...findProps
  },
  setup(props) {
    const state = regFindState(props);
    return {
      state
    };
  }
});
function _sfc_render$b(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_find_quick = resolveComponent("find-quick");
  const _component_find_all = resolveComponent("find-all");
  return openBlock(), createElementBlock(Fragment, null, [
    createCommentVNode("\u5FEB\u6377\u67E5\u8BE2"),
    createVNode(_component_find_quick),
    createCommentVNode("\u66F4\u591A\u67E5\u8BE2\uFF0C\u653E\u5728\u62BD\u5C49\u91CC\u9762"),
    createVNode(_component_find_all)
  ], 64);
}
var nfFind = /* @__PURE__ */ _export_sfc(_sfc_main$d, [["render", _sfc_render$b], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/find2/find-div.vue"]]);

const _hoisted_1$3 = { style: {"float":"left"} };

/**
 * v-bind="subMenuAttrs"
 * v-bind="menuItemAttrs"
 * subMenuAttrs: Object, // 树枝的属性
 * menuItemAttrs: Object, // 树叶的属性
 */
  
const _sfc_main$c = {
  __name: 'menu-sub',
  props: {
    subMenu: Array // 要显示的菜单，可以n级
  },
  setup(__props) {

const props = __props;

  

  const _subMeny = markRaw(props.subMenu);


return (_ctx, _cache) => {
  const _component_menu_sub = resolveComponent("menu-sub", true);

  return (openBlock(true), createElementBlock(Fragment, null, renderList(unref(_subMeny), (item, index) => {
    return (openBlock(), createElementBlock(Fragment, null, [
      createCommentVNode("树枝"),
      (item.childrens && item.childrens.length > 0)
        ? (openBlock(), createBlock(unref(ElSubMenu), {
            key: item.menuId + '_' + index,
            index: item.menuId,
            style: {"vertical-align":"middle"}
          }, {
            title: withCtx(() => [
              (openBlock(), createBlock(resolveDynamicComponent(item.icon), { style: {"width":"1.5em","height":"1.5em","margin-right":"8px","vertical-align":"middle"} })),
              createElementVNode("span", null, toDisplayString(item.title), 1 /* TEXT */)
            ]),
            default: withCtx(() => [
              createVNode(_component_menu_sub, {
                subMenu: item.childrens
              }, null, 8 /* PROPS */, ["subMenu"])
            ]),
            _: 2 /* DYNAMIC */
          }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["index"]))
        : (openBlock(), createElementBlock(Fragment, { key: 1 }, [
            createCommentVNode("树叶"),
            (openBlock(), createBlock(unref(ElMenuItem), {
              index: item.menuId,
              key: item.menuId + 'son_' + index
            }, {
              title: withCtx(() => [
                createElementVNode("span", _hoisted_1$3, [
                  (openBlock(), createBlock(resolveDynamicComponent(item.icon), { style: {"width":"1.5em","height":"1.5em","margin-right":"8px","vertical-align":"middle"} })),
                  createElementVNode("span", null, toDisplayString(item.title), 1 /* TEXT */)
                ])
              ]),
              _: 2 /* DYNAMIC */
            }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["index"]))
          ], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */))
    ], 64 /* STABLE_FRAGMENT */))
  }), 256 /* UNKEYED_FRAGMENT */))
}
}

};
var subMenu = /*#__PURE__*/_export_sfc(_sfc_main$c, [['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/menu/menu-sub.vue"]]);

var menu_vue_vue_type_style_index_0_lang = /* #__PURE__ */ (() => ".el-menu-vertical-demo:not(.el-menu--collapse){max-width:220px;min-height:400px}\n")();

const _sfc_main$b = defineComponent({
  name: "nf-ui-elp-menu",
  components: {
    ElMenu,
    subMenu
  },
  props: {
    naviId: {
      type: [Number, String],
      default: "0"
    }
  },
  setup(props, context) {
    const domMenu = ref(null);
    const router = useRouter();
    const menus = markRaw([]);
    const loadMenu = (id) => {
      menus.length = 0;
      if (id) {
        const arr = router.menus.filter((item) => item.naviId == id);
        menus.push(...arr);
      } else {
        menus.push(...router.menus);
      }
    };
    watch(() => props.naviId, (id) => {
      loadMenu(id);
    }, { immediate: true });
    watch(router.menus, () => {
      loadMenu(props.naviId);
    });
    setTimeout(() => {
      const id = menus[0]?.menuId ?? "";
      if (id)
        domMenu.value.open(id);
    }, 300);
    const select = (index, indexPath) => {
      router.currentRoute.paths = indexPath;
      router.currentRoute.key = index;
    };
    return {
      domMenu,
      menus,
      select
    };
  }
});
function _sfc_render$a(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_sub_menu = resolveComponent("sub-menu");
  const _component_el_menu = resolveComponent("el-menu");
  return openBlock(), createBlock(_component_el_menu, {
    ref: "domMenu",
    class: "el-menu-vertical-demo",
    onSelect: _ctx.select,
    "background-color": "#6c747c",
    "text-color": "#fff",
    "active-text-color": "#ffd04b"
  }, {
    default: withCtx(() => [
      createVNode(_component_sub_menu, { subMenu: _ctx.menus }, null, 8, ["subMenu"])
    ]),
    _: 1
  }, 8, ["onSelect"]);
}
var nfMenu = /* @__PURE__ */ _export_sfc(_sfc_main$b, [["render", _sfc_render$a], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/menu/menu.vue"]]);

const _sfc_main$a = {};

function _sfc_render$9(_ctx, _cache) {
  return (openBlock(), createBlock(resolveDynamicComponent(_ctx.$router.getComponent())))
}
var nfRouterView = /*#__PURE__*/_export_sfc(_sfc_main$a, [['render',_sfc_render$9],['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/menu/router-view.vue"]]);

const _sfc_main$9 = {
  __name: 'router-view-tabs',
  setup(__props) {

  
return (_ctx, _cache) => {
  return (openBlock(), createBlock(unref(ElTabs), {
    modelValue: _ctx.$router.currentRoute.key,
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => ((_ctx.$router.currentRoute.key) = $event)),
    type: "border-card"
  }, {
    default: withCtx(() => [
      createVNode(unref(ElTabPane), {
        label: "桌面",
        name: "home"
      }, {
        default: withCtx(() => [
          (openBlock(), createBlock(resolveDynamicComponent(_ctx.$router.home)))
        ]),
        _: 1 /* STABLE */
      }),
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.$router.tabs, (key) => {
        return (openBlock(), createBlock(unref(ElTabPane), {
          key: key,
          label: _ctx.$router.menuList[key].title,
          name: key
        }, {
          label: withCtx(() => [
            createElementVNode("span", null, [
              createTextVNode(toDisplayString(_ctx.$router.menuList[key].title) + "   ", 1 /* TEXT */),
              createVNode(unref(CircleCloseFilled), {
                style: {"width":"1.0em","height":"1.0em","margin-top":"8px"},
                onClick: withModifiers($event => (_ctx.$router.removeTab(key)), ["stop"])
              }, null, 8 /* PROPS */, ["onClick"])
            ])
          ]),
          default: withCtx(() => [
            (openBlock(), createBlock(resolveDynamicComponent(_ctx.$router.menuList[key].component)))
          ]),
          _: 2 /* DYNAMIC */
        }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["label", "name"]))
      }), 128 /* KEYED_FRAGMENT */))
    ]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["modelValue"]))
}
}

};
var nfRouterViewTabs = /*#__PURE__*/_export_sfc(_sfc_main$9, [['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/menu/router-view-tabs.vue"]]);

let loadingInstance1 = null;

const loading = (id) => {

  const start = () => {
    loadingInstance1 = ElLoading.service({
      text: '加载中...',
      background: 'rgba(255, 255, 255, 0.8)',
      target: id
    });
  };

  const stop = () => {
    loadingInstance1.close();
  };

  nextTick(() => {
    if (loadingInstance1)
      loadingInstance1.close();
  });

  return {
    start,
    stop
  }
};

function choiceManage(selection, gridMeta, gridRef) {
  let isCurrenting = false;
  let isMoring = false;
  const currentChange = (row) => {
    if (isMoring)
      return;
    if (!row)
      return;
    if (gridRef.value) {
      isCurrenting = true;
      gridRef.value.clearSelection();
      gridRef.value.toggleRowSelection(row);
      gridRef.value.setCurrentRow(row);
      selection.dataId = row[gridMeta.idName];
      selection.dataIds = [row[gridMeta.idName]];
      selection.row = row;
      selection.rows = [row];
      isCurrenting = false;
    }
  };
  const selectionChange = (rows) => {
    if (isCurrenting)
      return;
    if (typeof selection.dataIds === "undefined") {
      selection.dataIds = [];
    }
    selection.dataIds.length = 0;
    rows.forEach((item) => {
      if (typeof item !== "undefined" && item !== null) {
        selection.dataIds.push(item[gridMeta.idName]);
      }
    });
    selection.rows = rows;
    switch (rows.length) {
      case 0:
        gridRef.value.setCurrentRow();
        selection.dataId = "";
        selection.row = {};
        break;
      case 1:
        isMoring = true;
        gridRef.value.setCurrentRow(rows[0]);
        isMoring = false;
        selection.row = rows[0];
        selection.dataId = rows[0][gridMeta.idName];
        break;
      default:
        gridRef.value.setCurrentRow();
        selection.row = rows[rows.length - 1];
        selection.dataId = selection.row[gridMeta.idName];
    }
  };
  return {
    currentChange,
    selectionChange
  };
}

const _sfc_main$8 = defineComponent({
  name: "nf-elp-grid-list",
  inheritAttrs: false,
  components: {
    ElTable,
    ElTableColumn
  },
  props: {
    ...gridProps
  },
  setup(props, ctx) {
    const gridRef = ref();
    const { selection, gridMeta } = props;
    const {
      currentChange,
      selectionChange
    } = choiceManage(selection, gridMeta, gridRef);
    return {
      selectionChange,
      currentChange,
      gridRef
    };
  }
});
function _sfc_render$8(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_table_column = resolveComponent("el-table-column");
  const _component_el_table = resolveComponent("el-table");
  return openBlock(), createBlock(_component_el_table, mergeProps({
    ref: "gridRef",
    height: _ctx.height,
    stripe: _ctx.stripe,
    border: _ctx.border,
    fit: _ctx.fit,
    "highlight-current-row": _ctx.highlightCurrentRow
  }, _ctx.$attrs, {
    data: _ctx.dataList,
    "row-key": _ctx.gridMeta.idName,
    onSelectionChange: _ctx.selectionChange,
    onCurrentChange: _ctx.currentChange
  }), {
    default: withCtx(() => [
      createCommentVNode("\u663E\u793A\u9009\u62E9\u6846"),
      createVNode(_component_el_table_column, {
        type: "selection",
        width: "55",
        align: "center",
        "header-align": "center"
      }),
      createCommentVNode("\u663E\u793A\u5B57\u6BB5\u5217\u8868"),
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.gridMeta.colOrder, (id, index) => {
        return openBlock(), createBlock(_component_el_table_column, mergeProps({
          key: "grid_list_" + index + "_" + id,
          "min-width": 50,
          "column-key": "col_" + id
        }, _ctx.itemMeta[id], {
          prop: _ctx.itemMeta[id].colName
        }), null, 16, ["column-key", "prop"]);
      }), 128)),
      createCommentVNode("\u53F3\u9762\u7684\u64CD\u4F5C\u5217")
    ]),
    _: 1
  }, 16, ["height", "stripe", "border", "fit", "highlight-current-row", "data", "row-key", "onSelectionChange", "onCurrentChange"]);
}
var nfGrid = /* @__PURE__ */ _export_sfc(_sfc_main$8, [["render", _sfc_render$8], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/grid/grid-table.vue"]]);

const _sfc_main$7 = defineComponent({
  name: "nf-elp-grid-list-slot",
  inheritAttrs: false,
  components: {
    ElTable,
    ElTableColumn
  },
  props: {
    ...gridProps
  },
  setup(props, context) {
    const slots = context.slots;
    const slotsKey = Object.keys(slots);
    const gridRef = ref(null);
    const {
      currentChange,
      selectionChange
    } = choiceManage(props.selection, props.gridMeta, gridRef);
    return {
      slotsKey,
      selectionChange,
      currentChange,
      gridRef
    };
  }
});
function _sfc_render$7(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_table_column = resolveComponent("el-table-column");
  const _component_el_table = resolveComponent("el-table");
  return openBlock(), createBlock(_component_el_table, mergeProps({
    ref: "gridRef",
    height: _ctx.height,
    stripe: _ctx.stripe,
    border: _ctx.border,
    fit: _ctx.fit,
    "highlight-current-row": _ctx.highlightCurrentRow
  }, _ctx.$attrs, {
    data: _ctx.dataList,
    "row-key": _ctx.gridMeta.idName,
    onSelectionChange: _ctx.selectionChange,
    onCurrentChange: _ctx.currentChange
  }), {
    default: withCtx(() => [
      createCommentVNode("\u663E\u793A\u9009\u62E9\u6846"),
      !_ctx.$attrs["show-summary"] ? (openBlock(), createBlock(_component_el_table_column, {
        key: 0,
        type: "selection",
        width: "55",
        align: "center",
        "header-align": "center"
      })) : createCommentVNode("v-if", true),
      createCommentVNode("\u663E\u793A\u5B57\u6BB5\u5217\u8868"),
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.gridMeta.colOrder, (id, index) => {
        return openBlock(), createElementBlock(Fragment, {
          key: "grid_list_" + index + "_" + id
        }, [
          createCommentVNode("\u5E26\u63D2\u69FD\u7684\u5217"),
          _ctx.slotsKey.includes(_ctx.itemMeta[id].colName) ? (openBlock(), createBlock(_component_el_table_column, mergeProps({
            key: 0,
            "column-key": "col_" + id
          }, _ctx.itemMeta[id]), createSlots({ _: 2 }, [
            _ctx.slotsKey.includes("header_" + _ctx.itemMeta[id].colName) ? {
              name: "header",
              fn: withCtx(() => [
                renderSlot(_ctx.$slots, "header_" + _ctx.itemMeta[id].colName)
              ])
            } : void 0,
            _ctx.slotsKey.includes(_ctx.itemMeta[id].colName) ? {
              name: "default",
              fn: withCtx((scope) => [
                renderSlot(_ctx.$slots, _ctx.itemMeta[id].colName, normalizeProps(guardReactiveProps(scope)))
              ])
            } : void 0
          ]), 1040, ["column-key"])) : (openBlock(), createElementBlock(Fragment, { key: 1 }, [
            createCommentVNode("\u4E0D\u5E26\u63D2\u69FD\u7684\u5217"),
            (openBlock(), createBlock(_component_el_table_column, mergeProps({
              key: "grid_list_" + index + "_" + id,
              "column-key": "col_" + id,
              "min-width": 50
            }, _ctx.itemMeta[id], {
              prop: _ctx.itemMeta[id].colName
            }), null, 16, ["column-key", "prop"]))
          ], 2112))
        ], 64);
      }), 128))
    ]),
    _: 3
  }, 16, ["height", "stripe", "border", "fit", "highlight-current-row", "data", "row-key", "onSelectionChange", "onCurrentChange"]);
}
var nfGridSlot = /* @__PURE__ */ _export_sfc(_sfc_main$7, [["render", _sfc_render$7], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/grid/grid-slot.vue"]]);

const _sfc_main$6 = defineComponent({
  name: "nf-elp-grid-list-slot",
  inheritAttrs: false,
  components: {
    ElTable,
    ElTableColumn
  },
  props: {
    ...gridProps
  },
  setup(props, context) {
    const mydefault = context.slots?.default?.() ?? [];
    const defaultKey = {};
    mydefault.forEach((item, index) => {
      if (item.props?.prop) {
        defaultKey[item.props.prop] = mydefault[index];
      }
    });
    const gridRef = ref(null);
    const {
      currentChange,
      selectionChange
    } = choiceManage(props.selection, props.gridMeta.idName, gridRef);
    return {
      mydefault,
      defaultKey,
      selectionChange,
      currentChange,
      gridRef
    };
  }
});
function _sfc_render$6(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_table_column = resolveComponent("el-table-column");
  const _component_el_table = resolveComponent("el-table");
  return openBlock(), createBlock(_component_el_table, mergeProps({
    ref: "gridRef",
    height: _ctx.height,
    stripe: _ctx.stripe,
    border: _ctx.border,
    fit: _ctx.fit,
    "highlight-current-row": _ctx.highlightCurrentRow
  }, _ctx.$attrs, {
    data: _ctx.dataList,
    "row-key": _ctx.gridMeta.idName,
    onSelectionChange: _ctx.selectionChange,
    onCurrentChange: _ctx.currentChange
  }), {
    default: withCtx(() => [
      createCommentVNode("\u663E\u793A\u9009\u62E9\u6846"),
      !_ctx.$attrs["show-summary"] ? (openBlock(), createBlock(_component_el_table_column, {
        key: 0,
        type: "selection",
        width: "55",
        align: "center",
        "header-align": "center"
      })) : createCommentVNode("v-if", true),
      createCommentVNode("\u663E\u793A\u5B57\u6BB5\u5217\u8868"),
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.gridMeta.colOrder, (id, index) => {
        return openBlock(), createElementBlock(Fragment, {
          key: "grid_list_" + index + "_" + id
        }, [
          createCommentVNode("\u4F7F\u7528\u5916\u90E8\u7684 el-table-colmun "),
          !!_ctx.defaultKey[_ctx.itemMeta[id].colName] ? (openBlock(), createBlock(resolveDynamicComponent(_ctx.defaultKey[_ctx.itemMeta[id].colName]), { key: 0 })) : (openBlock(), createElementBlock(Fragment, { key: 1 }, [
            createCommentVNode("\u4F9D\u8D56 json \u7684\u5217"),
            (openBlock(), createBlock(_component_el_table_column, mergeProps({
              key: "grid_list_" + index + "_" + id,
              "column-key": "col_" + id,
              "min-width": 50
            }, _ctx.itemMeta[id], {
              prop: _ctx.itemMeta[id].colName
            }), null, 16, ["column-key", "prop"]))
          ], 2112))
        ], 64);
      }), 128))
    ]),
    _: 1
  }, 16, ["height", "stripe", "border", "fit", "highlight-current-row", "data", "row-key", "onSelectionChange", "onCurrentChange"]);
}
var nfGridSlotCol = /* @__PURE__ */ _export_sfc(_sfc_main$6, [["render", _sfc_render$6], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/grid/grid-slot-col.vue"]]);

const _sfc_main$5 = defineComponent({
  name: "nf-el-from-item-base",
  components: {
    ElFormItem,
    ElRow,
    ElCol
  },
  props: {
    colOrder: {
      type: Array
    },
    itemMeta: {
      type: Object
    },
    ruleMeta: {
      type: Object
    },
    showCol: {
      type: Object
    },
    formColSpan: {
      type: Object
    },
    model: {
      type: Object
    },
    size: [String]
  },
  setup(props, context) {
    const slots = context.slots;
    const slotsKey = Object.keys(slots);
    return {
      formItemKey,
      slotsKey
    };
  }
});
function _sfc_render$5(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_form_item = resolveComponent("el-form-item");
  const _component_el_col = resolveComponent("el-col");
  const _component_el_row = resolveComponent("el-row");
  return openBlock(), createBlock(_component_el_row, { gutter: 15 }, {
    default: withCtx(() => [
      (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.colOrder, (ctrId, index) => {
        return withDirectives((openBlock(), createBlock(_component_el_col, {
          key: "form_" + ctrId + "_" + index,
          span: _ctx.formColSpan[ctrId]
        }, {
          default: withCtx(() => [
            createVNode(Transition, {
              name: "el-zoom-in-top",
              persisted: ""
            }, {
              default: withCtx(() => [
                withDirectives(createVNode(_component_el_form_item, {
                  label: _ctx.itemMeta[ctrId].formItemMeta.label,
                  prop: _ctx.itemMeta[ctrId].formItemMeta.colName,
                  rules: _ctx.ruleMeta[ctrId] ?? [],
                  "label-width": _ctx.itemMeta[ctrId].formItemMeta.labelWidth ?? "",
                  size: _ctx.size
                }, {
                  default: withCtx(() => [
                    (openBlock(), createBlock(resolveDynamicComponent(_ctx.formItemKey[_ctx.itemMeta[ctrId].formItemMeta.controlType]), mergeProps({ model: _ctx.model }, _ctx.itemMeta[ctrId]), null, 16, ["model"]))
                  ]),
                  _: 2
                }, 1032, ["label", "prop", "rules", "label-width", "size"]), [
                  [vShow, _ctx.showCol[ctrId]]
                ])
              ]),
              _: 2
            }, 1024)
          ]),
          _: 2
        }, 1032, ["span"])), [
          [vShow, _ctx.showCol[ctrId]]
        ]);
      }), 128))
    ]),
    _: 1
  });
}
var baseItem = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["render", _sfc_render$5], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form/base-item.vue"]]);

const _sfc_main$4 = defineComponent({
  name: "nf-el-from-div",
  components: {
    baseItem,
    ElForm
  },
  props: {
    ...formProps
  },
  setup(props, context) {
    const formControl = ref(null);
    const {
      formMeta,
      itemMeta,
      model,
      partModel
    } = props;
    const {
      formColSpan,
      showCol,
      setFormColShow
    } = formController(formMeta, itemMeta, model, partModel);
    return {
      ...formMeta,
      itemMeta,
      showCol,
      formColSpan,
      setFormColShow,
      formControl
    };
  }
});
function _sfc_render$4(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_base_item = resolveComponent("base-item");
  const _component_el_form = resolveComponent("el-form");
  return openBlock(), createBlock(_component_el_form, mergeProps({
    model: _ctx.model,
    ref: "formControl",
    inline: false,
    class: "demo-form-inline",
    "label-suffix": _ctx.labelSuffix,
    "label-width": _ctx.labelWidth,
    size: _ctx.size
  }, _ctx.$attrs), {
    default: withCtx(() => [
      createVNode(_component_base_item, {
        colOrder: _ctx.colOrder,
        itemMeta: _ctx.itemMeta,
        ruleMeta: _ctx.ruleMeta,
        showCol: _ctx.showCol,
        formColSpan: _ctx.formColSpan,
        model: _ctx.model,
        size: _ctx.size
      }, null, 8, ["colOrder", "itemMeta", "ruleMeta", "showCol", "formColSpan", "model", "size"])
    ]),
    _: 1
  }, 16, ["model", "label-suffix", "label-width", "size"]);
}
var nfForm = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["render", _sfc_render$4], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form/form-div.vue"]]);

const _sfc_main$3 = defineComponent({
    name: 'el-from-slot-div',
    components: {
      ElForm, ElFormItem, ElRow, ElCol, ElCard
    },
    props: {
      ...formProps
    },
    setup (props, context) {
      // console.log('表单控件的props：', props)

      const slots = context.slots;
      const slotsKey = Object.keys(slots);

      // 获取 $ref
      const formControl = ref(null);
      onMounted(() => {
        // console.log('表单dom', formControl)
      });

      const {
        formMeta,
        itemMeta,
        model,
        partModel
      } = props;

      // 调用表单的控制类
      const {
        formColSpan, // 确定组件占位
        // formRules, // 表单的验证规则
        showCol, // 是否显示字段的对象
        setFormColShow, // 设置组件联动
      } = formController(formMeta, itemMeta, model, partModel);

      return {
        slotsKey,
        ...formMeta,
        itemMeta,
        showCol, // 是否显示字段的对象
        formColSpan, // 确定组件占位
        // 函数
        setFormColShow, // 设置组件联动
        //
        formControl, // 获取表单dom
        formItemKey // 子控件字典
      }
    }
  });

const _hoisted_1$2 = /*#__PURE__*/createTextVNode("父组件没有设置插槽");

function _sfc_render$3(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_form_item = resolveComponent("el-form-item");
  const _component_el_col = resolveComponent("el-col");
  const _component_el_row = resolveComponent("el-row");
  const _component_el_form = resolveComponent("el-form");

  return (openBlock(), createBlock(_component_el_form, mergeProps({
    model: _ctx.model,
    rules: _ctx.formRules,
    ref: "formControl",
    inline: false,
    class: "demo-form-inline",
    "label-suffix": _ctx.labelSuffix,
    "label-width": _ctx.labelWidth,
    size: _ctx.size
  }, _ctx.$attrs), {
    default: withCtx(() => [
      createVNode(_component_el_row, { gutter: 15 }, {
        default: withCtx(() => [
          (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.colOrder, (ctrId, index) => {
            return withDirectives((openBlock(), createBlock(_component_el_col, {
              key: 'form_' + ctrId + index,
              span: _ctx.formColSpan[ctrId]
            }, {
              default: withCtx(() => [
                createVNode(Transition, {
                  name: "el-zoom-in-top",
                  persisted: ""
                }, {
                  default: withCtx(() => [
                    withDirectives(createVNode(_component_el_form_item, {
                      rules: _ctx.ruleMeta[ctrId] ?? [],
                      "label-width": _ctx.itemMeta[ctrId].labelWidth??'',
                      size: _ctx.size,
                      label: _ctx.itemMeta[ctrId].formItemMeta.label,
                      prop: _ctx.itemMeta[ctrId].formItemMeta.colName
                    }, {
                      default: withCtx(() => [
                        createCommentVNode("判断要不要加载插槽"),
                        ((_ctx.slotsKey.includes(_ctx.itemMeta[ctrId].formItemMeta.colName)))
                          ? (openBlock(), createElementBlock(Fragment, { key: 0 }, [
                              createCommentVNode("<slot :name=\"ctrId\">父组件没有设置插槽</slot>"),
                              renderSlot(_ctx.$slots, _ctx.itemMeta[ctrId].formItemMeta.colName, {}, () => [
                                _hoisted_1$2
                              ])
                            ], 64 /* STABLE_FRAGMENT */))
                          : (openBlock(), createElementBlock(Fragment, { key: 1 }, [
                              createCommentVNode("表单item组件，采用动态组件的方式"),
                              (openBlock(), createBlock(resolveDynamicComponent(_ctx.formItemKey[_ctx.itemMeta[ctrId].formItemMeta.controlType]), mergeProps({ model: _ctx.model }, _ctx.itemMeta[ctrId]), null, 16 /* FULL_PROPS */, ["model"]))
                            ], 64 /* STABLE_FRAGMENT */))
                      ]),
                      _: 2 /* DYNAMIC */
                    }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["rules", "label-width", "size", "label", "prop"]), [
                      [vShow, _ctx.showCol[ctrId]]
                    ])
                  ]),
                  _: 2 /* DYNAMIC */
                }, 1024 /* DYNAMIC_SLOTS */)
              ]),
              _: 2 /* DYNAMIC */
            }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["span"])), [
              [vShow, _ctx.showCol[ctrId]]
            ])
          }), 128 /* KEYED_FRAGMENT */))
        ]),
        _: 3 /* FORWARDED */
      })
    ]),
    _: 3 /* FORWARDED */
  }, 16 /* FULL_PROPS */, ["model", "rules", "label-suffix", "label-width", "size"]))
}
var formDivSlot = /*#__PURE__*/_export_sfc(_sfc_main$3, [['render',_sfc_render$3],['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form/form-div-slot.vue"]]);

const _sfc_main$2 = defineComponent({
  name: "el-from-div",
  components: {
    baseItem,
    ElForm,
    ElRow,
    ElCol,
    ElCard
  },
  props: {
    ...formProps
  },
  setup(props, context) {
    const formControl = ref(null);
    onMounted(() => {
    });
    const {
      formMeta,
      itemMeta,
      model,
      partModel
    } = props;
    const {
      formColSpan,
      showCol,
      setFormColShow
    } = formController(formMeta, itemMeta, model, partModel);
    const { subMeta } = formMeta;
    if (subMeta.type === "card1") ;
    const cardOrder = subMeta.cols ?? [];
    const cardSpan = computed(() => 24 / (subMeta.cardColCount ?? 1));
    return {
      ...formMeta,
      itemMeta,
      cardOrder,
      cardSpan,
      showCol,
      formColSpan,
      setFormColShow,
      formControl
    };
  }
});
const _hoisted_1$1 = { class: "card-header" };
function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_base_item = resolveComponent("base-item");
  const _component_el_card = resolveComponent("el-card");
  const _component_el_col = resolveComponent("el-col");
  const _component_el_row = resolveComponent("el-row");
  const _component_el_form = resolveComponent("el-form");
  return openBlock(), createBlock(_component_el_form, mergeProps({
    model: _ctx.model,
    ref: "formControl",
    inline: false,
    class: "demo-form-inline",
    "label-suffix": _ctx.labelSuffix,
    "label-width": _ctx.labelWidth,
    size: _ctx.size
  }, _ctx.$attrs), {
    default: withCtx(() => [
      createVNode(_component_el_row, { gutter: 15 }, {
        default: withCtx(() => [
          (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.cardOrder, (item, index) => {
            return openBlock(), createBlock(_component_el_col, {
              span: _ctx.cardSpan,
              key: index
            }, {
              default: withCtx(() => [
                createVNode(_component_el_card, {
                  class: "box-card",
                  style: { "margin-bottom": "10px" }
                }, {
                  header: withCtx(() => [
                    createElementVNode("div", _hoisted_1$1, [
                      createElementVNode("span", null, toDisplayString(item.title), 1)
                    ])
                  ]),
                  default: withCtx(() => [
                    createVNode(_component_base_item, {
                      colOrder: item.colIds,
                      itemMeta: _ctx.itemMeta,
                      ruleMeta: _ctx.ruleMeta,
                      showCol: _ctx.showCol,
                      formColSpan: _ctx.formColSpan,
                      model: _ctx.model,
                      size: _ctx.size
                    }, null, 8, ["colOrder", "itemMeta", "ruleMeta", "showCol", "formColSpan", "model", "size"])
                  ]),
                  _: 2
                }, 1024)
              ]),
              _: 2
            }, 1032, ["span"]);
          }), 128))
        ]),
        _: 1
      })
    ]),
    _: 1
  }, 16, ["model", "label-suffix", "label-width", "size"]);
}
var nfFormCard = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form/form-div-card.vue"]]);

const _sfc_main$1 = defineComponent({
  name: "el-from-div",
  components: {
    baseItem,
    ElForm,
    ElTabPane,
    ElTabs
  },
  props: {
    ...formProps
  },
  setup(props, context) {
    const formControl = ref(null);
    const {
      formMeta,
      itemMeta,
      model,
      partModel
    } = props;
    const {
      formColSpan,
      showCol,
      setFormColShow
    } = formController(formMeta, itemMeta, model, partModel);
    const { subMeta } = formMeta;
    const cardOrder = subMeta.cols ?? [];
    const tabIndex = ref(cardOrder[0].title);
    return {
      ...formMeta,
      itemMeta,
      cardOrder,
      tabIndex,
      showCol,
      formColSpan,
      setFormColShow,
      formControl
    };
  }
});
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_base_item = resolveComponent("base-item");
  const _component_el_tab_pane = resolveComponent("el-tab-pane");
  const _component_el_tabs = resolveComponent("el-tabs");
  const _component_el_form = resolveComponent("el-form");
  return openBlock(), createBlock(_component_el_form, mergeProps({
    model: _ctx.model,
    ref: "formControl",
    inline: false,
    class: "demo-form-inline",
    "label-suffix": _ctx.labelSuffix,
    "label-width": _ctx.labelWidth,
    size: _ctx.size
  }, _ctx.$attrs), {
    default: withCtx(() => [
      createVNode(_component_el_tabs, {
        modelValue: _ctx.tabIndex,
        "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => _ctx.tabIndex = $event),
        type: "border-card"
      }, {
        default: withCtx(() => [
          (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.cardOrder, (item, index) => {
            return openBlock(), createBlock(_component_el_tab_pane, {
              key: "tabs_" + index,
              label: item.title,
              name: item.title
            }, {
              default: withCtx(() => [
                createVNode(_component_base_item, {
                  colOrder: item.colIds,
                  itemMeta: _ctx.itemMeta,
                  ruleMeta: _ctx.ruleMeta,
                  showCol: _ctx.showCol,
                  formColSpan: _ctx.formColSpan,
                  model: _ctx.model,
                  size: _ctx.size
                }, null, 8, ["colOrder", "itemMeta", "ruleMeta", "showCol", "formColSpan", "model", "size"])
              ]),
              _: 2
            }, 1032, ["label", "name"]);
          }), 128))
        ]),
        _: 1
      }, 8, ["modelValue"])
    ]),
    _: 1
  }, 16, ["model", "label-suffix", "label-width", "size"]);
}
var nfFormTab = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["__file", "D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form/form-div-tab.vue"]]);

const _sfc_main = defineComponent({
    name: 'el-from-div',
    components: {
      baseItem,
      ElForm, ElCard, ElButton, ElSteps, ElStep
    },
    props: {
      ...formProps
    },
    setup (props, context) {
      // console.log('表单控件的props：', props)

      // 获取 $ref
      const formControl = ref(null);

      const {
        formMeta,
        itemMeta,
        model,
        partModel
      } = props;

      // 调用表单的控制类
      const {
        formColSpan, // 确定组件占位
        // formRules, // 表单的验证规则
        showCol, // 是否显示字段的对象
        setFormColShow, // 设置组件联动
      } = formController(formMeta, itemMeta, model, partModel);

      // 获取分栏信息
      const { subMeta } = formMeta;

      if (subMeta.type === 'card1');

      const cardOrder = subMeta.cols ?? [];
      
      // tab 
      const active = ref(0);

      const next = () => {
        active.value ++;
        if (active.value >= cardOrder.length ) {
          active.value = 0;
        }
      };

      return {
        ...formMeta,
        itemMeta,
        cardOrder,
        active, // step 的步骤
        next, // 下一步
        showCol, // 是否显示字段的对象
        formColSpan, // 确定组件占位
        // 函数
        setFormColShow, // 设置组件联动
        //
        formControl // 获取表单dom

      }
    }
  });

const _hoisted_1 = { class: "card-header" };
const _hoisted_2 = /*#__PURE__*/createTextVNode("下一步");

function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_el_step = resolveComponent("el-step");
  const _component_el_steps = resolveComponent("el-steps");
  const _component_base_item = resolveComponent("base-item");
  const _component_el_card = resolveComponent("el-card");
  const _component_el_form = resolveComponent("el-form");
  const _component_el_button = resolveComponent("el-button");

  return (openBlock(), createElementBlock(Fragment, null, [
    createCommentVNode("向导"),
    createVNode(_component_el_steps, {
      active: _ctx.active,
      "finish-status": "success"
    }, {
      default: withCtx(() => [
        (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.cardOrder, (item, index) => {
          return (openBlock(), createBlock(_component_el_step, {
            key: index,
            title: item.title
          }, null, 8 /* PROPS */, ["title"]))
        }), 128 /* KEYED_FRAGMENT */))
      ]),
      _: 1 /* STABLE */
    }, 8 /* PROPS */, ["active"]),
    createCommentVNode("表单"),
    createVNode(_component_el_form, mergeProps({
      model: _ctx.model,
      ref: "formControl",
      inline: false,
      class: "demo-form-inline",
      "label-suffix": _ctx.labelSuffix,
      "label-width": _ctx.labelWidth,
      size: _ctx.size
    }, _ctx.$attrs), {
      default: withCtx(() => [
        (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.cardOrder, (item, index) => {
          return withDirectives((openBlock(), createBlock(_component_el_card, {
            class: "box-card",
            style: {"margin-bottom":"10px"},
            key: index
          }, {
            header: withCtx(() => [
              createElementVNode("div", _hoisted_1, [
                createElementVNode("span", null, toDisplayString(item.title), 1 /* TEXT */)
              ])
            ]),
            default: withCtx(() => [
              createVNode(_component_base_item, {
                colOrder: item.colIds,
                itemMeta: _ctx.itemMeta,
                ruleMeta: _ctx.ruleMeta,
                showCol: _ctx.showCol,
                formColSpan: _ctx.formColSpan,
                model: _ctx.model,
                size: _ctx.size
              }, null, 8 /* PROPS */, ["colOrder", "itemMeta", "ruleMeta", "showCol", "formColSpan", "model", "size"])
            ]),
            _: 2 /* DYNAMIC */
          }, 1024 /* DYNAMIC_SLOTS */)), [
            [vShow, _ctx.active === index]
          ])
        }), 128 /* KEYED_FRAGMENT */))
      ]),
      _: 1 /* STABLE */
    }, 16 /* FULL_PROPS */, ["model", "label-suffix", "label-width", "size"]),
    createVNode(_component_el_button, {
      type: "",
      onClick: _ctx.next
    }, {
      default: withCtx(() => [
        _hoisted_2
      ]),
      _: 1 /* STABLE */
    }, 8 /* PROPS */, ["onClick"])
  ], 64 /* STABLE_FRAGMENT */))
}
var nfFormStep = /*#__PURE__*/_export_sfc(_sfc_main, [['render',_sfc_render],['__file',"D:/nf/gitee-ts/nf-code/nf-rollup-ui-element-plus/lib/form/form-div-step.vue"]]);

const msgBox$1 = {
  confirmButtonText: "\u79FB\u9664",
  cancelButtonText: "\u4E0D\u8981\u5440",
  type: "warning"
};
const msgError$1 = {
  type: "info",
  message: "\u5DF2\u7ECF\u53D6\u6D88\u4E86\u3002"
};
const deleteDom$1 = (col, del) => {
  ElMessageBox.confirm(`\u6B64\u64CD\u4F5C\u5C06\u79FB\u9664\u8BE5\u5217\uFF1A(${col.label}), \u662F\u5426\u7EE7\u7EED\uFF1F`, "\u6E29\u99A8\u63D0\u793A", msgBox$1).then(() => {
    del();
  }).catch(() => {
    ElMessage(msgError$1);
  });
};
const _formDrag = {
  mounted(el, binding) {
    const className = "el-row";
    const meta = binding.value;
    nextTick(() => {
      const form = el.getElementsByClassName(className)[0];
      const { formMeta, itemMeta, modCol } = meta;
      formDrag(formMeta, itemMeta, form, deleteDom$1, modCol).formSetup();
    });
  },
  unmounted(el, binding) {
  }
};
const install$2 = (app, options) => {
  app.directive("formDrag", _formDrag);
};

const msgBox = {
  confirmButtonText: "\u79FB\u9664",
  cancelButtonText: "\u4E0D\u8981\u5440",
  type: "warning"
};
const msgError = {
  type: "info",
  message: "\u5DF2\u7ECF\u53D6\u6D88\u4E86\u3002"
};
const deleteDom = (col, del) => {
  ElMessageBox.confirm(`\u6B64\u64CD\u4F5C\u5C06\u79FB\u9664\u8BE5\u5217\uFF1A(${col.label}), \u662F\u5426\u7EE7\u7EED\uFF1F`, "\u6E29\u99A8\u63D0\u793A", msgBox).then(() => {
    del();
  }).catch(() => {
    ElMessage(msgError);
  });
};
const _gridDrag = {
  mounted(el, binding) {
    const className = "el-table__header";
    const meta = binding.value;
    nextTick(() => {
      setTimeout(() => {
        const table = el.getElementsByClassName(className)[0];
        const { gridMeta, itemMeta, modCol } = meta;
        gridDrag(gridMeta, itemMeta, table, deleteDom, modCol).girdSetup();
      }, 600);
    });
  },
  unmounted(el, binding) {
  }
};
const install$1 = (app, options) => {
  app.directive("gridDrag", _gridDrag);
};

const config = {
  formItem: reactive({})
};

dayjs.extend(isoWeeksInYear);
dayjs.extend(isLeapYear);
dayjs.extend(isoWeek);
const nfElementPlus = (app, option) => {
  app.component("nfButton", nfButton);
  app.component("nfForm", nfForm);
  app.component("nfFormCard", nfFormCard);
  app.component("nfFormTab", nfFormTab);
  app.component("nfFormStep", nfFormStep);
  app.component("nfFind", nfFind);
  app.component("nfGrid", nfGrid);
  app.component("nfGridSlot", nfGridSlot);
  app.component("nfGridSlotCol", nfGridSlotCol);
  app.component("nfMenu", nfMenu);
  app.component("nfRouterView", nfRouterView);
  app.component("nfRouterViewTabs", nfRouterViewTabs);
  for (const key in AllFormItem) {
    app.component(key, AllFormItem[key]);
  }
  app.directive("dialogDrag", _dialogDrag);
  app.directive("formDrag", _formDrag);
  app.directive("gridDrag", _gridDrag);
};
const install = nfElementPlus;

export { AllFormItem, _formDrag, _gridDrag, config, install$3 as dialogDrag, install$2 as formDrag, formItemKey, install$1 as gridDrag, install, loading, nfButton, nfElementPlus, nfFind, nfForm, nfFormCard, formDivSlot as nfFormSlot, nfFormStep, nfFormTab, nfGrid, nfGridSlot, nfMenu, nfRouterView, nfRouterViewTabs };
//# sourceMappingURL=nf-ui-elp.es.js.map
