# nf-ui-elementPlus  基于 element-plus 的二次开发

> 源码目录：https://naturefw.gitee.io/  

还在开发中，所以更新会比较频繁，接口也可能会改变。等稳定后，接口就不会随意改变了。

#### 介绍
封装UI库（elementPlus），支持 json 渲染和自定义扩展。  
表单控件、列表控件、查询控件、菜单控件等。  

#### 技术栈
* ts
* vite: ^2.9.5
* vue: ^3.2.33
* element-plus: ^2.2.5
* @element-plus/icons-vue: ^2.0.4
* dayjs: ^1.10.7
* @naturefw/nf-tool: ^0.1.0
* @naturefw/ui-core: ^0.2.4

#### 目录结构

* lib 状态管理的源码
* src 状态管理的使用 demo
* distp 在线演示的代码

#### 安装

```
yarn add @naturefw/ui-elp
```

#### 依赖的资源包

* @naturefw/nf-tool （自动安装）
* @naturefw/ui-core （自动安装）
* element-plus （需要手动安装）
* dayjs

#### 使用说明

App.vue 里面设置了一下国际化。

#### 源码
https://gitee.com/naturefw-code/nf-rollup-ui-element-plus

[![自然框架源码/nf-ui-elementPlus-基于 element-plus 的二次开发](https://gitee.com/naturefw-code/nf-rollup-ui-element-plus/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/naturefw-code/nf-rollup-ui-element-plus)


#### 在线演示
https://naturefw-code.gitee.io/nf-rollup-ui-element-plus/

#### 在线文档
https://nfpress.gitee.io/doc-ui-elp/


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
