/**
 * vue 的 props 的验证的类型约束
 */
export interface IPropsValidation {
    /**
     * 属性的类型，比较灵活，可以是 String、Number 等，也可以是数组、class等
     */
    type: Array<any> | any;
    /**
     * 是否必须传递属性
     */
    required?: boolean;
    /**
     * 自定义类型校验函数（箭头函数），value：属性值
     */
    validator?: (value: any) => boolean;
    /**
     * 默认值，可以是值，也可以是函数（箭头函数）
     */
    default?: any;
}

/**
 * vue 的 props 的验证的类型约束
 */
export interface IPropsValidation {
  /**
   * 属性的类型，比较灵活，可以是 String、Number 等，也可以是数组、class等
   */
  type: Array<any> | any,
  /**
   * 是否必须传递属性
   */
  required?: boolean,
  /**
   * 自定义类型校验函数（箭头函数），value：属性值
   */
  validator?: (value: any) => boolean,
  /**
   * 默认值，可以是值，也可以是函数（箭头函数）
   */
  default?: any
}import type { IPropsValidation } from './10-base-validation';
/**
 * 表单子控件的共用属性。约束必须有的属性
 */
export interface ItemProps {
    /**
     * 字段ID、控件ID，sting | number
     */
    columnId: IPropsValidation;
    /**
     * 表单的 model，含义多个属性，any
     */
    model: IPropsValidation;
    /**
     * 字段名称，string
     */
    colName: IPropsValidation;
    /**
     * 控件类型，number
     */
    controlType: IPropsValidation;
    /**
     * 控件备选项，一级或者多级，Array<IOptionItem | IOptionItemTree>
     */
    optionList: IPropsValidation;
    /**
     * 访问后端API的配置，IWebAPI
     */
    webAPI: IPropsValidation;
    /**
     * 防抖延迟时间，0：不延迟，number
     */
    delay: IPropsValidation;
    /**
     * 防抖相关的事件() => void
     */
    events: IPropsValidation;
    /**
     * 控件的大小，string
     */
    size: IPropsValidation;
    /**
     * 是否显示清空的按钮，boolean
     */
    clearable: IPropsValidation;
    /**
     * 控件的扩展属性，any
     */
    extend: IPropsValidation;
}
/**
 * 访问后端API的配置
 */
export interface IWebAPI {
    /**
     * 服务ID/服务编号/服务名称
     */
    serviceId: number | string;
    /**
     * 动作ID/动作名称
     */
    actionId: number | string;
    /**
     * 主键字段值
     */
    dataId: number | string;
    /**
     * form 的值
     */
    body: any;
    /**
     * 联动组件需要的设置
     */
    cascader: {
        /**
         * 是否需要动态加载
         */
        lazy: boolean;
        /**
         * 按照level的顺序设置后端 API 的 action 名称
         */
        actions: Array<string>;
    };
}
/**
 * 单层的选项，下拉列表等的选项。value、label
 */
export interface IOptionItem {
    /**
     * 值，提交到后端
     */
    value: number | string;
    /**
     * 标签、文字说明
     */
    label: string;
}
/**
 * 多级的选项，级联控件。value、label、children（n级嵌套）
 */
export interface IOptionItemTree {
    /**
     * 值，提交到后端
     */
    value: number | string;
    /**
     * 标签、文字说明
     */
    label: string;
    /**
     * 子选项，可以无限嵌套
     */
    children: Array<IOptionItemTree>;
}

import type { IPropsValidation } from './10-base-validation'

// 表单子控件，表单控件里面的各种控件的共用属性，以及属性类型。

/**
 * 表单子控件的共用属性。约束必须有的属性
 */
 export interface ItemProps {
  /**
   * 字段ID、控件ID，sting | number
   */
  columnId: IPropsValidation,
  /**
   * 表单的 model，含义多个属性，any
   */
  model: IPropsValidation,
  /**
   * 字段名称，string
   */
  colName: IPropsValidation,
  /**
   * 控件类型，number
   */
  controlType: IPropsValidation,
  /**
   * 控件备选项，一级或者多级，Array<IOptionItem | IOptionItemTree>
   */
  optionList: IPropsValidation,
  /**
   * 访问后端API的配置，IWebAPI
   */
  webAPI: IPropsValidation,
  /**
   * 防抖延迟时间，0：不延迟，number
   */
  delay: IPropsValidation,
  /**
   * 防抖相关的事件() => void
   */
  events: IPropsValidation,
  /**
   * 控件的大小，string
   */
  size: IPropsValidation,
  /**
   * 是否显示清空的按钮，boolean
   */
  clearable: IPropsValidation,
  /**
   * 控件的扩展属性，any
   */
  extend: IPropsValidation,
}

/**
 * 访问后端API的配置
 */
 export interface IWebAPI {
  /**
   * 服务ID/服务编号/服务名称
   */
  serviceId: number | string,
  /**
   * 动作ID/动作名称
   */
  actionId: number | string,
  /**
   * 主键字段值
   */
  dataId: number | string,
  /**
   * form 的值
   */
  body: any,
  /**
   * 联动组件需要的设置
   */
  cascader: {
    /**
     * 是否需要动态加载
     */
    lazy: boolean,
    /**
     * 按照level的顺序设置后端 API 的 action 名称
     */
    actions: Array<string>
  }
}

/**
 * 单层的选项，下拉列表等的选项。value、label
 */
export interface IOptionItem {
  /**
   * 值，提交到后端
   */
  value: number | string,
  /**
   * 标签、文字说明
   */
  label: string
}

/**
 * 多级的选项，级联控件。value、label、children（n级嵌套）
 */
export interface IOptionItemTree {
  /**
   * 值，提交到后端
   */
  value: number | string,
  /**
   * 标签、文字说明
   */
  label: string,
  /**
   * 子选项，可以无限嵌套
   */
  children: Array<IOptionItemTree>
}
import type { IPropsValidation } from './10-base-validation';
import type { IOptionItem, IOptionItemTree, IWebAPI } from './20-form-item';
/**
 * 表单控件的属性，约束必须有的属性
 */
export interface IFromProps {
    /**
     * 表单的 v-model，any
     */
    model: IPropsValidation;
    /**
     * 根据选项过滤后的 model,any
     */
    partModel?: IPropsValidation;
    /**
     * 表单字段的排序、显示依据，Array<number | string>,
     */
    colOrder: IPropsValidation;
    /**
     * 表单的列数，number,
     */
    formColCount: IPropsValidation;
    /**
     * 标签的后缀，string
     */
    labelSuffix: IPropsValidation;
    /**
     * 标签的宽度，string
     */
    labelWidth: IPropsValidation;
    /**
     * 控件的规格，string
     */
    size: IPropsValidation;
    /**
     * 表单子控件的属性，IFormItem
     */
    itemMeta: IPropsValidation;
    /**
     * 分栏的设置，ISubs
     */
    subs: IPropsValidation;
    /**
     * 验证信息，IRuleMeta
     */
    ruleMeta: IPropsValidation;
    /**
     * 子控件的联动关系，IFormColShow
     */
    formColShow: IPropsValidation;
    customerControl: IPropsValidation;
    /**
     * 是否重新加载配置，需要来回取反，boolean
     */
    reload: IPropsValidation;
    /**
     * () => void
     */
    reset: IPropsValidation;
    /**
     * () => void
     */
    events: IPropsValidation;
}
/**
 * 显示控件的联动设置
 */
export interface IFormColShow {
    /**
     * 控件的ID作为key
     */
    [key: string | number]: {
        /**
         * 控件的值作为key，后面的数组里存放需要显示的控件ID
         */
        [id: string | number]: Array<number>;
    };
}
/**
 * 一条验证规则
 */
export interface IRule {
    /**
     * 验证时机：blur、change、click、keyup
     */
    trigger?: string;
    /**
     * 提示消息
     */
    message?: string;
    /**
     * 必填
     */
    required?: boolean;
    /**
     * 数据类型：any、date、url等
     */
    type?: string;
    /**
     * 长度
     */
    len?: number;
    /**
     * 最大值
     */
    max?: number;
    /**
     * 最小值
     */
    min?: number;
    /**
     * 正则
     */
    pattern?: string;
}
/**
 * 表单的验证规则集合
 */
export interface IRuleMeta {
    /**
     * 控件的ID作为key， 一个控件，可以有多条验证规则
     */
    [key: string | number]: Array<IRule>;
}
/**
 * 分栏表单的设置
 */
export interface ISubs {
    type: string;
    cardColCount: number;
    cols: Array<{
        title: string;
        colIds: Array<number>;
    }>;
}
/**
 * 表单控件里的，子控件的属性类型。
 */
export interface IFormItem {
    /**
     * 字段ID、控件ID
     */
    columnId: number | string;
    /**
     * 表单的 model，含义多个属性
     */
    model: any;
    /**
     * 字段名称
     */
    colName: string;
    /**
     * 控件类型
     */
    controlType: number;
    /**
     * 控件备选项，一级或者多级
     */
    optionList: Array<IOptionItem | IOptionItemTree>;
    /**
     * 访问后端API的配置
     */
    webapi: IWebAPI;
    /**
     * 防抖延迟时间，0：不延迟
     */
    delay: number;
    /**
     * 防抖相关的事件
     */
    events: () => void;
    /**
     * 控件的大小
     */
    size: string;
    /**
     * 是否显示清空的按钮
     */
    clearable: boolean;
    /**
     * 控件的扩增属性
     */
    extend: any;
}
import type { IPropsValidation } from './10-base-validation'

import type {
  IOptionItem,
  IOptionItemTree,
  IWebAPI
} from './20-form-item'

// 表单控件 ==========================================================

/**
 * 表单控件的属性，约束必须有的属性
 */
export interface IFromProps {
  /**
   * 表单的 v-model，any
   */
  model: IPropsValidation,
  /**
   * 根据选项过滤后的 model,any
   */
  partModel?: IPropsValidation,
  /**
   * 表单字段的排序、显示依据，Array<number | string>,
   */
  colOrder: IPropsValidation,
  /**
   * 表单的列数，number,
   */
  formColCount: IPropsValidation,
  /**
   * 标签的后缀，string
   */
  labelSuffix: IPropsValidation,
  /**
   * 标签的宽度，string
   */
  labelWidth: IPropsValidation,
  /**
   * 控件的规格，string
   */
  size: IPropsValidation,
  /**
   * 表单子控件的属性，IFormItem
   */
  itemMeta: IPropsValidation,
  /**
   * 分栏的设置，ISubs
   */
  subs: IPropsValidation,
  /**
   * 验证信息，IRuleMeta
   */
  ruleMeta: IPropsValidation,
  /**
   * 子控件的联动关系，IFormColShow
   */
  formColShow: IPropsValidation,
  /*
  * 自定义子控件 key:value形式
  * * key: 编号。1：插槽；100-200：保留编号
  * * value：string：标签；函数：异步组件，类似路由的设置
  */
  customerControl: IPropsValidation,
  /**
   * 是否重新加载配置，需要来回取反，boolean
   */
  reload: IPropsValidation,
  /**
   * () => void
   */
  reset: IPropsValidation,
  /**
   * () => void
   */
  events: IPropsValidation,
}

/**
 * 显示控件的联动设置
 */
export interface IFormColShow {
  /**
   * 控件的ID作为key
   */
  [key: string | number]: {
    /**
     * 控件的值作为key，后面的数组里存放需要显示的控件ID
     */
    [id: string | number]: Array<number>
  }
}

/**
 * 一条验证规则
 */
export interface IRule {
  /**
   * 验证时机：blur、change、click、keyup
   */
  trigger?: string,
  /**
   * 提示消息
   */
  message?: string,
  /**
   * 必填
   */
  required?: boolean,
  /**
   * 数据类型：any、date、url等
   */
  type?: string,
  /**
   * 长度
   */
  len?: number, // 长度
  /**
   * 最大值
   */
  max?: number,
  /**
   * 最小值
   */
  min?: number,
  /**
   * 正则
   */
  pattern?: string
}

/**
 * 表单的验证规则集合
 */
 export interface IRuleMeta {
  /**
   * 控件的ID作为key， 一个控件，可以有多条验证规则
   */
  [key: string | number]: Array<IRule>
}

/**
 * 分栏表单的设置
 */
export interface ISubs {
  type: string, // 分栏类型：card、tab、step、"" （不分栏）
  cardColCount: number, // 分几栏目
  cols: Array<{ // 栏目信息
    title: string, // 栏目名称
    colIds:  Array<number> // 栏目里有哪些控件ID
  }>
}


/**
 * 表单控件里的，子控件的属性类型。
 */
export interface IFormItem {
  /**
   * 字段ID、控件ID
   */
  columnId: number | string,
  /**
   * 表单的 model，含义多个属性
   */
  model: any,
  /**
   * 字段名称
   */
  colName: string,
  /**
   * 控件类型
   */
  controlType: number,
  /**
   * 控件备选项，一级或者多级
   */
  optionList: Array<IOptionItem | IOptionItemTree>,
  /**
   * 访问后端API的配置
   */
  webapi: IWebAPI,
  /**
   * 防抖延迟时间，0：不延迟
   */
  delay: number,
  /**
   * 防抖相关的事件
   */
  events: () => void,
  /**
   * 控件的大小
   */
  size: string,
  /**
   * 是否显示清空的按钮
   */
  clearable: boolean,
  /**
   * 控件的扩增属性
   */
  extend: any
}
import type { IPropsValidation } from './10-base-validation';
import type { IFormItem } from './30-form';
import type { EFindKind } from './enum';
/**
 * 查询控件的属性
 */
export interface IFindProps {
    /**
     * 模块ID，number | string
     */
    moduleId: IPropsValidation;
    /**
     * 显示的控件以及排序， Array<number | string>
     */
    colOrder: IPropsValidation;
    /**
     * 用户输入的查询条件，紧凑型
     */
    findValue: IPropsValidation;
    /**
     * 用户输入的查询条件，标准型
     */
    findArray: IPropsValidation;
    /**
     * 快速查询的字段，Array<number>
     */
    quickFind: IPropsValidation;
    /**
     * 全部查询的字段，Array<number>
     */
    allFind: IPropsValidation;
    /**
     * 自定义的查询方案，Array<number>
     */
    customer: IPropsValidation;
    /**
     *  默认的查询方案，Array<any>
     */
    customerDefault: IPropsValidation;
    /**
     * 是否重新加载配置，需要来回取反，boolean
     */
    reload: IPropsValidation;
    /**
     * 查询子控件的属性，IFormItem
     */
    itemMeta: IPropsValidation;
    /**
     * 查询控件的扩展属性
     */
    [key: string]: IPropsValidation;
}
/**
 * 查询名称
 */
export interface ICus {
    [key: string]: {
        /**
         * 查询方案编号
         */
        id: string | number;
        /**
         * 方案名称
         */
        name: string;
        /**
         * 需要的查询字段ID
         */
        colId: Array<number>;
    };
}
/**
 * 紧凑型查询：{字段名:[查询方式, 查询值]}。。。多个
 */
export interface IFindValue {
    [colName: string | number]: [
        EFindKind,
        string | number | boolean | Array<string | number | boolean> | [any, any]
    ];
}
/**
 * 标准型查询：[{colName: '字段名', findKind: 101, value: 123}]
 */
export interface IFindArray {
    /**
     * 字段名称/字段编号
     */
    colName: string | number;
    /**
     * 查询方式
     */
    findKind: EFindKind;
    /**
     * 查询值，数字、字符串、日期，多条件用数组
     */
    value?: number | string | boolean | Array<string | number | boolean>;
    /**
     * 范围查询的开始的值，number | string | boolean
     */
    valueStart?: number | string | boolean;
    /**
     * 范围查询的结束的，number | string | boolean
     */
    valueEnd?: number | string | boolean;
}
/**
 * 查询控件的属性
 */
export interface IFindModel {
    moduleId: number | string;
    active: any;
    findValue: IFindValue;
    findArray: IFindArray;
    quickFind: Array<number>;
    allFind: Array<number>;
    customer: Array<any>;
    customerDefault: Array<any>;
    reload: boolean;
    itemMeta: IFormItem;
    [key: string]: any;
}

// 组件属性的约束
import type { IPropsValidation } from './10-base-validation'

// 查询子控件的属性
import type { IFormItem } from './30-form'

// 查询方式
import type { EFindKind } from './enum'

// 查询控件 ===========================================================

/**
 * 查询控件的属性
 */
export interface IFindProps {
  /**
   * 模块ID，number | string
   */
  moduleId: IPropsValidation,
  /**
   * 显示的控件以及排序， Array<number | string>
   */
  colOrder: IPropsValidation,
  /**
   * 用户输入的查询条件，紧凑型
   */
  findValue: IPropsValidation,
  /**
   * 用户输入的查询条件，标准型
   */
  findArray: IPropsValidation,
  /**
   * 快速查询的字段，Array<number>
   */
  quickFind: IPropsValidation,
  /**
   * 全部查询的字段，Array<number>
   */
  allFind: IPropsValidation,
  /**
   * 自定义的查询方案，Array<number>
   */
  customer: IPropsValidation,
  /**
   *  默认的查询方案，Array<any>
   */
  customerDefault: IPropsValidation,
  /**
   * 是否重新加载配置，需要来回取反，boolean
   */
  reload: IPropsValidation,
  /**
   * 查询子控件的属性，IFormItem
   */
  itemMeta: IPropsValidation,
  /**
   * 查询控件的扩展属性
   */
  [key: string]: IPropsValidation
}


// 自定义查询方式

/**
 * 查询名称
 */
export interface ICus {
  [key: string]: {
    /**
     * 查询方案编号
     */
    id: string | number,
    /**
     * 方案名称
     */
    name: string,
    /**
     * 需要的查询字段ID
     */
    colId: Array<number>
  }
}

/**
 * 紧凑型查询：{字段名:[查询方式, 查询值]}。。。多个
 */
export interface IFindValue {
  [colName: string | number]: [
    EFindKind,
    string | number | boolean | Array<string | number | boolean> | [any,any]
  ]
}

/**
 * 标准型查询：[{colName: '字段名', findKind: 101, value: 123}]
 */
export interface IFindArray {
  /**
   * 字段名称/字段编号
   */
  colName: string | number,
  /**
   * 查询方式
   */
  findKind: EFindKind,
  /**
   * 查询值，数字、字符串、日期，多条件用数组
   */
  value?: number | string | boolean | Array<string | number | boolean>
  /**
   * 范围查询的开始的值，number | string | boolean
   */
  valueStart?: number | string | boolean,
  /**
   * 范围查询的结束的，number | string | boolean
   */
  valueEnd?: number | string | boolean
}

/**
 * 查询控件的属性
 */
export interface IFindModel {
  moduleId: number | string, // 模块ID
  active: any, // 按钮的meta集合
  // colOrder: Array<number | string>, // 显示的控件以及排序,
  findValue: IFindValue, // 用户输入的查询条件，紧凑型
  findArray: IFindArray, // 用户输入的查询条件，标准型
  quickFind: Array<number> , // 快速查询的字段
  allFind: Array<number> , // 全部查询的字段
  customer: Array<any> , // 自定义的查询方案
  customerDefault: Array<any> // 默认的查询方案
  reload: boolean, // 是否重新加载配置，需要来回取反
  itemMeta: IFormItem, // 表单子控件的属性
  [key: string]: any

}
  import type { IPropsValidation } from './10-base-validation';
import { EAlign } from './enum';
/**
 * 列表的 meta，LowCode 需要
 * * moduleId: number | string,
 * * idName: String,
 * * colOrder: Array<number|string>
 */
export interface IGridMeta {
    /**
     * 模块ID，number | string
     */
    moduleId: number | string;
    /**
     * 主键字段的名称 String，对应 row-key
     */
    idName: string;
    /**
     * 列（字段）显示的顺序 Array<number|string>
     */
    colOrder: Array<number | string>;
}
/**
 * 列表的 props ，和 el-table 的属性名称一致，可以扩展
 */
export interface IGridTable {
    /**
     * table的高度， Number
     */
    height: number;
    /**
     * 斑马纹，Boolean
     */
    stripe: boolean;
    /**
     * 纵向边框，Boolean
     */
    border: boolean;
    /**
     * 列的宽度是否自撑开，Boolean
     */
    fit: boolean;
    /**
     * 要高亮当前行，Boolean
     */
    highlightCurrentRow: boolean;
    /**
     * 列表的扩展属性
     */
    [propName: string]: any;
}
/**
 * 列的属性，基于 el-table-column
 * * id: number | string,
 * * colName: string, 字段名称
 * * label: string, 列的标签、标题
 * * width: number, 列的宽度
 * * align: EAlign, 内容对齐方式
 * * headerAlign: EAlign 列标题对齐方式
 * * 其他属性
 */
export interface IGridItem {
    /**
     * 字段ID、列ID
     */
    id: number | string;
    /**
     * 字段名称
     */
    colName: string;
    /**
     * 列的标签、标题
     */
    label: string;
    /**
     * 列的宽度
     */
    width: number;
    /**
     * 内容对齐方式
     */
    align: EAlign;
    /**
     * 列标题对齐方式
     */
    headerAlign: EAlign;
    [propName: string]: any;
}
/**
 * 列表里选择的数据
 */
export interface IGridSelection {
    /**
     * 单选ID number 、string
     */
    dataId: number | string;
    /**
     * 单选的数据对象 {}
     */
    row: any;
    /**
     * 多选ID []
     */
    dataIds: Array<number | string>;
    /**
     * 多选的数据对象 []
     */
    rows: Array<any>;
}
/**
 * 新，放弃对整体 props 设置类型
 * * 列表控件的属性的类型，
 * * 可以用于 script setup 的 props（但是无法引入），
 * * 无法用于 Option API 的 props
 */
export interface IGridProps {
    /**
     * 列表的 meta，LowCode 需要。
     * * moduleId: number | string,
     * * idName: String,
     * * colOrder: Array<number|string>
     */
    gridMeta: IGridMeta;
    /**
     * 列表的 props ，和 el-table 的属性名称一致，可以扩展
     */
    gridTable: IGridTable;
    /**
     * table的列的属性， Object< IGridItem >
     * * id: number | string,
     * * colName: string, 字段名称
     * * label: string, 列的标签、标题
     * * width: number, 列的宽度
     * * align: EAlign, 内容对齐方式
     * * headerAlign: EAlign 列标题对齐方式
     */
    itemMeta: {
        [key: string | number]: IGridItem;
    };
    /**
     * 选择行的情况：IGridSelection
     * * dataId: '', 单选ID number 、string
     * * row: {}, 单选的数据对象 {}
     * * dataIds: [], 多选ID []
     * * rows: [] 多选的数据对象 []
     */
    selection: IGridSelection;
    /**
     * 绑定的数据 Array， 对应 data
     */
    dataList: Array<any>;
}
/**
 * 列表控件的属性的类型，基于el-table
 * * 放弃了。
 */
export interface IGridPropsComp {
    /**
     * 模块ID，number | string
     */
    moduleId: IPropsValidation;
    /**
     * 主键字段的名称 String，对应 row-key
     */
    idName: IPropsValidation;
    /**
     * table的高度， Number
     */
    height: IPropsValidation;
    /**
     * 列（字段）显示的顺序 Array<number|string>
     */
    colOrder: IPropsValidation;
    /**
     * 斑马纹，Boolean
     */
    stripe: IPropsValidation;
    /**
     * 纵向边框，Boolean
     */
    border: IPropsValidation;
    /**
     * 列的宽度是否自撑开，Boolean
     */
    fit: IPropsValidation;
    /**
     * 要高亮当前行，Boolean
     */
    highlightCurrentRow: IPropsValidation;
    /**
     * table的列的 Object< IGridItem >
     * * id: number | string,
     * * colName: string, 字段名称
     * * label: string, 列的标签、标题
     * * width: number, 列的宽度
     * * align: EAlign, 内容对齐方式
     * * headerAlign: EAlign 列标题对齐方式
     */
    itemMeta: IPropsValidation;
    /**
     * 选择行的情况：IGridSelection
     * * dataId: '', 单选ID number 、string
     * * row: {}, 单选的数据对象 {}
     * * dataIds: [], 多选ID []
     * * rows: [] 多选的数据对象 []
     */
    selection: IPropsValidation;
    /**
     * 绑定的数据 Array， 对应 data
     */
    dataList: IPropsValidation;
    [propName: string]: IPropsValidation;
}
import type { IPropsValidation } from './10-base-validation'

import { EAlign } from './enum'

// 列表控件 ===========================================================


/**
 * 列表的 meta，LowCode 需要
 * * moduleId: number | string,
 * * idName: String,
 * * colOrder: Array<number|string>
 */
export interface IGridMeta {
  /**
   * 模块ID，number | string
   */
  moduleId: number | string,
  /**
   * 主键字段的名称 String，对应 row-key
   */
  idName: string,
  /**
   * 列（字段）显示的顺序 Array<number|string>
   */
  colOrder: Array<number|string>
}

/**
 * 列表的 props ，和 el-table 的属性名称一致，可以扩展
 */
export interface IGridTable {
  /**
   * table的高度， Number
   */
  height: number,
  /**
   * 斑马纹，Boolean
   */
  stripe: boolean,
  /**
   * 纵向边框，Boolean
   */
  border: boolean,
  /**
   * 列的宽度是否自撑开，Boolean
   */
  fit: boolean,
  /**
   * 要高亮当前行，Boolean
   */
  highlightCurrentRow: boolean,
  /**
   * 列表的扩展属性
   */
  [propName: string]: any
}

/**
 * 列的属性，基于 el-table-column
 * * id: number | string,
 * * colName: string, 字段名称
 * * label: string, 列的标签、标题
 * * width: number, 列的宽度
 * * align: EAlign, 内容对齐方式
 * * headerAlign: EAlign 列标题对齐方式
 * * 其他属性
 */
export interface IGridItem {
  /**
   * 字段ID、列ID
   */
  id: number | string,
  /**
   * 字段名称
   */
  colName: string,
  /**
   * 列的标签、标题
   */
  label: string,
  /**
   * 列的宽度
   */
  width: number,
  /**
   * 内容对齐方式
   */
  align: EAlign,
  /**
   * 列标题对齐方式
   */
  headerAlign: EAlign,

  // 其他扩展属性
  [propName: string]: any
}

/**
 * 列表里选择的数据
 */
export interface IGridSelection {
  /**
   * 单选ID number 、string
   */
  dataId: number | string,
  /**
   * 单选的数据对象 {}
   */
  row: any,
  /**
   * 多选ID []
   */
  dataIds: Array<number | string>,
  /**
   * 多选的数据对象 []
   */
  rows: Array<any>
}

/**
 * 新，放弃对整体 props 设置类型
 * * 列表控件的属性的类型，
 * * 可以用于 script setup 的 props（但是无法引入），
 * * 无法用于 Option API 的 props
 */
export interface IGridProps {
  /**
   * 列表的 meta，LowCode 需要。
   * * moduleId: number | string,
   * * idName: String,
   * * colOrder: Array<number|string>
   */
  gridMeta: IGridMeta,
  /**
   * 列表的 props ，和 el-table 的属性名称一致，可以扩展
   */
  gridTable: IGridTable,
  /**
   * table的列的属性， Object< IGridItem >
   * * id: number | string,
   * * colName: string, 字段名称
   * * label: string, 列的标签、标题
   * * width: number, 列的宽度
   * * align: EAlign, 内容对齐方式
   * * headerAlign: EAlign 列标题对齐方式
   */
  itemMeta: {
    [key:string | number]: IGridItem
  },
  /**
   * 选择行的情况：IGridSelection
   * * dataId: '', 单选ID number 、string
   * * row: {}, 单选的数据对象 {}
   * * dataIds: [], 多选ID []
   * * rows: [] 多选的数据对象 []
   */
  selection: IGridSelection, 
    
  /**
   * 绑定的数据 Array， 对应 data
   */
  dataList: Array<any>

}

// 下面放弃了。

/**
 * 列表控件的属性的类型，基于el-table
 * * 放弃了。
 */
export interface IGridPropsComp {
  /**
   * 模块ID，number | string
   */
  moduleId: IPropsValidation,
  /**
   * 主键字段的名称 String，对应 row-key
   */
  idName: IPropsValidation,
  /**
   * table的高度， Number
   */
  height: IPropsValidation,
  /**
   * 列（字段）显示的顺序 Array<number|string>
   */
  colOrder: IPropsValidation,
  /**
   * 斑马纹，Boolean
   */
  stripe: IPropsValidation,
  /**
   * 纵向边框，Boolean
   */
  border: IPropsValidation,
  /**
   * 列的宽度是否自撑开，Boolean
   */
  fit: IPropsValidation,
  /**
   * 要高亮当前行，Boolean
   */
  highlightCurrentRow: IPropsValidation,
  /**
   * table的列的 Object< IGridItem >
   * * id: number | string,
   * * colName: string, 字段名称
   * * label: string, 列的标签、标题
   * * width: number, 列的宽度
   * * align: EAlign, 内容对齐方式
   * * headerAlign: EAlign 列标题对齐方式
   */
  itemMeta: IPropsValidation, // 
  /**
   * 选择行的情况：IGridSelection
   * * dataId: '', 单选ID number 、string
   * * row: {}, 单选的数据对象 {}
   * * dataIds: [], 多选ID []
   * * rows: [] 多选的数据对象 []
   */
  selection: IPropsValidation, 
    
  /**
   * 绑定的数据 Array， 对应 data
   */
  dataList: IPropsValidation,

  // 其他扩展属性
  [propName: string]: IPropsValidation

}
// 按钮组控件 ===========================================================


/// <reference types="node" />
/**
 * 拖拽时记录相关信息
 */
export interface IDragInfo {
    /**
     * 拖拽移除的延迟
     */
    timeout: NodeJS.Timeout;
    oldDom: {
        style: {
            border: string;
            borderStyle: string;
        };
    };
    /**
     * 状态
     */
    state: string;
    /**
     * 拖拽时X坐标
     */
    offsetX: number;
    /**
     * 是否左面释放鼠标
     */
    isLeft: boolean;
    /**
     * 是否按下 ctrl
     */
    ctrl: boolean;
    /**
     * 开始的id
     */
    targetId: string;
    /**
     * 开始的表头，判断是否拖拽出去
     */
    targetLabel: string;
    /**
     * 开始的 target
     */
    target: any;
    /**
     * 结束的ID
     */
    sourceId: string;
    /**
     * 开始的序号
     */
    targetIndex: number;
    /**
     * 结束的序号
     */
    sourceIndex: number;
}
/**
 * 拖拽相关的事件
 */
export interface IDragEvent {
    /**
     * 设置对齐方式的函数
     */
    setAlign: () => void;
    /**
     * 设置排序
     */
    setOrder: () => void;
    /**
     * 移除字段
     */
    removeDom: () => void;
    /**
     * ctrl + 单击控件（字段）触发的事件，可以用于打开修改界面
     */
    modCol: (colId: string | number, event: any) => void;
}

/**
 * 拖拽时记录相关信息
 */
export interface IDragInfo {
  /**
   * 拖拽移除的延迟
   */
  timeout: NodeJS.Timeout,
  oldDom: {
    style: {
      border: string,
      borderStyle: string
    }
  },
  /**
   * 状态
   */
  state: string,
  /**
   * 拖拽时X坐标
   */
  offsetX: number,
  /**
   * 是否左面释放鼠标
   */
  isLeft: boolean,
  /**
   * 是否按下 ctrl
   */
  ctrl: boolean,
  /**
   * 开始的id
   */
  targetId: string,
  /**
   * 开始的表头，判断是否拖拽出去
   */
  targetLabel: string,
  /**
   * 开始的 target
   */
  target: any,
  
  /**
   * 结束的ID
   */
  sourceId: string,
  /**
   * 开始的序号
   */
  targetIndex: number,
  /**
   * 结束的序号
   */
  sourceIndex: number
}

/**
 * 拖拽相关的事件
 */
export interface IDragEvent {
  /**
   * 设置对齐方式的函数
   */
  setAlign: () => void,
  /**
   * 设置排序
   */
  setOrder: () => void,
  /**
   * 移除字段
   */
  removeDom: () => void,
  /**
   * ctrl + 单击控件（字段）触发的事件，可以用于打开修改界面
   */
  modCol: (colId: string | number, event: any) => void
}
import type { AsyncComponentLoader, Component } from 'vue'

// 路由相关的类型

/**
 * 多层的菜单，转换为单层的菜单，便于用 key 查找 菜单
 */
export interface IMenuList {
  [key: string]: {
    /**
     * 菜单ID，类似路由的 name
     */
    menuId: string | number,
    /**
     * 浏览器的标题
     */
    title: string,
    /**
     * url 里的路径
     */
    path: string,
    /**
     * 路径集合 paths + '/' + _menu.path
     */
    paths: string,
    /**
     * 菜单对应的组件
     */
    component: Component
  }
}

/**
 * 一个菜单的类型
 */
export interface IMenu {
  /**
   * 菜单ID，类似路由的 name
   */
  menuId: string | number,
  /**
   * 浏览器的标题
   */
  title: string,
  /**
   * 导航ID
   */
  naviId: string | number,
  /**
   * url 里的路径
   */
  path: string,
  /**
   * 图标，组件类型
   */
  icon: any,
  /**
   * 子菜单
   */
  childrens: Array<IMenu>,
  /**
   * 打开的组件，树枝没有，树叶才有
   */
  component?: AsyncComponentLoader
}

/**
 * 创建路由-菜单时的对象类型
 */
export interface IMenus {
  /**
   * 基础路径，应对网站的二级目录
   */
  baseUrl: string,
  /**
   * 首页组件
   */
  home: any,
  /**
   * 菜单集合
   */
  menus: Array<IMenu>
}

/**
 * 当前激活的菜单的 key 和路径集合
 */
export interface ICurrentRoute {
  /**
   * 当前激活的菜单的ID
   */
  key: string,
  /**
   * 当前激活的多级菜单的路径集合
   */
  paths: Array<string | number>
}

/**
 * 泛型的约束
 */
export interface IProps {
  path: Array<string | number>,
  index: string | number
}

/**
 * 路由的类型
 */
export interface IRouter {
  /**
   * 基础路径，应对网站的二级目录
   */
  baseUrl: string,
  /**
   * 初始的浏览器标题
   */
  baseTitle: string,

  /**
   * 是否刷新进入
   */
  isRefresh: boolean,

  /**
   * 默认的首页
   */
  home: any,
  /**
   * 菜单集合
   */
  menus: Array<IMenu>,

  /**
   * 打开菜单的 tab 集合
   */
  tabs: Array<set>,

  /**
   * 当前激活的信息，菜单的ID和路径
   */
  currentRoute: ICurrentRoute
  
  // 内部方法
  /**
   * 初始化设置
   */
  setup: () => void,

  /**
   * 动态添加新路由
   */
  addRoute: (newMenus: IMenu, props = {}) => void,
  /**
   * 删除路由
   * @param { array } path 菜单的路径，[] 表示根菜单
   * @param { string | number } id 要删除的菜单ID
   */
  removeRoute: (path = [], id = '') => void,
 
  /**
   * 刷新时依据url加载组件
   */
  refresh: () => void

}
/**
 * 控件类型的枚举
 */
export declare const enum EControlType {
    /**
     * 单行文本框
     */
    Text = 101,
    Area = 100,
    Password = 102,
    Email = 103,
    Tel = 104,
    Url = 105,
    Search = 106,
    Autocomplete = 107,
    Color = 108,
    Number = 110,
    Range = 111,
    Rate = 112,
    Date = 120,
    Week = 123,
    TimePicker = 130,
    TimeSelect = 132,
    File = 140,
    Picture = 141,
    Video = 142,
    Checkbox = 150,
    Switch = 151,
    Checkboxs = 152,
    Radios = 153,
    Select = 160,
    Selects = 161,
    SelectGroup = 162,
    SelectGroups = 160,
    SelectCascader = 164,
    SelectTree = 165,
    SelectTrees = 166
}
/**
 * 查询方式的枚举
 */
export declare const enum EFindKind {
    strEqual = 401,
    strNotEqual = 402,
    strInclude = 403,
    strNotInclude = 404,
    strStart = 405,
    strEnd = 406,
    strNotStart = 407,
    strNotEnd = 408,
    numEqual = 411,
    numNotEqual = 412,
    numInclude = 413,
    numNotInclude = 414,
    numStart = 415,
    numEnd = 416,
    numBetween = 417,
    numBetweenEnd = 418,
    numBetweenStart = 419,
    rangeInclude = 440,
    rangeIn = 441,
    rangeNotIn = 442
}
/**
 * 横向对齐方式，左、中、右
 */
export declare const enum EAlign {
    left = "left",
    center = "center",
    right = "right"
}
/**
 * 横向对齐方式，左、中、右
 */
export declare const enum ESize {
    big = "large",
    def = "default",
    small = "small"
}
/**
 * 测试 Symbol
 */
export declare const enum EState {
    plat
}

/**
 * 控件类型的枚举
 */
export const enum EControlType {
  /**
   * 单行文本框
   */
  Text = 101,
  Area = 100,
  Password = 102,
  Email = 103,
  Tel = 104,
  Url = 105,
  Search = 106,
  Autocomplete = 107,
  Color = 108,
  // 数字
  Number = 110,
  Range = 111,
  Rate = 112,
  // 日期
  Date = 120,
  Week = 123,
  // 时间
  TimePicker = 130,
  TimeSelect = 132,
  // 上传
  File = 140,
  Picture = 141,
  Video = 142,
  // 选择等
  Checkbox = 150,
  Switch = 151,
  Checkboxs = 152,
  Radios = 153,
  // 下拉
  Select = 160, // 单选下拉
  Selects = 161, // 多选下拉
  SelectGroup = 162, // 分组下拉单选
  SelectGroups = 160, // 分组下拉多选
  SelectCascader = 164, // 联动下拉
  SelectTree = 165, // 树状下拉
  SelectTrees = 166 // 树状多选
}
// export EControlType


/**
 * 查询方式的枚举
 */
export const enum EFindKind {
  // 字符串
  strEqual = 401, // '{col} = ?'
  strNotEqual = 402, // '{col} <> ?'
  strInclude = 403, // '{col} like "%?%"'
  strNotInclude = 404, // '{col} not like "%?%"'
  strStart = 405, // '{col} like "?%"'
  strEnd = 406, // '{col} like "%?"'
  strNotStart = 407, // '{col} not like "?%"'
  strNotEnd = 408, // '{col} not like "%?"'
  // 数字、日期时间
  numEqual = 411, // '{col} = ?'
  numNotEqual = 412, // '{col} <> ?'
  numInclude = 413, // '{col} > ?'
  numNotInclude = 414, // '{col} >= ?'
  numStart = 415, // '{col} < ?'
  numEnd = 416, // '{col} <= ?'
  numBetween = 417, // '{col} between ? and ?'
  numBetweenEnd = 418, // '{col} > ? and {col} <= ?'
  numBetweenStart = 419, // '{col} >= ? and {col} < ?'
  // 多选
  rangeInclude = 440, // '( {col} like %?% or {col} like %?% ... ) '
  rangeIn = 441, // '{col} in (?)'
  rangeNotIn = 442 // '{col} not in (?)'
  
}

/**
 * 横向对齐方式，左、中、右
 */
export const enum EAlign {
  left = 'left',
  center = 'center',
  right = 'right'
}

/**
 * 横向对齐方式，左、中、右
 */
export const enum ESize {
  big = 'large',
  def = 'default',
  small = 'small'
}

/**
 * 测试 Symbol 
 */
export const enum EState {
  plat = Symbol('测试Symbol的枚举')
}


// 键盘快捷键 ===========================================================



// 路由 ===========================================================


// 拖拽 ===========================================================
