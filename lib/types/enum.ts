
/**
 * 控件类型的枚举
 */
export const enum EControlType {
  /**
   * 单行文本框
   */
  Text = 101,
  Area = 100,
  Password = 102,
  Email = 103,
  Tel = 104,
  Url = 105,
  Search = 106,
  Autocomplete = 107,
  Color = 108,
  // 数字
  Number = 110,
  Range = 111,
  Rate = 112,
  // 日期
  Date = 120,
  Week = 123,
  // 时间
  TimePicker = 130,
  TimeSelect = 132,
  // 上传
  File = 140,
  Picture = 141,
  Video = 142,
  // 选择等
  Checkbox = 150,
  Switch = 151,
  Checkboxs = 152,
  Radios = 153,
  // 下拉
  Select = 160, // 单选下拉
  Selects = 161, // 多选下拉
  SelectGroup = 162, // 分组下拉单选
  SelectGroups = 160, // 分组下拉多选
  SelectCascader = 164, // 联动下拉
  SelectTree = 165, // 树状下拉
  SelectTrees = 166 // 树状多选
}
// export EControlType


/**
 * 查询方式的枚举
 */
export const enum EFindKind {
  // 字符串
  strEqual = 401, // '{col} = ?'
  strNotEqual = 402, // '{col} <> ?'
  strInclude = 403, // '{col} like "%?%"'
  strNotInclude = 404, // '{col} not like "%?%"'
  strStart = 405, // '{col} like "?%"'
  strEnd = 406, // '{col} like "%?"'
  strNotStart = 407, // '{col} not like "?%"'
  strNotEnd = 408, // '{col} not like "%?"'
  // 数字、日期时间
  numEqual = 411, // '{col} = ?'
  numNotEqual = 412, // '{col} <> ?'
  numInclude = 413, // '{col} > ?'
  numNotInclude = 414, // '{col} >= ?'
  numStart = 415, // '{col} < ?'
  numEnd = 416, // '{col} <= ?'
  numBetween = 417, // '{col} between ? and ?'
  numBetweenEnd = 418, // '{col} > ? and {col} <= ?'
  numBetweenStart = 419, // '{col} >= ? and {col} < ?'
  // 多选
  rangeInclude = 440, // '( {col} like %?% or {col} like %?% ... ) '
  rangeIn = 441, // '{col} in (?)'
  rangeNotIn = 442 // '{col} not in (?)'
  
}

/**
 * 横向对齐方式，左、中、右
 */
export const enum EAlign {
  left = 'left',
  center = 'center',
  right = 'right'
}

/**
 * 控件尺寸，大中小
 */
export const enum ESize {
  big = 'large',
  def = 'default',
  small = 'small'
}

/**
 * 表单控件的分栏类型。
 * * card：卡片
 * * tab：多标签
 * * step：分步
 * * no：不分栏
 */
export const enum ESubType {
  card = 'card',
  tab = 'tab',
  step = 'step',
  no = ''
}
