
import type { EControlType } from './enum'
type idType = string | number

/**
 * 访问后端API的配置
 */
export interface IWebAPI {
  /**
   * 服务ID/服务编号/服务名称
   */
  serviceId: idType,
  /**
   * 动作ID/动作名称
   */
  actionId: idType,
  /**
   * 主键字段值
   */
  dataId: idType,
  /**
   * form 的 body
   */
  body: any,
  /**
   * 联动组件需要的设置
   */
  cascader: {
    /**
     * 是否需要动态加载
     */
    lazy: boolean,
    /**
     * 按照level的顺序设置后端 API 的 action 名称
     */
    actions: Array<string>,
    /**
     * 每次提交时，父节点的ID值
     */
    parentId: string | number
  }
}

/**
 * 单层的选项，下拉列表等的选项。value、label
 */
export interface IOptionList {
  /**
   * 值，提交到后端，可以是数字或者文本
   */
  value: number | string,
  /**
   * 标签、文字说明
   */
  label: string,
  /**
   * 是否可用
   */
  disabled: boolean
}

/**
 * 多级的选项，级联控件。value、label、children（n级嵌套）
 */
export interface IOptionTree {
  /**
   * 值，提交到后端，可以是数字或者文本
   */
  value: idType,
  /**
   * 标签、文字说明
   */
  label: string,
  /**
   * 是否可用
   */
  disabled: boolean,
  /**
   * 子选项，可以无限嵌套
   */
  children: Array<IOptionTree>
}

/**
 * 防抖相关的事件
 */
export interface IEventDebounce {
  /**
   * 立即更新
   */
  run: () => void,
  /**
   * 清掉上一次的计时
   */
  clear: () => void
}

/**
 * 子控件的低代码需要的数据
 */
export interface IFormItemMeta {
  /**
   * -- 字段ID、控件ID
   */
  columnId?: idType,
  /**
   * -- 字段名称
   */
  colName: string,
  /**
   * -- 字段的中文名称，标签
   */
  label?: string,
  /**
   * -- 子控件类型，number，EControlType
   */
  controlType: EControlType | number,
  /**
   * 子控件的默认值
   */
  defValue: any,
  /**
   * -- 一个控件占据的空间份数。
   */
  colCount?: number,
  /**
   * 访问后端API的配置信息，有备选项的控件需要
   */
  webapi?: IWebAPI,
  /**
   * -- 防抖延迟时间，0：不延迟
   */
  delay: number,
  /**
   * 防抖相关的事件
   */
  events?: IEventDebounce,
}

/**
 * 表单控件的子控件的 props。
 */
export interface IFormItemProps {
  /**
   * 低代码需要的数据
   */
  formItemMeta: IFormItemMeta,
  /**
   * 子控件备选项，一级或者多级
   */
  optionList: Array<IOptionList | IOptionTree>,
  /**
   * 表单的 model，含义多个属性
   */
  model: any,
  /**
   * 是否显示清空的按钮
   */
  clearable: boolean,
  /**
   * 浮动提示信息
   */
  title: string,
  /**
   * 组件尺寸
   */
  size: string,
   /**
   * 子控件的扩展属性
   */
  [key: string]: any
}

/**
 * 表单控件里面的子控件的 meta 集合
 */
 export interface IFormItemList {
  /**
   * 表单控件里的子控件的 meta
   */
  [key: string | number]: IFormItemProps | any
}

/**
 * 【不用了】表单控件里的，子控件的属性类型。
 */
export interface IFormItem_old {
  /**
   * 字段ID、控件ID
   */
  columnId: number | string,
  /**
   * 表单的 model，含义多个属性
   */
  model: any,
  /**
   * 字段名称
   */
  colName: string,
  /**
   * 控件类型
   */
  controlType: number,
  /**
   * 子控件的默认值
   */
  defValue: any,
  /**
   * 控件备选项，一级或者多级
   */
  optionList: Array<IOptionList | IOptionTree>,
  /**
   * 访问后端API的配置
   */
  webapi: IWebAPI,
  /**
   * 防抖延迟时间，0：不延迟
   */
  delay: number,
  /**
   * 防抖相关的事件
   */
  events: () => void,
  /**
   * 控件的大小
   */
  size: string,
  /**
   * 是否显示清空的按钮
   */
  clearable: boolean,
  /**
   * 控件的扩展属性
   */
  extend: any
}
