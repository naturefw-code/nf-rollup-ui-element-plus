import type { IPropsValidation } from './10-base-validation'

import type { IFormItem, IFormItemList } from './20-form-item'

import type { ESubType, ESize } from './enum'

// 表单控件 ==========================================================

/**
 * 表单控件的属性，约束必须有的属性
 */
export interface IFromProps_old {
  /**
   * 表单的 v-model，any
   */
  model: IPropsValidation,
  /**
   * 根据选项过滤后的 model,any
   */
  partModel?: IPropsValidation,
  /**
   * 表单字段的排序、显示依据，Array<number | string>,
   */
  colOrder: IPropsValidation,
  /**
   * 表单的列数，number,
   */
  formColCount: IPropsValidation,
  /**
   * 标签的后缀，string
   */
  labelSuffix: IPropsValidation,
  /**
   * 标签的宽度，string
   */
  labelWidth: IPropsValidation,
  /**
   * 控件的规格，string
   */
  size: IPropsValidation,
  /**
   * 表单子控件的属性，IFormItem
   */
  itemMeta: IPropsValidation,
  /**
   * 分栏的设置，ISubs
   */
  subs: IPropsValidation,
  /**
   * 验证信息，IRuleMeta
   */
  ruleMeta: IPropsValidation,
  /**
   * 子控件的联动关系，IFormColShow
   */
  formColShow: IPropsValidation,
  /*
  * 自定义子控件 key:value形式
  * * key: 编号。1：插槽；100-200：保留编号
  * * value：string：标签；函数：异步组件，类似路由的设置
  */
  customerControl: IPropsValidation,
  /**
   * 是否重新加载配置，需要来回取反，boolean
   */
  reload: IPropsValidation,
  /**
   * () => void
   */
  reset: IPropsValidation,
  /**
   * () => void
   */
  events: IPropsValidation,
}

/**
 * 显示控件的联动设置
 */
export interface IFormColShow {
  /**
   * 控件的ID作为key，每个控件值对应一个数组，数组里面是需要显示的控件ID。
   */
  [key: string | number]: {
    /**
     * 控件的值作为key，后面的数组里存放需要显示的控件ID
     */
    [id: string | number]: Array<number>
  }
}

/**
 * 一条验证规则，一个控件可以有多条验证规则
 */
export interface IRule {
  /**
   * 验证时机：blur、change、click、keyup
   */
  trigger?: string,
  /**
   * 提示消息
   */
  message?: string,
  /**
   * 必填
   */
  required?: boolean,
  /**
   * 数据类型：any、date、url等
   */
  type?: string,
  /**
   * 长度
   */
  len?: number, // 长度
  /**
   * 最大值
   */
  max?: number,
  /**
   * 最小值
   */
  min?: number,
  /**
   * 正则
   */
  pattern?: string
}

/**
 * 表单的验证规则集合
 */
 export interface IRuleMeta {
  /**
   * 控件的ID作为key， 一个控件，可以有多条验证规则
   */
  [key: string | number]: Array<IRule>
}

/**
 * 分栏表单的设置
 */
export interface ISubs {
  type: ESubType, // 分栏类型：card、tab、step、"" （不分栏）
  cardColCount: number, // 分几栏目
  subs: Array<{ // 栏目信息
    title: string, // 栏目名称
    colIds:  Array<number> // 栏目里有哪些控件ID
  }>
}

/**
 * 表单控件需要的 meta
 */
export interface IFromMeta {
  /**
   * 模块编号，综合使用的时候需要
   */
  moduleId: number | string,
  /**
   * 表单编号，一个模块可以有多个表单
   */
  formId: number | string,
  /**
   * 表单字段的排序、显示依据，Array<number | string>,
   */
  colOrder: Array<number | string>,
  /**
   * 表单的列数，分为几列 number,
   */
  formColCount: number,
   
  /**
   * 表单子控件的属性，IFormItem
   */
  itemMeta: IFormItemList,
  /**
   * 分栏的设置，ISubs
   */
  subs: ISubs,
  /**
   * 验证信息，IRuleMeta
   */
  ruleMeta: IRuleMeta,
  /**
   * 子控件的联动关系，IFormColShow
   */
  formColShow: IFormColShow,
  /*
  * 自定义子控件 key:value形式
  * * key: 编号。1：插槽；100-200：保留编号
  * * value：string：标签；函数：异步组件，类似路由的设置
  */
  customerControl: any,
  /**
   * 是否重新加载配置，需要来回取反，boolean
   */
  reload: boolean,
  /**
   * () => void
   */
  reset: () => void,
  /**
   * () => void
   */
  events: () => void
}

/**
 * 表单控件的属性，约束必须有的属性
 */
export interface IFromProps {
  /**
   * 表单的 model，对象，包含多个字段。
   */
  model: any,
  /**
   * 根据选项过滤后的 model,any
   */
  partModel?: any,
  /**
   * 表单控件需要的 meta
   */
  formMeta: IFromMeta,

  /**
   * 标签的后缀，string
   */
  labelSuffix: string,
  /**
  * 标签的宽度，string
  */
  labelWidth: string,
  /**
  * 控件的规格，string
  */
  size: ESize,
  /**
  * 其他扩展属性
  */
  [propName: string]: any
 
}