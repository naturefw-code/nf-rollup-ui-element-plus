import type { IPropsValidation } from './10-base-validation';
/**
 * 【不用了】表单子控件的共用属性。约束必须有的属性
 */
export interface ItemProps_old {
    /**
     * 字段ID、控件ID，sting | number
     */
    columnId: IPropsValidation;
    /**
     * 表单的 model，含义多个属性，any
     */
    model: IPropsValidation;
    /**
     * 字段名称，string
     */
    colName: IPropsValidation;
    /**
     * 控件类型，number
     */
    controlType: IPropsValidation;
    /**
     * 控件备选项，一级或者多级，Array<IOptionItem | IOptionItemTree>
     */
    optionList: IPropsValidation;
    /**
     * 访问后端API的配置，IWebAPI
     */
    webAPI: IPropsValidation;
    /**
     * 防抖延迟时间，0：不延迟，number
     */
    delay: IPropsValidation;
    /**
     * 防抖相关的事件() => void
     */
    events: IPropsValidation;
    /**
     * 控件的大小，string
     */
    size: IPropsValidation;
    /**
     * 是否显示清空的按钮，boolean
     */
    clearable: IPropsValidation;
    /**
     * 控件的扩展属性，any
     */
    extend: IPropsValidation;
}
/**
 * 访问后端API的配置
 */
export interface IWebAPI {
    /**
     * 服务ID/服务编号/服务名称
     */
    serviceId: number | string;
    /**
     * 动作ID/动作名称
     */
    actionId: number | string;
    /**
     * 主键字段值
     */
    dataId: number | string;
    /**
     * form 的 body
     */
    body: any;
    /**
     * 联动组件需要的设置
     */
    cascader: {
        /**
         * 是否需要动态加载
         */
        lazy: boolean;
        /**
         * 按照level的顺序设置后端 API 的 action 名称
         */
        actions: Array<string>;
        /**
         * 每次提交时，父节点的ID值
         */
        parentId: string | number;
    };
}
/**
 * 单层的选项，下拉列表等的选项。value、label
 */
export interface IOptionList {
    /**
     * 值，提交到后端
     */
    value: number | string;
    /**
     * 标签、文字说明
     */
    label: string;
}
/**
 * 多级的选项，级联控件。value、label、children（n级嵌套）
 */
export interface IOptionTree {
    /**
     * 值，提交到后端
     */
    value: number | string;
    /**
     * 标签、文字说明
     */
    label: string;
    /**
     * 子选项，可以无限嵌套
     */
    children: Array<IOptionTree>;
}
/**
 * 防抖相关的事件
 */
export interface IEventDebounce {
    /**
     * 立即更新
     */
    run: () => void;
    /**
     * 清掉上一次的计时
     */
    clear: () => void;
}
/**
 * 表单控件里的，子控件的属性类型。
 */
export interface IFormItem {
    /**
     * 字段ID、控件ID
     */
    columnId: number | string;
    /**
     * 表单的 model，含义多个属性
     */
    model: any;
    /**
     * 字段名称
     */
    colName: string;
    /**
     * 控件类型
     */
    controlType: number;
    /**
     * 子控件的默认值
     */
    defValue: any;
    /**
     * 控件备选项，一级或者多级
     */
    optionList: Array<IOptionList | IOptionTree>;
    /**
     * 访问后端API的配置
     */
    webapi: IWebAPI;
    /**
     * 防抖延迟时间，0：不延迟
     */
    delay: number;
    /**
     * 防抖相关的事件
     */
    events: () => void;
    /**
     * 控件的大小
     */
    size: string;
    /**
     * 是否显示清空的按钮
     */
    clearable: boolean;
    /**
     * 控件的扩展属性
     */
    extend: any;
}
/**
 * 表单控件里面的子控件的 meta 集合
 */
export interface IFormItemList {
    /**
     * 表单控件里的子控件的 meta
     */
    [key: string | number]: IFormItem | any;
}
