import type { IPropsValidation } from './10-base-validation';
import type { IFormItem } from './30-form';
import type { EFindKind } from './enum';
/**
 * 查询控件的属性
 */
export interface IFindProps {
    /**
     * 模块ID，number | string
     */
    moduleId: IPropsValidation;
    /**
     * 显示的控件以及排序， Array<number | string>
     */
    colOrder: IPropsValidation;
    /**
     * 用户输入的查询条件，紧凑型
     */
    findValue: IPropsValidation;
    /**
     * 用户输入的查询条件，标准型
     */
    findArray: IPropsValidation;
    /**
     * 快速查询的字段，Array<number>
     */
    quickFind: IPropsValidation;
    /**
     * 全部查询的字段，Array<number>
     */
    allFind: IPropsValidation;
    /**
     * 自定义的查询方案，Array<number>
     */
    customer: IPropsValidation;
    /**
     *  默认的查询方案，Array<any>
     */
    customerDefault: IPropsValidation;
    /**
     * 是否重新加载配置，需要来回取反，boolean
     */
    reload: IPropsValidation;
    /**
     * 查询子控件的属性，IFormItem
     */
    itemMeta: IPropsValidation;
    /**
     * 查询控件的扩展属性
     */
    [key: string]: IPropsValidation;
}
/**
 * 查询名称
 */
export interface ICus {
    [key: string]: {
        /**
         * 查询方案编号
         */
        id: string | number;
        /**
         * 方案名称
         */
        name: string;
        /**
         * 需要的查询字段ID
         */
        colId: Array<number>;
    };
}
/**
 * 紧凑型查询：{字段名:[查询方式, 查询值]}。。。多个
 */
export interface IFindValue {
    [colName: string | number]: [
        EFindKind,
        string | number | boolean | Array<string | number | boolean> | [any, any]
    ];
}
/**
 * 标准型查询：[{colName: '字段名', findKind: 101, value: 123}]
 */
export interface IFindArray {
    /**
     * 字段名称/字段编号
     */
    colName: string | number;
    /**
     * 查询方式
     */
    findKind: EFindKind;
    /**
     * 查询值，数字、字符串、日期，多条件用数组
     */
    value?: number | string | boolean | Array<string | number | boolean>;
    /**
     * 范围查询的开始的值，number | string | boolean
     */
    valueStart?: number | string | boolean;
    /**
     * 范围查询的结束的，number | string | boolean
     */
    valueEnd?: number | string | boolean;
}
/**
 * 查询控件的属性
 */
export interface IFindModel {
    moduleId: number | string;
    active: any;
    findValue: IFindValue;
    findArray: IFindArray;
    quickFind: Array<number>;
    allFind: Array<number>;
    customer: Array<any>;
    customerDefault: Array<any>;
    reload: boolean;
    itemMeta: IFormItem;
    [key: string]: any;
}
