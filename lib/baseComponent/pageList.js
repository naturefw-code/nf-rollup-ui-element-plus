
import { defineComponent } from 'vue'
import BaseComp from './baseComponent.js'

export default function createPageList(info) {
  // 处理 name、inheritAttrs、components、props
  const list = new BaseComp(info)

  list.setup = (props, ctx) => {
    const parnet = list._setup(props, ctx)

    return {
      ...parnet
    }
  }

  return  defineComponent(list)
}