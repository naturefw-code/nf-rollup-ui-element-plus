declare const _default: import("vue").DefineComponent<{
    model: {
        type: import("vue").PropType<any>;
        required: boolean;
    };
    partModel: {
        type: import("vue").PropType<any>;
        default: () => {};
    };
    formMeta: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/30-form").IFromMeta>;
        default: () => {};
    };
    itemMeta: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/20-form-item").IFormItemList>;
        default: () => {};
    };
    labelSuffix: {
        type: StringConstructor;
        default: string;
    };
    labelWidth: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/enum").ESize>;
        default: import("@naturefw/ui-core/dist/types/enum").ESize;
    };
}, {
    itemMeta: import("@naturefw/ui-core/dist/types/20-form-item").IFormItemList;
    cardOrder: {
        title: string;
        colIds: number[];
    }[];
    cardSpan: import("vue").ComputedRef<number>;
    showCol: any;
    formColSpan: any;
    setFormColShow: any;
    formControl: import("vue").Ref<null>;
    moduleId: string | number;
    formId: string | number;
    colOrder: (string | number)[];
    columnsNumber: number;
    subMeta: import("@naturefw/ui-core/dist/types/30-form").ISubMeta;
    ruleMeta: import("@naturefw/ui-core/dist/types/30-form").IRuleMeta;
    linkageMeta: import("@naturefw/ui-core/dist/types/30-form").ILinkageMeta;
    customerControl: any;
    reload: boolean;
    reset: () => void;
    events: () => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    model: {
        type: import("vue").PropType<any>;
        required: boolean;
    };
    partModel: {
        type: import("vue").PropType<any>;
        default: () => {};
    };
    formMeta: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/30-form").IFromMeta>;
        default: () => {};
    };
    itemMeta: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/20-form-item").IFormItemList>;
        default: () => {};
    };
    labelSuffix: {
        type: StringConstructor;
        default: string;
    };
    labelWidth: {
        type: StringConstructor;
        default: string;
    };
    size: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/enum").ESize>;
        default: import("@naturefw/ui-core/dist/types/enum").ESize;
    };
}>>, {
    partModel: any;
    formMeta: import("@naturefw/ui-core/dist/types/30-form").IFromMeta;
    itemMeta: import("@naturefw/ui-core/dist/types/20-form-item").IFormItemList;
    labelSuffix: string;
    labelWidth: string;
    size: import("@naturefw/ui-core/dist/types/enum").ESize;
}>;
export default _default;
