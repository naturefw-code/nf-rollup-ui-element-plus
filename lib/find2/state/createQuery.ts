
import { typeName } from '@naturefw/nf-tool'

// 类型
import type {
  IFindState,
  IFindModel
} from '../../map'

/**
 * 判断有没有查询值
 * @param val 查询值
 * @returns true：没有查询值；false：有查询值
 *  'function',
  * *   'async',
  * *   'object',
  * *   'array',
  * *   'string',
  * *   'number',
  * *   'bigInt',
  * *   'regExp',
  * *   'date',
  * *   'map',
  * *   'set',
  * *   'promise',
  * *   'symbol',
  * *   'math',
  * *   'null',
  * *   'undefined'
 */
const _isEmpty = (val) => {
  switch (typeName(val)) {
    case null:
    case 'null':
    case 'undefined':
      return true
      break
    case 'string':
      return val.length === 0
      break
    case 'number':
    case 'bigInt':
      return isNaN(val)
      break
    case 'array':
      return val.length === 0
      break
    default:
      return false
      break
  }
}

/**
 * 处理多级联动的查询结果
 * @param colName 字段名称
 * @param model 查询结构，完整
 * @param state 简洁型的查询结果
 * @returns 返回标准结构，数组形式，多个
 */
const _selectCascader = (colName: string, model: IFindModel, state: IFindState) => {
  // 简洁模式
  const colNames = colName.split('_')
  colNames.forEach((col: string, index: number) => {
    if (index < model.value.length) {
      state.querys.push({
        colName: col,
        findKind: model.useKind,
        value: model.value[index],
        valueStart: '',
        valueEnd: ''
      })
      state.queryMini[col] = [model.useKind, model.value[index]]
    } else {
      // 没有
      delete state.queryMini[col]
    }
  })
}

/**
 * 范围的拆分
 * @param colName 字段名称
 * @param model 查询结构，完整
 * @param state 简洁型的查询结果
 * @returns 返回标准结构，数组形式，多个
 */
const _selectCascader2 = (colName: string, model: IFindModel, state: IFindState) => {

  // 简洁模式
  const colNames = colName.split('_')
  // 开始
  state.querys.push({
    colName: colNames[0],
    findKind: model.useKind,
    value: '',
    valueStart: model.value[0],
    valueEnd: model.value[1]
  })
  state.queryMini[colNames[0]] = [model.useKind, [model.value[0], model.value[1]]]
  // 结束
  state.querys.push({
    colName: colNames[1],
    findKind: model.useKind,
    value: '',
    valueStart: model.valueEnd[0],
    valueEnd: model.valueEnd[1]
  })
  state.queryMini[colNames[1]] = [model.useKind, [ model.valueEnd[0], model.valueEnd[1]]]
}

/**
 * 创建需要的查询条件
 * @param state 
 */
export default function createQuery(state: IFindState) {
  // 清空
  state.querys.length = 0
  for (const key in state.queryMini) {
    delete state.queryMini[key]
  }

  // 记录当前查询字段
  const tmpFindCol:Array<number | string> = []

  for (const [key, model ] of Object.entries(state.findModel)) {
    const {
      colName,
      controlType
    } = state.itemMeta[key]?.formItemMeta

    // 先判断查询方式，范围、一个（包含）
    if (model.useKind >= 17 && model.useKind <= 19) {
      // 范围，两个查询条件
      if (_isEmpty(model.value) || _isEmpty(model.valueEnd)) {
        // 没有查询值，删除
      } else {
        // 有查询条件
        tmpFindCol.push(key)
        // 判断是不是多字段名
        if (colName.split('_').length > 1) {
          _selectCascader2(colName, model, state)
        } else {
          state.querys.push({
            colName: colName,
            findKind: model.useKind,
            value: '',
            valueStart: model.value,
            valueEnd: model.valueEnd
          })
          state.queryMini[colName] = [model.useKind, [model.value, model.valueEnd]]
        }
      }
    } else {
      // 一个查询条件，或则 in 
      if (_isEmpty(model.value)) {
        // 没有查询值，删除
      } else {
        // 有查询条件
        if (model.value) {
          // 一个查询条件，
          tmpFindCol.push(key)
          // 判断是不是多字段名
          if (colName.split('_').length > 1) {
            _selectCascader(colName, model, state)
          } else {
            state.querys.push({
              colName: colName,
              findKind: model.useKind,
              value: model.value,
              valueStart: '',
              valueEnd: ''
            })
            state.queryMini[colName] = [model.useKind, model.value]
          }
        }
      }
    }

    // 如果是 “全部查询”，那么修改 快捷查询
    if (state.showMoreFind && tmpFindCol.length > 0) {
      // 清空临时
      state.tmpQuickFind.length = 0
      state.tmpQuickFind.push(...tmpFindCol)
    }

  }
}