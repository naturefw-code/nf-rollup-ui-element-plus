
// 查询相关的状态

import type { InjectionKey } from 'vue'
import { watch } from 'vue'
// 状态
import { defineStore, useStoreLocal } from '@naturefw/nf-state'
import type { IState } from '@naturefw/nf-state/dist/type'
// 类型
import type {
  IFindState,
  IFindProps
} from '../../map'

import createFindModel from './createModel'
import createQuery from './createQuery'

// 状态的标识，避免命名冲突
const flag = Symbol('find-control') as InjectionKey<string>


/**
 * 注册局部状态
 * * 部门信息管理用的状态 : IMetaDataState & IState
 * @param props 查询控件的 props
 * @returns 
 */
export function regFindState(props: IFindProps): IFindState & IState {
  // 定义 角色用的状态
  const state = defineStore<IFindState>(flag, {
    state: (): IFindState => {
      return {
        showMoreFind: false, // 是否显示更多的查询条件
        findModel: {}, // 工作簿的名称集合
        queryMini: {}, // 用户输入的查询条件，紧凑型
        querys: [], // 用户输入的查询条件，标准型
        itemMeta: props.itemMeta,
        quickFind: props.findMeta.quickFind, // 快速查询的字段
        tmpQuickFind: [], // 绑定快捷查询
        allFind: props.findMeta.allFind, // 全部查询的字段
        customer: [], // 自定义的查询方案
        customerDefault: props.findMeta.customerDefault // 默认的查询方案
      }
    },
    getters: {
    },
    actions: {
      
    }
  },
  { isLocal: true }
  )

  // 创建表单查询表单的 model
  state.findModel = createFindModel(state)
  state.queryMini = props.queryMini
  state.querys = props.querys
  // 设置快捷查询
  state.tmpQuickFind.push(...state.quickFind)
  // 记录查询方案
  state.customer = Array.isArray(props.findMeta.customer) ? props.findMeta.customer : []

  // 生成 查询
  watch(state.findModel, () => {
    createQuery(state)
  })
 
  return state
}

/**
 * 子组件获取状态
 */
export const getFindState = (): IFindState & IState => {
  return useStoreLocal<IFindState & IState>(flag) as IFindState & IState
}
