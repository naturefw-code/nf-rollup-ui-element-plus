// 操作按钮
import nfButton from './button/button.vue'

import { AllFormItem, formItemKey } from './form-item/_map-form-item'
// import { AllFormItem, formItemKey } from './form/item/_map-form-item-async'

// 查询
import nfFind from './find2/find-div.vue'

// 菜单
import nfMenu from './menu/menu.vue'
// 路由视图
import nfRouterView from './menu/router-view.vue'
import nfRouterViewTabs from './menu/router-view-tabs.vue'

import loading from './js/loading'

// 列表
import nfGrid from './grid/grid-table.vue'
import nfGridSlot from './grid/grid-slot.vue'
import nfGridSlotCol from './grid/grid-slot-col.vue'

// 表单
import nfForm from './form/form-div.vue'
import nfFormSlot from './form/form-div-slot.vue'
import nfFormCard from './form/form-div-card.vue'
import nfFormTab from './form/form-div-tab.vue'
import nfFormStep from './form/form-div-step.vue'

// 拖拽
import { _dialogDrag, install as dialogDrag } from './drag/dialogDrag'
// import findDrag from './drag/findDrag.js'
import { _formDrag, install as formDrag } from './drag/formDrag'
import { _gridDrag, install as gridDrag} from './drag/girdDrag'

// ===

// 设置表单控件的子控件
import { config } from './config'


import dayjs from 'dayjs'

import isoWeeksInYear from 'dayjs/plugin/isoWeeksInYear'
import isLeapYear from 'dayjs/plugin/isLeapYear' // dependent on isLeapYear plugin
dayjs.extend(isoWeeksInYear)
dayjs.extend(isLeapYear)

import isoWeek from 'dayjs/plugin/isoWeek'
dayjs.extend(isoWeek)


// 全局注册组件和自定义指令
const nfElementPlus = (app: any, option: any) => {
  // 按钮
  app.component('nfButton', nfButton)
  // 表单 nfFormStep
  app.component('nfForm', nfForm)
  app.component('nfFormCard', nfFormCard)
  app.component('nfFormTab', nfFormTab)
  app.component('nfFormStep', nfFormStep)

  // 查询
  app.component('nfFind', nfFind)
  // 列表
  app.component('nfGrid', nfGrid)
  app.component('nfGridSlot', nfGridSlot)
  app.component('nfGridSlotCol', nfGridSlotCol)
  // 菜单
  app.component('nfMenu', nfMenu)
  // 路由视图
  app.component('nfRouterView', nfRouterView)
  app.component('nfRouterViewTabs', nfRouterViewTabs)

  // 表单子控件
  for (const key in AllFormItem) {
    app.component(key, AllFormItem[key])
  }

  // 自定义指令
  app.directive('dialogDrag', _dialogDrag)
  app.directive('formDrag', _formDrag)
  app.directive('gridDrag', _gridDrag)

}

// UMD 的注册
const install = nfElementPlus

import {
  serviceConfig, // 配置
  service, // 访问后端API
  lifecycle, // 生命周期

  // 简易路由
  createRouter,
  useRouter,

  itemProps,
  createDataList,
  createModel, // 创建表单需要的 v-model
  loadController, // 加载后端字典
  itemController, // 表单子控件的控制类
  formController // 表单控件的控制类
} from './map'


export {

  serviceConfig, // 配置
  service, // 访问后端API
  lifecycle, // 生命周期

  // 简易路由
  createRouter,
  useRouter,
  
  config, // 设置表单需要的子控件
  
  nfElementPlus, // 安装插件
  install,
  loading, // 加载动画

  // 菜单
  nfMenu,
  // 路由视图
  nfRouterView,
  nfRouterViewTabs,
  // 表单
  nfForm, // 表单控件
  nfFormCard, // 分 card 的表单控件
  nfFormTab, // 分 tab 的表单控件
  nfFormStep, // 分步骤 的表单
  AllFormItem, // 表单子控件集合
  formItemKey, // id转换为控件
  // 查询
  nfFind, // 查询控件
  // 列表
  createDataList,
  nfGrid, // 列表控件
  nfGridSlot,
  nfGridSlotCol,
  // 操作按钮
  nfButton,

  // 表单
  nfFormSlot,
  itemProps,
  createModel, // 创建表单需要的 v-model
  loadController, // 加载后端字典
  itemController, // 表单子控件的控制类
  formController, // 表单控件的控制类
  
  // 拖拽
  _dialogDrag, // 拖拽对话框
  _gridDrag,
  _formDrag, // 表单
  // findDrag,
  dialogDrag,
  formDrag,
  gridDrag
}