import nfButton from './button/button.vue';
import { AllFormItem, formItemKey } from './form/item/_map-form-item';
import nfFind from './find/find-div.vue';
import nfMenu from './menu/menu.vue';
import nfRouterView from './menu/router-view.vue';
import nfRouterViewTabs from './menu/router-view-tabs.vue';
import loading from './js/loading';
import nfGrid from './grid/grid-table.vue';
import nfGridSlot from './grid/grid-slot.vue';
import nfForm from './form/form-div.vue';
import nfFormSlot from './form/form-div-slot.vue';
import nfFormCard from './form/form-div-card.vue';
import nfFormTab from './form/form-div-tab.vue';
import nfFormStep from './form/form-div-step.vue';
import { install as dialogDrag } from './drag/dialogDrag';
import { _formDrag, install as formDrag } from './drag/formDrag';
import { _gridDrag, install as gridDrag } from './drag/girdDrag';
import { config } from './config';
declare const nfElementPlus: (app: any, option: any) => void;
declare const install: (app: any, option: any) => void;
import { serviceConfig, // 配置
service, // 访问后端API
lifecycle, // 生命周期
createRouter, useRouter, createDataList, createModel, // 创建表单需要的 v-model
loadController, // 加载后端字典
itemController, // 表单子控件的控制类
formController } from './map';
export { serviceConfig, // 配置
service, // 访问后端API
lifecycle, // 生命周期
createRouter, useRouter, config, // 设置表单需要的子控件
nfElementPlus, // 安装插件
install, loading, // 加载动画
nfMenu, nfRouterView, nfRouterViewTabs, nfForm, // 表单控件
nfFormCard, // 分 card 的表单控件
nfFormTab, // 分 tab 的表单控件
nfFormStep, // 分步骤 的表单
AllFormItem, // 表单子控件集合
formItemKey, // id转换为控件
nfFind, // 查询控件
createDataList, nfGrid, // 列表控件
nfGridSlot, nfButton, nfFormSlot, createModel, // 创建表单需要的 v-model
loadController, // 加载后端字典
itemController, // 表单子控件的控制类
formController, // 表单控件的控制类
dialogDrag, // 拖拽对话框
_gridDrag, _formDrag, // 表单
formDrag, gridDrag };
