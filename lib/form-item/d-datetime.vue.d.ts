declare const _default: import("vue").DefineComponent<{
    format: {
        type: StringConstructor;
        default: string;
    };
    'value-format': {
        type: StringConstructor;
        default: string;
    };
    modelValue: (StringConstructor | DateConstructor | NumberConstructor | ArrayConstructor)[];
    columnId: {
        type: (StringConstructor | NumberConstructor)[];
        default: () => number;
    };
    model: {
        type: ObjectConstructor;
    };
    colName: {
        type: StringConstructor;
        default: string;
    };
    controlType: {
        type: NumberConstructor[];
        default: number;
    };
    optionList: {
        type: import("vue").PropType<(import("@naturefw/ui-core/dist/types/20-form-item").IOptionList | import("@naturefw/ui-core/dist/types/20-form-item").IOptionTree)[]>;
        default: () => never[];
    };
    webAPI: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/20-form-item").IWebAPI>;
        default: () => {
            serviceId: string;
            actionId: string;
            dataId: string;
            body: null;
            cascader: {
                lazy: boolean;
                actions: string[];
            };
        };
    };
    delay: {
        type: NumberConstructor;
        default: number;
    };
    events: {
        type: ObjectConstructor;
        default: () => {
            input: () => void;
            enter: () => void;
            keydown: () => void;
        };
    };
    size: {
        type: StringConstructor;
        default: string;
        validator: (value: any) => boolean;
    };
    clearable: {
        type: BooleanConstructor;
        default: boolean;
    };
    extend: {
        type: ObjectConstructor;
        default: () => {};
    };
}, {
    dateType: string;
    value: any;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "update:modelValue"[], "update:modelValue", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    format: {
        type: StringConstructor;
        default: string;
    };
    'value-format': {
        type: StringConstructor;
        default: string;
    };
    modelValue: (StringConstructor | DateConstructor | NumberConstructor | ArrayConstructor)[];
    columnId: {
        type: (StringConstructor | NumberConstructor)[];
        default: () => number;
    };
    model: {
        type: ObjectConstructor;
    };
    colName: {
        type: StringConstructor;
        default: string;
    };
    controlType: {
        type: NumberConstructor[];
        default: number;
    };
    optionList: {
        type: import("vue").PropType<(import("@naturefw/ui-core/dist/types/20-form-item").IOptionList | import("@naturefw/ui-core/dist/types/20-form-item").IOptionTree)[]>;
        default: () => never[];
    };
    webAPI: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/20-form-item").IWebAPI>;
        default: () => {
            serviceId: string;
            actionId: string;
            dataId: string;
            body: null;
            cascader: {
                lazy: boolean;
                actions: string[];
            };
        };
    };
    delay: {
        type: NumberConstructor;
        default: number;
    };
    events: {
        type: ObjectConstructor;
        default: () => {
            input: () => void;
            enter: () => void;
            keydown: () => void;
        };
    };
    size: {
        type: StringConstructor;
        default: string;
        validator: (value: any) => boolean;
    };
    clearable: {
        type: BooleanConstructor;
        default: boolean;
    };
    extend: {
        type: ObjectConstructor;
        default: () => {};
    };
}>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
}, {
    size: string;
    colName: string;
    clearable: boolean;
    columnId: string | number;
    controlType: number;
    optionList: (import("@naturefw/ui-core/dist/types/20-form-item").IOptionList | import("@naturefw/ui-core/dist/types/20-form-item").IOptionTree)[];
    webAPI: import("@naturefw/ui-core/dist/types/20-form-item").IWebAPI;
    delay: number;
    events: Record<string, any>;
    extend: Record<string, any>;
    format: string;
    'value-format': string;
}>;
/**
 * 年月日 时分秒
 */
export default _default;
