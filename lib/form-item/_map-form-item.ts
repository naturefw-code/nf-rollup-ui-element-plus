

interface IFormItemKey {
  [key: number | string]: any
}

// 文本
import nfArea from './t-area.vue' // 100
import nfText from './t-text.vue' // 101
import nfPassword from './t-password.vue' // 102
import nfUrl from './t-url.vue' // 105
import nfAutocomplete from './t-autocomplete.vue' // 107
import nfColor from './t-color.vue' // 108
// 数字
import nfNumber from './n-number.vue' // 110
import nfRange from './n-range.vue' // 111
import nfRate from './n-rate.vue' // 112
// 日期
import nfDate from './d-date.vue' // 120 125 -picker
import nfDatetime from './d-datetime.vue' // 121
import nfMonth from './d-month.vue'  // 122 127
import nfYear from './d-year.vue' // 124
import nfWeek from './d-week.vue' // 123
import nfDates from './d-date-more.vue' // 128
// 时间
import nfTimePicker from './d-time-picker.vue' // 130
import nfTimeSelect from './d-time-select.vue' // 131

// 二选一
import nfSwitch from './s1-switch.vue' // 150
import nfCheckbox from './s1-checkbox.vue' // 151
// 多选和单选
import nfCheckboxs from './s2-checkboxs.vue' // 152
import nfRadios from './s2-radios.vue' // 153
// 下拉
import nfSelect from './s-select.vue' // 160 单选
import nfSelectGroup from './s-select-group.vue' // 162 分组
import nfSelectCascader from './s-select-cascader.vue' // 164 级联
import nfSelectTree from './s-select-tree.vue' // 165 单选

// 上传文件
import nfFile from './u-file.vue'
// 上传图片
import nfPicture from './u-picture.vue'
// 上传视频
import nfVideo from './u-video.vue'

// 不用的
// import nfSelectFind from './s-select-find.vue' // 165 查询
 
/**
 * 表单子控件的集合
 */
const AllFormItem = {
  // 文本 单行 多行 URL 密码 颜色 可选可填
  nfText,
  nfArea,
  nfPassword,
  nfUrl,
  nfAutocomplete,
  nfColor,
  // 数字 滑块 评分
  nfNumber,
  nfRange,
  nfRate,
  // 日期综合  年周
  nfDate,
  nfDatetime,
  nfMonth,
  nfYear,
  nfWeek,
  nfDates,
  // 时间
  nfTimePicker,
  nfTimeSelect,
  // 上传文件 图片 视频
  nfFile,
  nfPicture,
  nfVideo,
  // 开关  勾选 多选组 单选组
  nfSwitch,
  nfCheckbox,
  nfCheckboxs,
  nfRadios,
  // 下拉
  nfSelect, // 160 单选
  nfSelectGroup, // 162 分组=
  nfSelectCascader, // 164 联动
  nfSelectTree // 165 树
}

/**
 * 编号和控件的对应
 */
const formItemKey: IFormItemKey = {
  // 文本类
  100: nfArea,
  101: nfText,
  102: nfPassword,
  103: nfText,
  104: nfText,
  105: nfUrl,
  106: nfText,
  107: nfAutocomplete,
  108: nfColor,
  // 数字
  110: nfNumber,
  111: nfRange,
  112: nfRate,
  // 日期
  120: nfDate,
  121: nfDatetime, // 'nf-datetime',
  122: nfMonth, // 'nf-month',
  123: nfWeek,
  124: nfYear, // 'nf-year',
  125: nfDate, // 'nf-date-range',
  126: nfDatetime, // 'nf-datetime-range',
  127: nfMonth, // 'nf-month-range',
  128: nfDates, // 'nf-dates',
  // 时间
  130: nfTimePicker,
  131: nfTimePicker,
  132: nfTimeSelect,
  133: nfTimeSelect, // 去掉
  // 上传，文件、图片 140
  140: nfFile,
  141: nfPicture,
  142: nfVideo,
  // 选择等
  150: nfCheckbox,
  151: nfSwitch,
  152: nfCheckboxs,
  153: nfRadios,
  // 下拉
  160: nfSelect, // 单选下拉
  161: nfSelect, // 多选下拉
  162: nfSelectGroup, // 分组下拉单选
  163: nfSelectGroup, // 分组下拉多选
  164: nfSelectCascader, // 联动下拉
  165: nfSelectTree, // 树状下拉
  166: nfSelectTree // 树状多选
  // 167: 'nf-select-find', // 远程查询
  // 其他
  // 170: 'nf-option', // 单选、多选、可选、下拉用的选项维护
  // 171: 'nfOptionGroup', // 分组的选项维护
  // 172: 'nfOptionTree' // 树的选项的维护

}

// const formItem = AllFormItem 

export {
  formItemKey,
  AllFormItem
}