
// 类型
export type {
  IGridMeta,
  // IGridTable,
  IGridProps,
  IGridItem,
  IGridSelection
} from './types/50-grid'

// core
export {
  // 配置
  serviceConfig,
  // 访问后端API
  service,
  // 生命周期
  lifecycle,
  // 简易路由
  createRouter,
  useRouter,
  // 属性
  itemProps, // 表单子控件的共用属性
  formProps, // 表单控件的属性
  findProps, // 查询控件的属性
  gridProps, // 列表控件的属性
  // 选项
  getChildrenOne,
  getChildren,
  shallowToDeep,
  cascaderManage, // 下拉列表框等的选项的控制类
  // 表单
  createModel, // 创建表单需要的 v-model
  loadController, // 加载后端字典
  itemController, // 表单子控件的控制类
  formController, // 表单控件的控制类
  // 查询
  findKindDict, // 查询方式的字典
  // createFindModel, // 创建查询需要的 内部存储结构
  // findController, // 查询控件的控制类
  // 列表
  createDataList, // 建立演示数据
  // 按键
  mykeydown, // 操作按钮
  mykeypager, // 翻页的按键
  // 拖拽
  dialogDrag, // 拖拽 dialog
  gridDrag, // 数据列表的拖拽设置
  formDrag, // 设置表单的拖拽
  domDrag, // 设置 td、div可以拖拽
  // 赋值
  debounceRef, // 防抖
  // dateRef, // 处理日期
  custmerRef // 不防抖
} from "@naturefw/ui-core"

// 临时
export type {
  IFindState, // 查询需要的状态，便于各个组件传递数据
  IFindModel, // 绑定查询表单的 model
  IFindProps // 查询控件的 props
} from '@naturefw/ui-core/types/40-find'
