declare const _default: import("vue").DefineComponent<{
    gridMeta: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/50-grid").IGridMeta>;
    };
    height: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
    stripe: {
        type: BooleanConstructor;
        default: boolean;
    };
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    fit: {
        type: BooleanConstructor;
        default: boolean;
    };
    highlightCurrentRow: {
        type: BooleanConstructor;
        default: boolean;
    };
    itemMeta: {
        type: import("vue").PropType<{
            [key: string]: import("@naturefw/ui-core/dist/types/50-grid").IGridItem;
            [key: number]: import("@naturefw/ui-core/dist/types/50-grid").IGridItem;
        }>;
    };
    selection: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/50-grid").IGridSelection>;
        default: () => {
            dataId: string;
            row: {};
            dataIds: never[];
            rows: never[];
        };
    };
    dataList: {
        type: import("vue").PropType<any[]>;
        default: () => never[];
    };
}, {
    slotsKey: string[];
    selectionChange: (rows: import("./controller").IRow[]) => void;
    currentChange: (row: import("./controller").IRow) => void;
    gridRef: import("vue").Ref<null>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    gridMeta: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/50-grid").IGridMeta>;
    };
    height: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
    stripe: {
        type: BooleanConstructor;
        default: boolean;
    };
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    fit: {
        type: BooleanConstructor;
        default: boolean;
    };
    highlightCurrentRow: {
        type: BooleanConstructor;
        default: boolean;
    };
    itemMeta: {
        type: import("vue").PropType<{
            [key: string]: import("@naturefw/ui-core/dist/types/50-grid").IGridItem;
            [key: number]: import("@naturefw/ui-core/dist/types/50-grid").IGridItem;
        }>;
    };
    selection: {
        type: import("vue").PropType<import("@naturefw/ui-core/dist/types/50-grid").IGridSelection>;
        default: () => {
            dataId: string;
            row: {};
            dataIds: never[];
            rows: never[];
        };
    };
    dataList: {
        type: import("vue").PropType<any[]>;
        default: () => never[];
    };
}>>, {
    height: string | number;
    stripe: boolean;
    border: boolean;
    fit: boolean;
    highlightCurrentRow: boolean;
    selection: import("@naturefw/ui-core/dist/types/50-grid").IGridSelection;
    dataList: any[];
}>;
/**
 * 支持插槽扩展的列表
 */
export default _default;
