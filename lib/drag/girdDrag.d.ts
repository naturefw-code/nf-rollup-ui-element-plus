declare const _gridDrag: {
    mounted(el: any, binding: any): void;
    unmounted(el: any, binding: any): void;
};
/**
 * grid 的拖拽的自定义指令，
 * * 可以记录调整后的 td 的宽度，
 * * 修改 td 的顺序，
 * * 移除 td
 * @param {*} app
 * @param {*} options
 */
declare const install: (app: any, options: any) => void;
export { _gridDrag, install };
