declare const _formDrag: {
    mounted(el: any, binding: any): void;
    unmounted(el: any, binding: any): void;
};
/**
 * form 表单控件 的拖拽的自定义指令，
 * * 修改 字段 的顺序，
 * * 移除 字段
 * @param app
 * @param options
 */
declare const install: (app: any, options: any) => void;
export { _formDrag, install };
